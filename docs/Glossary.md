# Appendix E: Glossary
  
The nomenclature surrounding energy systems can sometimes be quite overwhelming, and even somewhat confusing. This appendix aims at clarifying some of the most common accronyms used across the COPPER model through the glossary provided below.

## Glossary Table

| Accronym / COPPER Term | Meaning                                                                                                   | Learn More                                                                                                               |
|------------------------|-----------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| CCS                    | Carbon capture and storage. This technology is designed to reduce carbon dioxide levels in the atmosphere | [What is CCS? - International CCS Knowledge Centre](https://ccsknowledge.com/what-is-ccs)                                               |
| gasCC                  | Combined-cycle natural gas power plants                                                                                                           | [Combined cycle power plants: how it works - GE Vernova](https://www.gevernova.com/gas-power/resources/education/combined-cycle-power-plants)                                                                                                                         |
| gasCG                  |   Cogeneration natural gas power plants                                                                                                        | [Combined heat and power - Canadian Gas Association](https://www.cga.ca/natural-gas-101/combined-heat-and-power/)                                                                                                                         |
| gasSC                  |                                                                                                           |                                                                                                                          |
| hydro_daily            |                                                                                                           |                                                                                                                          |
| hydro_monthly          |                                                                                                           |                                                                                                                          |
| hydro_run              |                                                                                                           |                                                                                                                          |
| ITC                    | Investment-tax credit. Federal tax credit for investors in clean electricity technologies                 | [What to Know: Canada’s Investment Tax Credit (ITC) - Sky Fire Energy](https://skyfireenergy.com/investmenttaxcredit/)                     |
| Planning reserve       |                                                                                                           |                                                                                                                          |
| Reserve margin         |                                                                                                           |                                                                                                                          |
| RNG                    | Renewable natural gas. Produced from organic waste, purified, and reintroduced into the grid              | [Renewable natural gas - Canadian Gas Association](https://www.cga.ca/natural-gas-101/the-renewable-natural-gas-opportunity/)                    |
| SMR                    | Small modular nuclear reactor                                                                             | [What are SMRs? - IAEA](https://www.iaea.org/newscenter/news/what-are-small-modular-reactors-smrs)                              |
| storage_PH             | Pumped hydro storage                                                                                      | [Pumped storage hydropower explained - US Department of Energy](https://www.energy.gov/eere/water/pumped-storage-hydropower)                       |
| storage_LI             | Lithium-ion battery storage                                                                               | [Applications of Lithium‑Ion Batteries in Grid‑Scale Energy Storage Systems - Springer](https://link.springer.com/article/10.1007/s12209-020-00236-w) |
| wind_ons               | Onshore wind plant                                                                                        | [Wind Energy - NRCAN](https://natural-resources.canada.ca/energy/energy-sources-distribution/renewables/wind-energy/7299) |
| wind_ofs               | Offshore wind plant                                                                                       | [Wind Energy - NRCAN](https://natural-resources.canada.ca/energy/energy-sources-distribution/renewables/wind-energy/7299) |


## Generation Type Data for CER Constraints

| Genenration Type Name                  | Definition                                                                                                            | How it is Used in COPPER                      |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------|--------------------------------------------|
| coal_pre2025                    | coal unit in existence before simulation begins                                                                      | CER regulates end of life but not operation |
| coal_ccs_pre2025                | coal unit in existence before simulation begins that has transitioned to carbon capture and storage                   | CER regulates operation                     |
| coal_retire_pre2025             | coal unit in existence before simulation begins that has retired                                                      | Unit is retired and does NOT operate        |
| diesel_pre2025                  | diesel unit in existence before simulation begins                                                                     | CER regulates end of life but not operation |
| diesel_bio_pre2025              | diesel unit in existence before simulation begins that has transitioned to a biodiesel unit                           | CER regulates operation                     |
| diesel_backup_pre2025           | diesel unit in existence before simulation begins that has transitioned to a backup diesel generation unit            | CER regulates operation                     |
| gasCG_free_pre2025              | Cogeneration natural gas unit in existence before simulation begins which is not a net exporter and is not regulated | CER does NOT regulate these units           |
| gasCG_free_retire_pre2025       | Cogeneration natural gas unit in existence before simulation begins which has retired                                 | Unit is retired and does NOT operate        |
| gasCG_restricted_pre2025        | Cogeneration natural gas unit in existence before simulation begins which is a net exporter                          | CER regulates end of life but not operation |
| gasCG_restricted_ccs_pre2025    | Cogeneration natural gas unit that has transitioned to carbon capture and storage                                     | CER regulates operation                     |
| gasCG_restricted_rng_pre2025    | Cogeneration natural gas unit that has transitioned to renewable natural gas                                          | CER regulates operation                     |
| gasCG_restricted_backup_pre2025 | Cogeneration natural gas unit that has transitioned to backup generation                                              | CER regulates operation                     |
| gasCG_restricted_retire_pre2025 | Cogeneration natural gas unit that has retired                                                                        | Unit is retired and does NOT operate        |
