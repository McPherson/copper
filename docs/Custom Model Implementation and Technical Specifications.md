# Customizing Model Implementation and Technical Specifications

This section is to provide direction on how to customize COPPER using the various input files and features of COPPER.py. The primary points of interaction

Sure, I can help clarify and structure the steps for creating a custom scenario in COPPER. Here’s a revised version with clearer instructions and formatting:

---

# Steps to Creating a Custom Scenario in COPPER

Follow these steps to create and run a custom scenario in COPPER:

### Step 1: Navigate to the Scenarios Folder
Start by navigating to the `./scenarios` directory in your project.

### Step 2: Create a New Scenario Folder
Create a new folder within the `./scenarios` directory. Name the folder after the scenario you wish to test.

### Step 3: Copy Initial Configuration Files
Copy the `config.toml` and `extant_capacity.csv` files from an example scenario (e.g., `Custom_Scenario`) located in `./scenarios/common` to your newly created scenario folder.
- Repeat this step for any other input sheets in the common folder that you wish to cusomize, for more information on inputs please see [Inputs and Outputs Datatable](./Inputs%20and%20Outputs%20Datatable.md)

### Step 4: Edit Configuration Files
Edit the copied files (`config.toml` and `extant_capacity.csv`) to adjust the settings according to the needs of your custom scenario.

### Step 5: Run Your Scenario
Execute the scenario by running the following command in your terminal, making sure to replace `"Custom_Scenario"` with the name of your new scenario folder:

```bash
python copper.py --scenario "Your_New_Scenario_Name"
```

### Notes
- **File Precedence**: COPPER will first read the input files from your custom scenario folder. If any files are missing, it will then look for them in the `./scenarios/common` folder.
- **Editing Default Values**: If you need to edit additional default values, refer to the example scenario configurations, such as those in the `./scenarios/CER_scenario` folder.
- **Other Inputs**: Repeat step

# Configuring the `config.toml` File

The `config.toml` file contains various configuration settings that control the behavior and parameters of the simulation model. When making a custom scenario this file should always be present in yout inputs folder. Below is a breakdown of each section as articulated in the default scenario and the settings you can modify.

## Simulation Settings

This section allows you to set the general parameters for the simulation:

- `user_specified_demand`: Toggle to use a user-specified demand profile. Default is `false`.
- `forecast_year`: Set the target year for the forecast. Default is `2050`.
- `reference_year`: Set the base year for the simulation. Default is `2021`.
- `season`: Define the seasons for which the simulation will be run. Default is `["winter", "summer"]`.
- `capacity_val_inds`: Specify indices for validation of capacity values by season and technology.
- `pds`: Define projection decision years within the simulation. Default includes several years up to `2050`.
- `ap`: Define the areas or regions included in the simulation.
- `aba`: List of all possible balancing areas.
- `aba1`: Do not modify this vector.
- `run_days`: Specify representative peak days for each month.
- `run_days_test`: Specify the days used for testing.
- `save_lp`: Toggle to save the linear programming model. Default is `false`.

## Transmission

Control the settings for the transmission model within the simulation:

- `trans_o_m`: Set the operations and maintenance costs for transmission.
- `transcost`: Define the baseline cost for transmission.
- `intra_ba_transcost`: Set the cost for intra-balancing area transmission.
- `translossfixed`: Set a fixed transmission loss rate.
- `translossperkm`: Define the per kilometer loss rate for transmission.
- `CTE_extant`: Toggle constrained transmission expansion.
- `CTE_coef`: Set the allowable percentage increase in transmission capacity.
- `CTE_custom`: Toggle the use of a custom transmission capacity scenario.

## Hydro

Configure settings related to hydroelectric power:

- `pump_ret_limit`: Set the limit for retrofitting hydro facilities to include storage.
- `hydro_development`: Toggle the development of new hydro projects.
- `just_small_hydro`: Limit consideration to small hydro projects under 100 MW.
- `hydro_minflow`: Set the minimal flow rate for hydro installations.

## Carbon

Manage settings for carbon emissions and policies:

- Detailed configurations for carbon taxes, emission limits, and technologies related to carbon capture and storage (CCS).
- Control which technologies are allowed new CCS installations or are phased out by certain years.

## Phase Out

Define phase-out schedules for various thermal power technologies:

- `thermal_phase_out`: Toggle whether thermal power units are being phased out.
- Detailed configurations for when specific technologies must be phased out.

## Economics

Set economic parameters like inflation rate, discount rates, and specific costs related to technology and operations:

- `inflation`: Define the annual inflation rate. Setting this to 0 results in the discount rate being the real discount rate
- `discount`: Set the discount rate used in economic calculations.
- `discount_2050`: Set a specific discount rate for the year 2050.
- Detailed settings for costs associated with different technologies and operational parameters.

## Storage

Configure settings for energy storage within the simulation:

- `storage_continuous`: Toggle the use of continuous storage.
- Detailed configurations for the number of storage hours associated with different storage technologies.

## Regulations

Set regulatory parameters that affect the simulation:

- `GPS`: Toggle the General Pricing Scheme.
- `CPO`: Toggle the Clean Power Option.
- `OBPS_on`: Activate the Output-Based Pricing System.
- `autarky`: Toggle a scenario of limited trade between balancing areas.
- Detailed autonomy percentages for specific regions.

## Electrical Grid

Configure parameters specific to the electrical grid's operation:

- Toggle whether reserve margins are aggregated.
- Set non-emitting limits and other calculations related to grid operation.

## Thermal

Control settings for thermal power generation:

- `new_thermal_limit`: Toggle restrictions on new thermal generation units.
- Define which old thermal plants are limited and the gas prices for different areas.

## Constraints

Manage various constraints that apply to the simulation:

- Toggle capacity constraints for nuclear, small modular

 reactors (SMRs), and biomass.
- Set maximum allowable capacities for wind and solar per square kilometer.
- Detailed configurations for renewable portfolio standards and emission limits for different periods.

# Running COPPER Command Line Tool

The COPPER command line tool is designed to allow users to specify different scenarios, solvers, and other options for their COPPER runs. The default command to run the tool is:

```bash
python copper.py
```

## Command Line Arguments

You can customize the execution of the COPPER tool by passing various command line arguments. Below is a list of available arguments with their descriptions:

### Scenario
- **Flag**: `-sc` or `--scenario`
- **Description**: Select the COPPER scenario to run. This string must match exactly the name of a folder in the ./scenarios folder. for example CER_scenario which is located in ./scenarios/ by default
- **Type**: `string`
- **Default**: `"BAU_scenario"`
- **Usage**:
  ```bash
  python copper.py --scenario "Custom_Scenario"
  ```

### Solver
- **Flag**: `-so` or `--solver`
- **Description**: Select the solver for COPPER.
- **Type**: `string`
- **Default**: `"cplex"`
- **Other Valid Options**: `"glpk"`, `"cbc"`
- **Usage**:
  ```bash
  python copper.py --solver "glpk"
  ```

### Output Folder
- **Flag**: `-o` or `--output`
- **Description**: Specify the folder where the output results should be saved.
- **Type**: `string`
- **Default**: `"results"`
- **Usage**:
  ```bash
  python copper.py --output "output_folder"
  ```

### Test Mode
- **Flag**: `-t` or `--true`
- **Description**: Specifies that copper will run in test mode (only runs 1 day of simulation per year)
- **Type**: `string`
- **Default**: `"true"`
- **Usage**:
  ```bash
  python copper.py -t
  ```

### Seed
- **Flag**: `-se` or `--seed`
- **Description**: Set a seed to make results reproducible. Note that this option is only effective when using the `"glpk"` solver.
- **Type**: `integer`
- **Default**: `None`
- **Usage**:
  ```bash
  python copper.py --seed 12345
  ```

## Example Usage

Below is an example of how to run the tool with multiple options:

```bash
python copper.py --scenario "Advanced_Scenario" --solver "cbc" --output "new_results" --test "true" --seed 54321
```

This command will configure COPPER to use the "Advanced_Scenario", run with the "cbc" solver, save the outputs in the "new_results" folder, operate in test mode, and use a seed of `54321` for reproducibility.

or for our development testing we use:

```
python COPPER.py -sc BAU_scenario -o tests/results/ -so glpk --seed 0 -t
```
---

Ensure that users have Python and necessary dependencies installed, and that they have the necessary permissions to execute scripts and write to specified directories.