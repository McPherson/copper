# Quickstart

To run COPPER the following requirements must be setup on your machine:
  - either [Python 3.10](https://www.python.org/downloads/release/python-3100/), [Anaconda](https://www.anaconda.com/) or [Miniconda](https://docs.anaconda.com/free/miniconda/)
  - [git](https://git-scm.com/)

  In addition, you will need to install an optimization solver of choice to run the model. The following solvers have been tested to work with COPPER:

1. [CPLEX](https://www.ibm.com/products/ilog-cplex-optimization-studio/cplex-optimizer) (recommended and default)
2. [GLPK](http://www.osemosys.org/uploads/1/8/5/0/18504136/glpk_installation_guide_for_windows10\_-\_201702.pdf)
3. [Coin-OR CBC](https://github.com/coin-or/Cbc)

   > Note: if installing from binaries, we recommend [this distribution](https://www.coin-or.org/download/binary/Cbc/Cbc-master-win64-msvc15-mtd.zip) of CBC for windows users

## Cloning and running your first COPPER Simulation
1. Clone the repository: `git clone https://gitlab.com/sesit/copper.git`
2. Open your local version of copper (the directory, cloned in step 1) and enter the following commands to create and activate the conda environment (you have to activate the environment each time the promt is started): `conda create -n copper python=3.10` `conda activate copper`
  >note: conda is not a requirement using [venv](https://docs.python.org/3/library/venv.html) or your preferred python virtual environment is fine
3. Install packages (relevant for the first time you activate the environment): `pip install -r requirements.txt`
4. Run COPPER: `python COPPER.py`
   > Note: if you are using a solver other than CPLEX you will need to specify it in the command line argument for example if using glpk use: `python COPPER.py -so glpk`

# General Description of COPPER Modules

COPPER has one main script that starts with the scenario configuration section. This section reads in the input files for a scenario name (i.e. BAU_scenario). All the input files (extant_capacity.csv, transmission.csv, genteration_type_data.csv, etc.) are to be provided in folder with the title of your scenario name. Three scenarios and associated folders are provided by default: ./secnarios/BAU_scenario, ./scenarios/CER_ITC_sceanrio and ./scenarios/CER_sceanrio. The differences related to constraints for the CER scenario are explained in the _Mathematical Formulation of COPPER_ appendix to this wiki. Majority of scenario configuration can be accomplished using the config.toml file. 

A complete set of inputs can be found in ./scenarios/common. This folder contains all the default inputs to run COPPER for a Canada-wide simulation. If you would like to customize any of these inputs, please see the section on creating scenarios.

The second part of the code reads and pre-processes the various input sheets and converts them into the proper format for the optimization model. The third part of the script is the optimization problem formulation in which all variables are defined and objective function and constraints are modeled using the `ConcreteModel` class in Pyomo.

The fourth section saves the solver report, variable values and model configuration into csv and text format files. Finally, the last section of the scripts creates a summary excel sheet out of the results by post processing the solver outputs and extracting the most important variables such as the generation mix, new capacity addition, emissions, and costs data, called Results_summary.xlsx. All the result files are saved into the results folder with the scenario name, the date and the mention “Test” if test flag is set to true (example: _BAU_scenario_20231004_1539_Test_).

# Input Data

The input data for COPPER is contained within the "scenarios" folder in the repository, where each scenario has a different set of input files and in-code flags to run the model. The `config.toml` file contains the general flags and input variables that will be used by COPPER.

## Common Inputs

The common folder contains data that is not scenario-specific and doesn't usually change between scenarios. However, if the user decides to change the values in one of these "static" files, it is possible to do so by simply adding a file with the same name inside the desired "scenario" folder.

### gridcells.csv

Data relevant to the grid cells used for VRE development in COPPER. Includes each grid cell’s location, population, distance to transmission grid and onshore/offshore surface area.

### Capacity Factor Files

1. hydro_csv: the hourly capacity factors for hydro in each province;
2. solarcf.csv: the hourly capacity factors for solar generation in each grid cell;
3. windcf.csv: the hourly capacity factors for wind generation in each grid cell.

## Scenario Inputs

The scenario folder contains data that is used for any user-defined scenarios to be run by the user. The name of the folder to be run is defined in the COPPER.py script, under the `scenario` variable. To create a new scenario, duplicate the entire scenario folder and change the scenario name in the code to the new folder name. The ‘BAU_scenario’ is COPPER's default scenario. These are the scenarios-specific input files:

- config.toml: this is the scenario configuration file, containing many user-input parameters and variables. Descriptions of each value are given in the file.
- capacity_value.csv: the capacity value per province for each generation type, used in the reserve margin calculation.
- extant_capacity.csv: the extant electricity generating capacity in each balancing area and period by plant type.
- extant_capacity_CER (extant capacity files for the CER scenario):
  * \_20_years.csv: default for CER scenario
  * \_25_years.csv: used in the CER scenario when 25 years EOPL is applied
- extant_storage.csv: existing storage capacity per balancing area at the start of the simulation.
- extant_wind_solar.csv: existing wind and solar capacity per grid cell, for each period.
- generation_type_data.csv: the cost and technical data of different types of generation technologies.
- hydro_new.csv: a list of all possible hydro greenfield, renewable and repowering projects across Canada and associated data for each project.
- max_capacity_limits.csv: the capacity limits for nuclear, SMR and biomass in MW over the entire horizon.
- renewable_portfolio_max.csv: the maximum amount of wind/solar capacity per period for each province.
- renewable_portfolio_min.csv: the minimum amount of wind/solar capacity per period for each province.
- technology_evolution.csv: the change in capital costs for each technology, per period. Contains 2 sets of values: base and advanced.
- transmission.csv: data about the transmission lines between balancing areas. Contains the distance, (optional) expansion limits and capacity per period.
- trans_expansion_minimum_1000.csv: data about the transmission lines between balancing areas, but setting a minimum 1000 MW transmission between some balancing areas.
- wind_solar_capacity_value.csv: the seasonal capacity value of wind and solar generation in each balancing area for planning reserve margin calculations.
- wind_solar_location_recon.csv: potential locations and maximum capacity of wind and solar reconstruction options in each grid cell per period.

## Demand

The following are the input files related to demand:

- annual_growth.csv: the annual electricity demand growth for each balancing area.
- demand.csv: the hourly electricity demand in each province.
- reserve_margin.csv: the reserve margins required by each province.
- us_demand.csv: the hourly electricity import/export at connected points between Canada and the U.S.
- user_input_demand.csv: the hourly electricity demand in each province. This data can be specified by the user. Format is different from the regular demand.csv file (columns are for province and year, with rows for each hour of the year)

# Output Data

Output data is saved in a unique folder for each run inside the “results” folder. The output files are described below.

- ABA_investment_costs.csv: annualized investments costs per period per technology per balancing area.
- capacity_solar.csv: the capacity of the solar farm that the model builds in grid cell X and period P.
- capacity_solar_recon.csv: the capacity of the solar farm that the model repowers in grid cell X and period P.
- capacity_wind_ofs.csv: the capacity of offshore wind farms that the model builds in grid cell X and period P.
- capacity_wind_ons.csv: the capacity of onshore wind farms that the model builds in grid cell X and period P.
- capacity_wind_recon_ofs.csv: the capacity of offshore wind farms that the model repowers in grid cell X and period P.
- capacity_wind_recon_ons.csv: the capacity of onshore wind farms that the model repowers in grid cell X and period P.
- capacity_thermal.csv: the capacity of the thermal units the model builds in balancing area (BA) X and period P.
- capacity_transmission.csv: the capacity of the transmission line that the model builds between BA area X and Y.
- capacity_storage.csv: the pumped hydro storage capacity that model builds in each BA area and period.
- COPPER config.txt: summary of the configuration parameters used to run COPPER.
- day_renewal_binary.csv: the value of the binary associated with each hydro day potential project: 1 means that the model builds the project.
- dayrenewalout.csv: the hourly generation of new installed day-storage hydro facilities.
- daystoragehydroout.csv: the output of daystorage hydro facilities in each BA, hour and period.
- load_shedding.csv: energy from load shedding in each BA and period.
- month_renewal_binary.csv: the value of the binary associated with each hydro month potential project: 1 means that the model builds the project.
- monthrenewalout.csv: the hourly generation of new installed month-storage hydro facilities.
- monthstoragehydroout.csv: the output of monthstorage hydro facilities in each BA, hour and period.
- new_ABA_generation_mix.csv: the added capacity per technology in each BA and period.
- obj_value.csv: the objective function value (total costs).
- Qualifying_capacity_summer.csv: the summer qualifying capacity for each technology, BA and period.
- Qualifying_capacity_winter.csv: the winter qualifying capacity for each technology, BA and period.
- Results_summary.xlsx:The results summary file is an excel spreadsheet that aggregates the most important model output data from the simulation. Data found in this file can also be found across the other model output files described above. This file is useful for producing figures using visualization software, given that the data is stored in a standard format.
- retire_thermal.csv: the capacity of each type of thermal unit that the model retires in each BA and period.
- ror_renewal_binary.csv: the value of the binary associated with each hydro ror potential project: 1 means that the model builds the project.
- solarout.csv: the generation of solar electricity in each grid cell, and for each hour and period.
- storageenergy.csv: the amount of energy stored for each BA, hour and period.
- storagein.csv: the amount of new energy charged for each BA, hour and period.
- storageout.csv: the amount of energy discharged for each BA, hour and period.
- supply.csv: the generation of thermal units in each BA, hour and period (thermal dispatch schedule).
- Total_installed_ABA.csv: The total capacity per technology in each BA and period.
- transmission.csv: the transmitted flow of power between BAs in each hour and period.
- windofsout.csv: the generation of offshore wind in each grid cell, and for each hour and period.
- windonsout.csv: the generation of onshore wind in each grid cell, and for each hour and period.