# Inputs and Outputs

## Inputs Table

The table below lists the output files from COPPER and provides further details on their data contents, units, and resolutions.

Note: all day and hour calculations and indexes are based off of NASA's [Day of the Year (DOY) calendar](https://nsidc.org/data/user-resources/help-center/day-year-doy-calendar)

| Input File             | Data                             | Description                                                          | Unit                          | Spatial Resolution    | Temporal Resolution     |
|------------------------|----------------------------------|----------------------------------------------------------------------|-------------------------------|-----------------------|-------------------------|
| gridcells.csv          |                                  |                                                                      |                               |                       |                         |
|                        | grid_cell                        | A number identifying the grid cell                                   | N/A                           | Each grid cell        | Entire time series      |
|                        | lon/lat                          | Coordinates of the grid cell                                         | N/A                           | Each grid cell        | Entire time series      |
|                        | ba                               | Balancing area                                                       | N/A                           | Each grid cell        | Entire time series      |
|                        | pr                               | Province                                                             | N/A                           | Each grid cell        | Entire time series      |
|                        | distance_to_grid                 | Distance to the closest transmission grid cell                       | Km                            | Each grid cell        | Entire time series      |
|                        | population                       | Population contained in each grid cell                               | Population size in numbers    | Each grid cell        | Entire time series      |
|                        | surface_area_ofs                 | Offshore surface area                                                | Km2                           | Each grid cell        | Entire time series      |
|                        | surface_area_ons                 | Onshore surface area                                                 | Km2                           | Each grid cell        | Entire time series      |
| hydro_cf.csv           | Hydro capacity factor            | Provincial hydro capacity factor                                     | %                             | Each province         | Hourly                  |
| solarcf.csv            | Solar capacity factor            | Solar capacity factor for each grid cell                             | %                             | Each grid cell        | Hourly                  |
| windcf.csv             | Wind capacity factor             | Wind capacity factor for each grid cell                              | %                             | Each grid cell        | Hourly                  |
| capacity_value.csv     | Capacity values                  | Capacity values for all generation technologies                      | %                             | Each province         | Entire time series      |
| extant_capacity.csv    | Existing capacity                | Existing capacity in each balancing area for each period             | MW                            | Each balancing area   | Per period              |
| extant_storage.csv     | Existing storage                 | Existing storage in each balancing area for each storage type        | MW                            | Each balancing area   | At the start of model  |
| extant_wind_solar.csv  | Existing wind / solar capacity   | Existing wind and solar capacity in each grid cell for each period   | MW                            | Each grid cell        | Per period              |
| generation_type_data.csv |                                |                                                                      |                               |                       |                         |
|                        | min_capa_fact                    | Minimum capacity factor (not used)                                   | %                             | All balancing areas   | Entire time series      |
|                        | max_capa_fact                    | Maximum capacity factor (not used)                                   | %                             | All balancing areas   | Entire time series      |
|                        | efficiency                       | Efficiency of technology                                             | %                             | All balancing areas   | Entire time series      |
|                        | fuel_co2                         | Real emissions intensity of fuel for thermal generators              | tCO2eq/MWh                    | All balancing areas   | Entire time series      |
|                        | fuel_co2_obps_2025               | Emissions intensity of fuel subject to output based standards in 2025 | tCO2eq/MWh                    | All balancing areas   | Entire time series      |
|                        | fuel_co2_obps_2030               | Emissions intensity of fuel subject to output based standards in 2030 | tCO2eq/MWh                    | All balancing areas   | Entire time series      |
|                        | fixed_o_m                        | Fixed operation and maintenance costs                                 | CAD$/MW-year                  | All balancing areas   | Entire time series      |
|                        | variable_o_m                     | Variable operation and maintenance costs                              | CAD$/MWh                      | All balancing areas   | Entire time series      |
|                        | fuelprice                        | Price of fuel for thermal generators                                  | CAD$/GJ                       | All balancing areas   | Entire time series      |
|                        | capitalcost                      | Annualized capital cost of new capacity                               | CAD$/MW-year                  | All balancing areas   | Entire time series      |
| max_capacity_limits.csv |                                  |                                                                      |                               |                       |                         |
|                        | MAX_SMR                          | Maximum SMR capacity per balancing area                               | MW                            | Each balancing area   | Entire time series      |
|                        | MAX_nuclear                      | Maximum nuclear capacity per balancing area                           | MW                            | Each balancing area   | Entire time series      |
|                        | MAX_bio                          | Maximum bio fuel capacity per balancing area                          | MW                            | Each balancing area   | Entire time series      |
| renewable_portfolio_max.csv | Solar/wind max capacity   | Maximum solar and wind (offshore and onshore) capacity per province   | MW                            | Each


## Outputs Table

The table below lists the output files from COPPER and provides further details on their data contents, units, and resolutions.

| Output File                 | Data                      | Description                                                                  | Unit        | Spatial Resolution  | Temporal Resolution |
|-----------------------------|---------------------------|------------------------------------------------------------------------------|-------------|---------------------|---------------------|
| ABA_investment_costs.csv    | Capacity investment costs | Capacity investment costs for each balancing area (Annuity value)            | $CAD/year   | All balancing areas | Per period          |
| capacity_solar.csv          | Solar capacity            | Resulting solar capacity in each time slice for each year                    | MW          | All balancing areas | Per period          |
| capacity_solar _recon.csv   | Reconstructed solar capacity | Reconstructed capacity for solar at the end of the model execution         | MW          | All balancing areas | Per period          |
| capacity_storage.csv        | Storage capacity          | Resulting capacity for storage in each time slice for each year              | GWh         | All balancing areas | Per period          |
| capacity_thermal.csv        | Thermal capacity          | Resulting capacity for thermal technologies for each province for each year  | MW          | Per balancing area  | Yearly              |
| capacity_transmission.csv   | Transmission capacity     | Resulting capacity for transmission technologies for each province for each year | MW      | Per balancing area  | Yearly              |
| capacity_wind_ons.csv       | On-shore wind capacity    | Resulting capacity for transmission technologies for each province for each year | MW      | All balancing areas | Per period          |
| capacity_wind_ofs.csv       | Off-shore wind capacity   | Resulting capacity for off-shore wind in each time slice for each year        | MW          | All balancing areas | Yearly              |
| day_renewal_binary.csv      | Hydro capacity renewal as a binary | Daily hydro capacity renewal as a binary value                         | N/A         | Per balancing area  | Yearly              |
| dayrenewalout.csv           | Hourly hydro generation   | Daily hydro renewal capacity output per hour                                 | GWh         | Per balancing area  | Yearly              |
| daystoragehydroout.csv      | Daily hydro storage output| Daily hydro storage output per hour                                          | GWh         | Per balancing area  | Per period          |
| load_shedding.csv           | Load shedding             | Load shedding per hour per period per balancing area                         | MW          | Per balancing area  | Per hour            |
| model.lp                    | Model linear program      | Linear program description of the model in a file that can be used by linear solvers | N/A     | N/A                 | N/A                 |
| month_renewal_binary.csv    | Renewal binary            | Monthly hydro capacity renewal binary                                        | N/A         | Per balancing area  | Yearly              |
| monthstoragehydroout.csv    | Hourly generation         | Monthly hydro storage output per hour                                        | GWh         | Per balancing area  | Per hour            |
| new_ABA_generation_mix.csv  | New ABA installed capacity| New installed capacity per balancing area per period                         | MW          | Per balancing area  | Yearly              |
| obj_value.csv               | Objective value           | Total system costs                                                           | CAD$        | All balancing areas | Over the entire time scope of the model |
| Qualifying_capacity_summer.csv | Qualifying capacity   | Qualifying capacity of each generation type for the summer time per balancing area and year | MW  | Per balancing area  | Yearly              |
| Results_summary.xlsx        | Results summary           | Summary of Canada’s generation mix for each modeled generation type          | MW          | All balancing areas | Yearly              |
| retire_thermal.csv          | Retired Thermal           | Retired thermal capacity for each generation type                            | MW          | Per balancing area  | Yearly              |
| ror_renewal_binary.csv      | Renewal binary            | Run-of-river capacity renewal binary                                         | N/A         | Per balancing area  | Yearly              |
| solarout.csv                | Solar output              | Output from solar generation                                                 | MW          | Per balancing area  | Yearly              |
| storageenergy.csv           | Stored energy             | Energy stored for each storage type                                          | GWh         | Per balancing area  | Yearly              |
| storagein.csv               | Stored energy input       | Input of energy stored for each storage type                                 | GWh         | Per balancing area  | Yearly              |
| storageout.csv              | Stored energy output      | Output of energy stored for each storage type                                | GWh         | Per balancing area  | Yearly              |
| supply.csv                  | Total energy supply       | Total energy supplied by each generation type                                | MW          | Per balancing area  | Yearly              |
| Total_installed_ABA.csv     | Total installed ABA capacity | Total new installed capacity by balancing area                             | MW          | Per balancing area  | Yearly              |
| transmission.csv            | Energy transmission       | Total energy transferred between balancing areas                             | MW          | Per balancing area  | Yearly              |
| windofsout.csv              | Wind off-shore output     | Output of off-shore wind capacity                                            | MW          | Per balancing area  | Yearly              |
| windonsout.csv              | Wind on-shore output      | Output of on-shore wind capacity                                             | MW          | Per balancing area  | Yearly              |
| solver_report.txt           | Solver report             | Summary of solver messages                                                   | N/A         | N/A                 | N/A                 |
