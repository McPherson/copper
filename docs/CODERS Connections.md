# Appendix C: CODERS Database Integrations

This appendix section shows the implemented connections from COPPER to the [CODERS database](https://cme-emh.ca/en/coders/).

The endpoints accessed by COPPER are

1. /generators

2. /grid_cell_info

3. /storage

4. /transfer_capacities_copper


These are used by the preprocess scripts to create the following files

1. extant_capcity

2. extant_wind_solar

3. extant_transmission

4. extant_storage


## Extant Capcity
This file is created in the get_extant_capacity() by taking in the total generators in CODERS and the years to split into. The function first creates a location column based on the balancing area and then creates and end_year column by taking the higher of the closure_year and possible_renewal_year columns. Using this it bins the generators by their end year and cumulatively sums up their capacity.

## Extant Wind and Solar
Uses the total generators in coders along with grid_cell_info to take out the vre generators and split up into their respective grid cells. This is then sent to the get_extant_capacity() functioning similar to Extant Capacity except instead creating a location column based on the grid cell.

## Extant Transmission
Simply takes in the data within the /transfer_capacities_copper endpoint and converts it to a csv with some renaming.

## Extant Storage
Functions identically to Extant Capcity but instead using the /storage endpoint to just get the storage generators.