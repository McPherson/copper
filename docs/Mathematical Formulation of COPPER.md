# Appendix B: Mathematical Formulation of COPPER

The following section shows the constraints implemented for the [CER](https://www.canada.ca/en/environment-climate-change/news/2023/08/draft-clean-electricity-regulations.html) legislation.

## Clean Electricity Regulations (CER) Constraints

These contraints are based on the draft of the CER that is currently under review.

1. Coal plants forced to retire by 2030:

    ![](https://gitlab.com/sesit/copper/-/raw/a2ed342cf136ddeec228df27476a3e9bbe2a4b10/Documentation/images/Coal_Plants_Retire_by_2030.jpg)

    Where *EX* is the extant capacity, *x* the generation capacity, *xr* the retired capacity, *n* the balancing area, *t* the period (year), and *K^RC_o* the subset coal generation technologies.
    
2. No more develeopment of pre-2025 fossil generators

    ![](https://gitlab.com/sesit/copper/-/raw/a2ed342cf136ddeec228df27476a3e9bbe2a4b10/Documentation/images/No_Development_pre_2025_fossil.jpg)

    Where *x* is the generation capacity, *n* the balancing area, *t* the period (year), and *K^R* the subset of generation technologies subject to the CER

3. No backup generation for business as usual (BAU) case
    
    ![](https://gitlab.com/sesit/copper/-/raw/a2ed342cf136ddeec228df27476a3e9bbe2a4b10/Documentation/images/No_Backup_Generation.jpg)

    Where *x* is the generation capacity, *n* the balancing area, *t* the period (year), and *K^RB_g* the subset of backup generating technologies introduced by the CER.
    
    This constraint is built on top of the BAU case, to make sure that no backup generators are built, since the backup generation is considered only when the CER flag is set to **true**

4. Limiting carbon capture and storage (CCS) development for Alberta and Saskatchewan
    
    ![](https://gitlab.com/sesit/copper/-/raw/a2ed342cf136ddeec228df27476a3e9bbe2a4b10/Documentation/images/Limiting%20CCS.jpg)
    
    Where *x* is the generation caapacity, *n* the balancing area, *t* the period (year), and *K^RCCS* the subset of CCS-equipped technologies generation technologies subject to the CER

5. Parasitic load for CCS thermal plants
    
    ![](https://gitlab.com/sesit/copper/-/raw/a2ed342cf136ddeec228df27476a3e9bbe2a4b10/Documentation/images/Parasitic%20Load%20CCS%20Thermal.jpg)

    Where *y* is the generation, *EX* the extant capacity, *x* the generation caapacity, *n* the balancing area, *t* the period (year), *xr* the retired capacity, *h* the hour of the year, and *K^RCCS* the subset of CCS-equipped technologies generation technologies subject to the CER

6. Maximum annual emissions for the backup thermal plants
   
   ![](https://gitlab.com/sesit/copper/-/raw/a2ed342cf136ddeec228df27476a3e9bbe2a4b10/Documentation/images/Maximum_Annual_Emissions_Backup_Generation.jpg)

   Where *y* is the generation, *GHGI* the CO_2-equivalent emission intensity, *GHGC* the CO_2-equivalent emission cap, *EX* the extant capacity, *x* the generation caapacity, *n* the balancing area, *t* the period (year), *xr* the retired capacity, *h* the hour of the year, and *K^RCCS* the subset of CCS-equipped technologies generation technologies subject to the CER