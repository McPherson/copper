# Visualizing COPPER with IDEA

![IDEA dashboard visualizing COPPER results](ideaDashboard.png)

## Steps to Visualize

1. Follow the instructions to setup [IDEA from the SESIT repository](https://gitlab.com/sesit/idea-dash)
2. Once Idea is running in your browser, select Load data
3. In the Load data interface navigate to your copper results: .COPPER/results/{scenario name}/processed_results/{scenario name}_output_summary_IAMC_names.csv
4. Select the output_summary_IAMC_names.csv file for all scenarios that you want to visualize:
    for example:
    results\BAU_scenario_20240516_1216_Test\processed_results\BAU_scenario_output_summary_IAMC_names.csv
    - NOTE you can upload multiple scenarios to compare 
5. explore your results in visual form

