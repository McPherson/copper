# The Canadian Opportunities for Planning and Production of Electricity Resources (COPPER) model for Canada electricity system, national and provincial scale.
# Written by Reza Arjmand, Ph.D candidate at UVic (rezaarjmand@uvic.ca; rezaarjmand@yahoo.com). Linkedin: www.linkedin.com/in/rezaarjmand
# Please CITE this paper (Arjmand, R., & McPherson, M. (2022). Canada's electricity system transition under alternative policy scenarios. Energy Policy, April 2022)
# https://doi.org/10.1016/j.enpol.2022.112844.
# The following code also has contributions from the Government of Canada
# Copyright (c) 2023 Government of Canada
# COPPER-9.0: NRCan included code updates to treat firm contracts between provinces
# COPPER-9.0: NRCan included code updates related to the treatment of off-shore wind constraints and separation of inputs from the main COPPER code
# For more info: https://gitlab.com/sesit/copper
# Version 9.0

from pyomo.environ import (
    Constraint, ConcreteModel, Var, Binary,
    NonNegativeReals, Objective, minimize, Suffix, SolverFactory,
    SolverStatus, TerminationCondition, Set, quicksum
    )
import pandas as pd
import numpy as np
import os
import shutil
import gc
import time
import sys
import toml
import argparse
import re
import random
from datetime import datetime, timedelta
from phases.postprocessingtools import *
from phases.postprocessing import post_process_copper
from tools.COPPER_Cost import *
from tools.COPPER_tools import (init_input, get_VRE_cf, dataframe_processor)#, runday_generator)
from phases.preprocess_validation import pre_process_copper

########################################################
#####   COMMAND LINE ARGUMENTS AND INPUT FORMATTING ####
########################################################

# Command line arguments
arguments = argparse.ArgumentParser()
arguments.add_argument("-sc", "--scenario", dest="scenario", default="BAU_scenario", type=str,
                       help="Select the COPPER scenario")
#other valid options include glpk and cbc
arguments.add_argument("-so", "--solver", dest="solver", default="cplex", type=str,
                       help="Select the COPPER solver")
arguments.add_argument("-o", "--output", dest="output", default="results", type=str,
                       help="Select the Output folder")
arguments.add_argument("-t", "--test",  action='store_true',
                       help="Choose if COPPER is running on test mode, or as a full scenario run")
arguments.add_argument("-se", "--seed", dest="seed", default=None, type=int,
                       help="Set a seed to make results reproducible (only works with solver=glpk")
arguments.add_argument("-rd", "--runday", dest="run_days_check", default="false", type=str,
    help="Choose to create new rundays for your config file using your demand file")
# Functionality for argument above still needs to be implemented

args = arguments.parse_args()

########################################################

# Check if scenario came from command line
scenario = args.scenario
solver = args.solver
destination_folder = args.output
test_flag = args.test
run_days_check = args.run_days_check
seed = args.seed
seed_message = f'* Using Custom seed {seed}\n' if seed is not None else '* Using Randomized Seed\n'
print("\n==================================================\n" +
      "Running COPPER using the following set-up:\n" +
      f"* Scenario: {scenario}\n" +
      f"* Solver: {solver}\n" +
      f"* Output folder: {destination_folder}\n" +
      f"* Test mode is {test_flag}\n" +
      seed_message +
      "==================================================\n")

if test_flag:
    test = True
else:
    test = False

################# Pre-process #################
preprocess_start = time.time()
input_process = pre_process_copper(scenario)
input_process.config_validation()
input_process.input_validation()
if bool(re.search(r'(?i)^(true|t|True|TRUE)$', run_days_check)):
    input_process.runday_generator(demand_file=os.path.join(scenario + "\\demand.csv"), config_file=os.path.join(scenario + "\\config.toml"))
preprocess_end = time.time()
print(f'\n==================================================\n\
Input processing time: {round((preprocess_end - preprocess_start) / 60)} Min and {round((preprocess_end - preprocess_start) % 60)} Sec\
\n==================================================')
###############################################################

get_file = init_input(scenario)

os.chdir(os.path.join('scenarios', scenario))

# Starting timer
start = time.time()

# Load config file
config = toml.load('config.toml')

# Initializing all sets
### All regions
ap = config['Simulation_Settings']['ap']  # all provinces
aba = config['Simulation_Settings']['aba']  # all possible balancing areas
###auxiliary, just for creatin input files####
aba1 = config['Simulation_Settings']['aba1']
## model sub-periods which act as stepping points in the simulation to track the trajectory of the network configuration
pds = config['Simulation_Settings'][
    'pds']  # Also common to simulate with 5-year sub-periods - ['2025','2030','2035','2040','2045','2050']
season = config['Simulation_Settings']['season']
# Flag for user defined input demand
user_demand = config['Simulation_Settings']['user_specified_demand']

# Carbon tax using average tax between 2025-2029 = 143
ctax = config['Carbon']['ctax']

# load_shedding_cost [$/MWh]
load_shedding_cost = config['Economics']['load_shedding_cost']

### planning reserve data
reserve_margin_aggregate = config['Electrical_grid']['reserve_margin_aggregate']
reserve_margin_df = pd.read_csv(get_file('reserve_margin.csv'), header=None)
reserve_margin_df = reserve_margin_df.tail(-1)
reserve_margin_dict = {}
for index, row in reserve_margin_df.iterrows():
    reserve_margin_dict.update({row[0]: row[1]})

###percent of available hydro that can be upgraded to  store energy (retrofit to add pumped hydro storage)
pump_ret_limit = config['Hydro']['pump_ret_limit']
## discount rate expressed as a decimal
discount = config['Economics']['discount']
## inflation rate expressed as a decimal
inflation = config['Economics']['inflation']
## discount rate for year 2050 - 10x regular discount
discount_2050 = config['Economics']['discount_2050']
# flag for hydro development
hydro_development = config['Hydro']['hydro_development']
# Minimal flow of hydro installations
hydro_minflow = config['Hydro']['hydro_minflow']
# flag for autarky - a scenario of limited trade between balancing areas
autarky = config['Regulations']['autarky']
# flag for continuous storage
storage_continous = config['Storage']['storage_continuous']
# flag for provincial emission limit
provincial_emission_limit = config['Carbon']['provincial_emission_limit']
emission_limit_ref_year = config['Carbon']['emission_limit_ref_year']
# flag for national emission limit
national_emission_limit = config['Carbon']['national_emission_limit']
# dictionary of periods (keys) and their associated emission limits (values)
nat_em_limit = config['Carbon']['nat_em_limit']
## local or national natural gas price
local_gas_price = config['Carbon']['local_gas_price']
## OBPS flag
OBPS_on = config['Regulations']['OBPS_on']  # OBPS is always on
## GPS and CPO regulations
GPS = config['Regulations']['GPS']
CPO = config['Regulations']['CPO']
##constrained transmission expansion, constrains the tranmission expansion capacity to a coefficient of the current capacity
CTE_extant = config['Transmission']['CTE_extant']
## percent allowable increase in transmission beyond the original capacities (0 indicates a grid fixed to its input transmission capacities; 1 implies an allowable increase of 100% of the input transmission capacities)
CTE_coef = config['Transmission']['CTE_coef']
## flag for a custom transmission capacity scenario - user must include the trans_expansion_limits.csv file to submit input for transmission capacity constraints
CTE_custom = config['Transmission']['CTE_custom']
## input for custom transmission capacity constraints
transmission_data = dataframe_processor(pd.read_csv(get_file('extant_transmission.csv'), header=0), ap, prov_idx=0)
if CTE_custom:
    trans_expansion_limits = dict(transmission_data[['Transmission Line', 'Expansion_limits']].values)
    ## input for custom transmission minimum capacity constraints
    transexpansionminimum = dataframe_processor(pd.read_csv(get_file('trans_expansion_minimum_1000.csv'), header=None, nrows=30),
                                                ap, 
                                                prov_idx=0)
    trans_expansion_minimum = dict(transexpansionminimum.values)

## Thermal unit phase out, type:year
thermal_phase_out = config['Phase_out']['thermal_phase_out']
phase_out_type_year = config['Phase_out']['phase_out_type_year']
ph_out_t = list(phase_out_type_year.keys())
## just consider small hydro projects, under 100 MW
just_small_hydro = config['Hydro']['just_small_hydro']
## flag for non-emitting limit
non_emitting_limit = config['Electrical_grid']['non_emitting_limit']
## limit electrcity generation emission
nonemitting_limit = config['Electrical_grid']['nonemitting_limit']
## technology evolutiion level
technology_evolution_on = config['Economics']['technology_evolution_on']
## limited capacity addition of a generation type
limited_new_thermal_gen = config['Thermal']['limited_new_thermal_gen']
#sets the percentage of parasitic load as float value ranging from 0-.9999 this is applied to CCS thermal plants
parasitic_load = config['Thermal']['parasitic_load']
## renewable portfolio standards - flags for minimum and maximum capacities for wind and solar generation
renewable_portfolio_standards = config['Constraints']['renewable_portfolio_standards']
## input files for minimum and maximum capacities for wind and solar generation
renewable_portfolio_max = dataframe_processor(pd.read_csv(get_file('renewable_portfolio_max.csv'), index_col=0), ap, prov_idx=0)
renewable_portfolio_min = dataframe_processor(pd.read_csv(get_file('renewable_portfolio_min.csv'), index_col=0), ap, prov_idx=0)

wind_ons_max_ap_limit = renewable_portfolio_max.copy().query('Technology == "wind_ons"').drop(['Technology'], axis=1)
wind_ons_min_ap_limit = renewable_portfolio_min.copy().query('Technology == "wind_ons"').drop(['Technology'], axis=1)
wind_ofs_max_ap_limit = renewable_portfolio_max.copy().query('Technology == "wind_ofs"').drop(['Technology'], axis=1)
wind_ofs_min_ap_limit = renewable_portfolio_min.copy().query('Technology == "wind_ofs"').drop(['Technology'], axis=1)
solar_max_ap_limit = renewable_portfolio_max.copy().query('Technology == "solar"').drop(['Technology'], axis=1)
solar_min_ap_limit = renewable_portfolio_min.copy().query('Technology == "solar"').drop(['Technology'], axis=1)
### flag for nuclear capacity constraints by balancing area
max_nuclear_aba = config['Constraints']['max_nuclear_aba']
### flag for SMR capacity constraints by balancing area
max_smr_aba = config['Constraints']['max_smr_aba']
### flag for biomass capacity constraints by balancing area
max_bio_aba = config['Constraints']['max_bio_aba']
## flag for aggregated reserve margin
flag_reserve_aggregated = config['Electrical_grid']['flag_reserve_aggregated']

###No CCS except for AB and SK
no_CCS_aba = config['Carbon']['no_CCS_aba']
###CCS technologies
CCS_tech = config['Carbon']['CCS_tech']
###CCS technologies
cg_tech = config['Carbon']['cg_tech']
#Cogeneration Rate
cogeneration_rate = config['Carbon']['cogeneration_rate']
## CCS in 2035
flag_noNewCCS_2035 = config['Carbon']['flag_noNewCCS_2035']

### CER regulation block ###

## max emissions limit for CER
max_em_limit = config['Regulations']['CER']['max_em_limit']
## flag for Clean Electricity Regulations (CER)
flag_cer = config['Regulations']['CER']['flag_cer']
## backup generation technologies
backup_generators = config['Regulations']['CER']['backup_generators']
## retired generation technologies
retired_generators = config['Regulations']['CER']['retired_generators']
## pre2025 fossil emitting thermal technologies
pre2025_generators = config['Regulations']['CER']['pre2025_generators']

## flag for Investment Tax Credit (ITC)
flag_itc = config['Economics']['flag_itc']
## ITC factor for 15% discount on cap cost
factor_itc = config['Economics']['factor_itc']

# flag for disaggregating generators in postprocessing
flag_disaggregate_generators = config['Disaggregation_Settings']['flag_disaggregate_generators']

# Initializing all sets
### All regions
ap = config['Simulation_Settings']['ap']  # all provinces
aba = config['Simulation_Settings']['aba']  # all possible balancing areas
# ensure alignment between these balancing areas
no_CCS_aba = list(set(no_CCS_aba).intersection(aba))
###auxiliary, just for creatin input files####
aba1 = config['Simulation_Settings']['aba1']
## model sub-periods which act as stepping points in the simulation to track the trajectory of the network configuration
pds = config['Simulation_Settings'][
    'pds']  # Also common to simulate with 5-year sub-periods - ['2025','2030','2035','2040','2045','2050']
num_pds = len(pds)  # Kenzie added, used for postprocessing
season = config['Simulation_Settings']['season']
# Flag for user defined input demand
user_demand = config['Simulation_Settings']['user_specified_demand']

regions_per_province = {AP: [ABA for ABA in aba if AP in ABA] for AP in ap}
provinces_per_region = {ABA: [AP for AP in ap if AP in ABA] for ABA in aba}

capacity_val = pd.read_csv(get_file('wind_solar_capacity_value.csv'), header=None)
header_A = list(capacity_val.iloc[0, 1:])
header_B = list(capacity_val.iloc[1, 1:])

ind = list(capacity_val.iloc[2:, 0])

capacity_value = pd.DataFrame(np.array(capacity_val.loc[2:12, 1:5]),
                              columns=pd.MultiIndex.from_tuples(zip(header_A, header_B)), index=ind)
# capacity_val_inds = config['Simulation_Settings']['capacity_val_inds']
# solar_wind_capacity = {TECH+'.'+SEASON+'.'+ABA:float(capacity_value.values[ap.index(provinces_per_region[ABA][0])][capacity_val_inds.index(SEASON+'.'+TECH)]) for ABA in aba for SEASON in season for TECH in ['wind','solar'] }

tech_evolution = pd.read_csv(get_file('technology_evolution.csv'), header=0, index_col=0)
technology_evolution = {}
gen_tech_data = pd.read_csv(get_file('generation_type_data.csv'), header=0)
allplants_temp = list(gen_tech_data.iloc[:]['Type'])
for PD in pds:
    for GT in allplants_temp:
        if technology_evolution_on['base']:
            technology_evolution[GT + '.' + PD] = tech_evolution[PD][
                GT + '.' + 'base']  ### canada energy future 2020 base scenario
        elif technology_evolution_on['evolving']:
            technology_evolution[GT + '.' + PD] = tech_evolution[PD][GT + '.' + 'evolving']
        else:
            technology_evolution[GT + '.' + PD] = 1

# reserve_margin_tech = pd.read_csv(get_file('capacity_value.csv'),header=0)
# reserve_margin_tech_type = list(reserve_margin_tech['Type'])
# reserve_margin_tech_raw_dict = {AP:reserve_margin_tech[AP] for AP in ap}

# reserve_margin_tech_dict = {TECH+'.'+AP:reserve_margin_tech_raw_dict[AP][reserve_margin_tech_type.index(TECH)] for TECH in reserve_margin_tech_type for AP in ap}

# Definition of the qualifying capacity for winter and summer (wind and solar)
# qualifing_capacity_summer = reserve_margin_tech_dict.copy()
# qualifing_capacity_winter = reserve_margin_tech_dict.copy()

# for cap_renewal in [key for key in solar_wind_capacity.keys() if 'summer' in key]:
#    qualifing_capacity_summer.update({cap_renewal:solar_wind_capacity[cap_renewal]})

# for cap_renewal in [key for key in solar_wind_capacity.keys() if 'winter' in key]:
#    qualifing_capacity_winter.update({cap_renewal:solar_wind_capacity[cap_renewal]})

#### gas PHP installed capacity limit
LB_PHP_installed_limit = {}
for ABA in aba:
    LB_PHP_installed_limit[ABA] = 0

# reads in run days for simulation from config file
if test:
    run_days = config['Simulation_Settings']['run_days_test']
else:
    run_days = config['Simulation_Settings']['run_days']
rundays = run_days
rundays = [int(RD) for RD in rundays]
cap_cost_alter = (365 / len(rundays))

##### Generation fleets data

gendata = pd.read_csv(get_file('generation_type_data.csv'), header=0)

#####Storage dataol(x) -
# storage technology
st = config['Storage']['st']
storage_hours = config['Storage']['storage_hours']
# some data is pulled from input sheet and put into a dictionary by type
storage_data = {
    s_type: {
        'capitalcost': gendata[gendata['Type'] == s_type]['capitalcost'].iloc[0],
        'fixed_o_m': gendata[gendata['Type'] == s_type]['fixed_o_m'].iloc[0],
        'efficiency': gendata[gendata['Type'] == s_type]['efficiency'].iloc[0]
    }
    for s_type in st}
# capital and efficiency are isolated from the above dictionary and put into more specific dictionaries
storage_capital = {s_type: storage_data[s_type]['capitalcost'] for s_type in st}
storage_efficiency = {s_type: storage_data[s_type]['efficiency'] for s_type in st}
# storage cost is calculated considering technology evolution
storage_cost = {}
for PD in pds:
    for ST in st:
        key = f'{ST}.{PD}'
        if key in technology_evolution:
            storage_cost[key] = storage_capital[ST] * technology_evolution[key]
        else:
            storage_cost[key] = storage_capital[ST]
# fixed operation and maintenance cost of storage technologies is put into dictionaries for each year.
ph_fix_o_m = storage_data['storage_PH']['fixed_o_m']
li_fix_o_m = storage_data['storage_LI']['fixed_o_m']
store_fix_o_m = {'storage_PH.2030': ph_fix_o_m, 'storage_PH.2040': ph_fix_o_m, 'storage_PH.2050': ph_fix_o_m,
                 'storage_LI.2030': li_fix_o_m, 'storage_LI.2040': li_fix_o_m, 'storage_LI.2050': li_fix_o_m,
                 'storage_PH.2025': ph_fix_o_m, 'storage_PH.2035': ph_fix_o_m, 'storage_PH.2045': ph_fix_o_m,
                 'storage_LI.2025': li_fix_o_m, 'storage_LI.2035': li_fix_o_m, 'storage_LI.2045': li_fix_o_m}

runhours = rundays[-1] * 24
foryear = config['Simulation_Settings']['forecast_year']
refyear = config['Simulation_Settings']['reference_year']

## converts GJ to MWh
GJtoMWh = 3.6
autonomy_pct = config['Regulations']['autonomy_pct']
# share carbon reduced
carbon_reduction = config['Carbon']['carbon_reduction']
# reference case carbon emissions in electricity sector in 2005 in Mt by province (source: https://www.cer-rec.gc.ca/en/data-analysis/energy-markets/provincial-territorial-energy-profiles/provincial-territorial-energy-profiles-explore.html)
carbon_2005_ref = {"British Columbia": 1.04, "Alberta": 48.83, "Saskatchewan": 14.82, "Manitoba": .36, "Ontario": 33.9,
                   "Quebec": 0.65, "New Brunswick": 7.8, "Newfoundland and Labrador": 0.82, "Nova Scotia": 10.77,
                   "Prince Edward Island": 0}

# reference case carbon emissions in electricity sector in 2017 in Mt by province (source: https://www.cer-rec.gc.ca/en/data-analysis/energy-markets/provincial-territorial-energy-profiles/provincial-territorial-energy-profiles-explore.html)
carbon_2017_ref = {"British Columbia": 0.15, "Alberta": 44.33, "Saskatchewan": 15.53, "Manitoba": .07, "Ontario": 1.99,
                   "Quebec": 0.26, "New Brunswick": 3.65, "Newfoundland and Labrador": 1.53, "Nova Scotia": 6.5,
                   "Prince Edward Island": 0.01}
# maximum carbon emissions in the target year in Mt
carbon_limit = {}
if emission_limit_ref_year == 2017:
    for AP in ap:
        carbon_limit[AP] = carbon_2017_ref[AP] * (1 - carbon_reduction)

elif emission_limit_ref_year == 2005:
    for AP in ap:
        carbon_limit[AP] = carbon_2005_ref[AP] * (1 - carbon_reduction)

h = list(range(1, 8761))

gridcell_data = dataframe_processor(pd.read_csv(get_file('gridcells.csv')), ap, prov_idx=4)

map_gl_to_ba = dict(gridcell_data[['grid_cell', 'ba']].values)  # map grid locations to balancing areas
map_gl_to_pr = dict(gridcell_data[['grid_cell', 'pr']].values)  # map grid locations to provinces

allplants = list(gendata['Type'])

tplants = gendata.loc[gendata["Is thermal?"], "Type"].to_list()

ITCplants = list()
if flag_itc:
    isITCsupport = list(gendata['ITC support?'])
    cc = 0
    for i in isITCsupport:
        if i:
            ITCplants.append(allplants[cc])
        cc += 1
# Fossil-emitting thermal plants with emission intensities over the limit
ftplants = list()
fuel_co2 = list(gendata['fuel_co2'])
fuel_co2_obps_2025 = list(gendata['fuel_co2_obps_2025'])
fuel_co2_obps_2030 = list(gendata['fuel_co2_obps_2030'])

tplant_fuel_co2_dict_cer = {tplant: fuel_co2[allplants.index(tplant)] for tplant in allplants}

tplant_fuel_co2_dict_obps_2025 = {tplant: fuel_co2_obps_2025[allplants.index(tplant)] for tplant in allplants}

tplant_fuel_co2_dict_obps_2030 = {tplant: fuel_co2_obps_2030[allplants.index(tplant)] for tplant in allplants}

cc = 0
for i in fuel_co2:
    if (i > 0.0) and (allplants[cc] not in pre2025_generators):  # taken from Jake's CER update
        ftplants.append(allplants[cc])
    cc += 1

## set of non-emitting generation technologies
non_emitting_tplants = list()
non_emitting = list(gendata['non-emitting thermal?'])
cc = 0
for i in non_emitting:
    if i:
        non_emitting_tplants.append(allplants[cc])
    cc += 1

## for linkage feedback
min_installed_LB_PHP = config['Storage']['min_installed_LB_PHP']
## new thermal unit restriction, types
new_thermal_limit = config['Thermal']['new_thermal_limit']
## list of thermal generation technologies that are restricted for new development
old_limited_tplants = config['Thermal']['old_limited_tplants']
limited_tplants = [tplant for tplant in tplants for category in old_limited_tplants if category in tplant]

## Reading thermal units specifications
max_cap_fact = dict(zip(list(gendata['Type']), list(gendata[
                                                        'max_cap_fact'])))  # {'gasCC':0.8, 'gasSC': .3, 'nuclear': 0.95, 'coal': 0.9, 'diesel': 0.95, 'biomass': 0.9} #annual maximum capacity factor
min_cap_fact = dict(zip(list(gendata['Type']), list(gendata[
                                                        'min_cap_fact'])))  # {'gasCC': 0.2, 'gasSC': .02, 'nuclear': 0.75,  'coal': 0.5, 'diesel': 0.05, 'biomass': 0.2} #annual minimum capacity factor
ramp_rate_percent = dict(zip(list(gendata['Type']), list(gendata[
                                                             'ramp_rate_percent'])))  # {'gasCC': 0.1, 'gasSC':0.1 , 'nuclear': 0.05,  'coal': 0.05, 'diesel': 0.1, 'biomass': 0.05} #ramp rate in percent of capacity per hour
efficiency = dict(zip(list(gendata['Type']), list(gendata[
                                                      'efficiency'])))  # {'gasCC': 0.509, 'gasSC': 0.28, 'nuclear': 0.327, 'coal': 0.39, 'diesel': 0.39, 'biomass': 0.39}

### Reading cost data
fixed_o_m = dict(zip(list(gendata['Type']), list(gendata['fixed_o_m'])))  # (fixedom.values)

variable_o_m = dict(zip(list(gendata['Type']), list(gendata['variable_o_m'])))  # dict(variableom.values)
fuelprice = dict(zip(list(gendata['Type']), list(gendata['fuelprice'])))  # dict(fuel_price.values)

capitalcost1 = dict(zip(list(gendata['Type']), list(gendata['capitalcost'])))  # dict(capital_cost.values)
if technology_evolution_on['evolving']:
    capitalcost1['nuclear_SMR'] = 629574  # TODO: Move this to gen_type_data?
capitalcost = {}
for ABA in aba:
    for G in capitalcost1:
        capitalcost[ABA + '.' + G] = capitalcost1[G]

if config["Regulations"]["GPS"]:
    for ABA in aba:
        capitalcost[ABA + '.gasSC'] = 1000000000
        capitalcost[ABA + '.coal'] = 1000000000

#### Transmission sytem costs
trans_o_m = config['Transmission']['trans_o_m']
transcost = config['Transmission']['transcost']
intra_ba_transcost = config['Transmission']['intra_ba_transcost']

#gridlocations = list(range(1, 2984))
gl = [str(GL) for GL in gridcell_data['grid_cell']]

distance_to_grid = dict(gridcell_data[['grid_cell', 'distance_to_grid']].values)
windonscost = {}
windofscost = {}
solarcost = {}
redundant_gls = []
for PD in pds:
    for GL in gl:
        # if selected balancing areas are modified in the config, we ignore the missing pieces        
        if map_gl_to_ba[int(GL)] not in aba:    
            redundant_gls.append(GL)
            continue        
        windonscost[f'{PD}.{GL}'] = capitalcost[map_gl_to_ba[int(GL)] + '.wind_ons'] * technology_evolution[
            f'wind_ons.{PD}'] + distance_to_grid[int(GL)] * intra_ba_transcost
        windofscost[f'{PD}.{GL}'] = capitalcost[map_gl_to_ba[int(GL)] + '.wind_ofs'] * technology_evolution[
            f'wind_ofs.{PD}'] + distance_to_grid[int(GL)] * intra_ba_transcost
        solarcost[f'{PD}.{GL}'] = capitalcost[map_gl_to_ba[int(GL)] + '.solar'] * technology_evolution[f'solar.{PD}'] + \
                                  distance_to_grid[int(GL)] * intra_ba_transcost

for r_gl in redundant_gls:
    if r_gl in gl:
        gl.remove(r_gl)

# thermal plant co2 emissions in tonne per MWh of electricity generated
carbondioxide = dict(zip(list(gendata['Type']), list(gendata['fuel_co2'])))
# thermal plant co2 emissions in tonne per MWh of electricity generated
carbondioxide_obps_2025 = dict(zip(list(gendata['Type']), list(gendata['fuel_co2_obps_2025'])))
# thermal plant co2 emissions in tonne per MWh of electricity generated
carbondioxide_obps_2030 = dict(zip(list(gendata['Type']), list(gendata['fuel_co2_obps_2030'])))

#### Adding carbon price to the fuelcost, fuelcost=fuelprice + carbonprice ($/MWh)
## ref CERI study 168 table 3.11 from Statistics Canada (CANSIM Table 129-0003 Sales of natural gas, annual)
if local_gas_price:
    gasprice = config['Thermal']['gasprice']
else:
    gasprice = {}
    for ABA in aba:
        gasprice[ABA] = fuelprice['gasCC_backup_post2025'] #sets gas price to 2.94 or the value updated in generation_type_data.csv

# Calculation of fuel cost in function of OBPS and CER regulations
fuelcost = energy_cost_per_pd_aba(
    config, tplants, gendata, gasprice, ctax, carbondioxide, carbondioxide_obps_2025, carbondioxide_obps_2030
)

max_development = dataframe_processor(pd.read_csv(get_file('max_capacity_limits.csv')), ap, prov_idx=0)
### Reading input file for nuclear development (By balancing area)
max_nuclear = dict(max_development[['ABA', 'MAX_nuclear']].values)

### Reading input file for SMR development (By balancing area)
max_smr = dict(max_development[['ABA', 'MAX_SMR']].values)

### Reading input file for biomass development (By balancing area)
max_bio = dict(max_development[['ABA', 'MAX_bio']].values)

### Reading transmission expansion routes
transmap = list(transmission_data['Transmission Line'].values)

distance = dict(transmission_data[['Transmission Line', 'Distance']].values)

### Reading extant wind and solar data
extantwindsolar = pd.read_csv(get_file('extant_wind_solar.csv'))
extant_wind_solar_raw = extantwindsolar.copy()

extant_wind_solar = list()
for PD in pds:
    extant_wind_solar.append(dict(extantwindsolar[['location', f'{PD}']].values))

#aggregates the capacity of existing vre by province and vre type
extantwindsolar = extantwindsolar.reset_index(drop=True)
extantwindsolar[["gl", "type"]] = extantwindsolar["location"].str.split(".", expand=True)
extantwindsolar["gl"] = extantwindsolar["gl"].astype(int)
extantwindsolar["aba"] = extantwindsolar["gl"].apply(map_gl_to_ba.get)

### Reading extant generation capacity data
#TODO update CER file to be just extant_capacity_CER.csv
if flag_cer:
    try:
            extantcapacity = pd.read_csv(get_file('extant_capacity_CER_20_years.csv'), header=0)
            print("\nCER applied. Updated extant capacity file to extant_capacity_CER_20_years.csv")
    except FileNotFoundError:
        print(
            "You are trying to run a CER scenario, but the extant capacity file extant_capacity_CER_20_years.csv was not found! Check the scenario again")
        sys.exit()
else:
    extantcapacity = pd.read_csv(get_file('extant_capacity.csv'), header=0)
extantcapacity = dataframe_processor(extantcapacity, ap, prov_idx=0)
extant_capacity_raw = extantcapacity.copy()
extant_capacity = {}
for PD in pds:
    label_ABA = list(extantcapacity['ABA'])
    label_ABA = [PD + '.' + LL for LL in label_ABA]
    excap = dict(zip(label_ABA, list(extantcapacity[PD])))
    extant_capacity.update(excap)

# These dataframes are used never used
# periods_capacity_diffrance = extantcapacity[:]

# #calculates the difference in capacity by generation type and balancing area by periods where a decrease in capacity is a positive number 
# #so if 2025 solar is 30 and 2030 solar is 20, the difference is 10 and the value in 2030 is 10
# for index, row in periods_capacity_diffrance.iterrows():
#     for PD_index in reversed(range(7)):
#         if PD_index != 0 and PD_index != 1:
#             Capicity_table_entry = periods_capacity_diffrance.iat[index, PD_index]
#             periods_capacity_diffrance.iat[index, PD_index] = periods_capacity_diffrance.iat[index, PD_index - 1] - \
#                                                               periods_capacity_diffrance.iat[index, PD_index]
#     periods_capacity_diffrance.iat[index, 1] = 0

# periods_capacity_diffrance_dict = {}
# for PD in periods_capacity_diffrance.columns[1:]:
#     label_ABA = list(periods_capacity_diffrance['ABA'])
#     label_ABA = [PD + '.' + LL for LL in label_ABA]
#     excap = dict(zip(label_ABA, list(periods_capacity_diffrance[PD])))
#     periods_capacity_diffrance_dict.update(excap)

### Reading extant tranmission lines capacity data
extanttrans = transmission_data.copy()
extanttrans = extanttrans.drop(['Distance', 'Expansion_limits'], axis=1)
extant_transmission = list()
for PD in pds:
    extant_transmission.append(dict(zip(list(extanttrans['Transmission Line']), list(extanttrans[PD]))))
del extanttrans

##### Reading hydro units capacity factor data
hydrocf = dataframe_processor(pd.read_csv(get_file('hydro_cf.csv')), ap, prov_idx=0)
hydro_cf = dict(hydrocf.values)

## ### Reading demand related data
demand_growth = pd.read_csv(get_file('annual_growth.csv'), header=0, index_col=0)

##### Reading population data to disaggregate demand data
population = dict(gridcell_data[['grid_cell', 'population']].values)
population = {k: v for k, v in population.items() if pd.Series(v).notna().all()}

demand_us = dict(pd.read_csv(get_file('us_demand.csv')).values)

##### Converting hours to days/months
start_date = datetime(refyear, 1, 1, 0, 0, 0)
year_hours = [start_date]
for i in range(1, 8760):
    start_date += timedelta(seconds=3600)
    year_hours.append(start_date)
year_hours = pd.DataFrame({'dates': year_hours})
year_hours['hour'] = year_hours.index + 1
year_hours['day'] = (year_hours['hour'] - 1) // 24 + 1
year_hours['month'] = year_hours['dates'].dt.month

map_hd = dict(year_hours[['hour', 'day']].values)
map_hm = dict(year_hours[['hour', 'month']].values)

surface_area_solar = dict(gridcell_data[['grid_cell', 'surface_area_ons']].values)
surface_area_wind_ons = dict(gridcell_data[['grid_cell', 'surface_area_ons']].values)
surface_area_wind_ofs = dict(gridcell_data[['grid_cell', 'surface_area_ofs']].values)

demand_all = dict()
## Calculating seasonal peak demand
peak_demand = {}
peak_days = {}
province_demand = {}
for AP in ap:
    province_demand[AP] = np.zeros((len(h), len(pds)))

if user_demand:
    demand_piv = pd.read_csv(get_file('user_input_demand.csv'), sep=',', index_col=0, header=0)  # User specified demand
else:
    demand_piv = pd.read_csv(get_file('demand.csv'), index_col=0)  # Pivoted demand

time_idx = pd.MultiIndex.from_product([pds, h], names=["period", "hour"])
all_demand_df = pd.DataFrame(index=time_idx, columns=ap)

last_year = refyear

growth_factor = ((1 + demand_growth) ** (demand_growth.columns.astype(int) - last_year)).reindex(ap)
demand_aggregate = pd.DataFrame(columns = pds)
for PD in pds:
    gf = growth_factor[PD]
    demand_aggregate[PD] = (demand_piv[ap] * gf).sum(axis=1)

demand_aggregate_summer = {}
demand_aggregate_winter = {}
peak_demand_aggregate = {}

for PD in pds:
    for H in h:
        if H < 2160 or H > 6480:
            demand_aggregate_winter[PD + '.' + str(H)] = demand_aggregate[PD][H-1]
        else:
            demand_aggregate_summer[PD + '.' + str(H)] = demand_aggregate[PD][H-1]
    max_demand_summer = 0
    max_demand_hour_summer = 0
    max_demand_winter = 0
    max_demand_hour_winter = 0
    for H in h:
        if H < 2160 or H > 6480:
            if demand_aggregate_winter[PD + '.' + str(H)] > max_demand_winter:
                max_demand_winter = demand_aggregate_winter[PD + '.' + str(H)]
                max_demand_hour_winter = H
        else:
            if demand_aggregate_summer[PD + '.' + str(H)] > max_demand_summer:
                max_demand_summer = demand_aggregate_summer[PD + '.' + str(H)]
                max_demand_hour_summer = H
    peak_demand_aggregate[PD + '.' + 'winter'] = max_demand_winter
    peak_demand_aggregate[PD + '.' + 'summer'] = max_demand_summer

# Change this loop to allow demand input selection from the user [Aaron's request]
if not user_demand:
    for PD in pds:
        for AP in ap:
            if not user_demand:
                if PD == pds[0]:
                    all_demand_df.loc[PD, AP] = (
                                demand_piv[AP] * (1 + demand_growth[PD][AP]) ** (int(PD) - last_year)).values
                else:
                    all_demand_df.loc[PD, AP] = (
                                all_demand_df.loc[str(last_year), AP] * (1 + demand_growth[PD][AP]) ** (
                                    int(PD) - last_year)).values
            else:
                break
            province_demand[AP][:, pds.index(PD)] = all_demand_df.loc[PD, AP]

            peak_demand[PD + '.' + 'winter' + '.' + AP] = max(max(province_demand[AP][:2160, pds.index(PD)]),
                                                              max(province_demand[AP][6480:8760, pds.index(PD)]))
            peak_demand[PD + '.' + 'summer' + '.' + AP] = max(province_demand[AP][2160:6480, pds.index(PD)])

        last_year = int(PD)
else:
    demand_piv.columns = map(str.lower, demand_piv.columns)
    demand_header = list(map(lambda x: x.lower(), list(demand_piv.columns)))
    for PD in pds:
        for AP in ap:
            if str(AP).lower() + "." + str(PD).lower() in demand_header:
                all_demand_df.loc[PD, AP] = demand_piv[str(AP).lower() + "." + str(PD).lower()].values

            province_demand[AP][:, pds.index(PD)] = all_demand_df.loc[PD, AP]

            peak_demand[PD + '.' + 'winter' + '.' + AP] = max(max(province_demand[AP][:2160, pds.index(PD)]),
                                                              max(province_demand[AP][6480:8760, pds.index(PD)]))
            peak_demand[PD + '.' + 'summer' + '.' + AP] = max(province_demand[AP][2160:6480, pds.index(PD)])

del h[runhours:]
hours = len(h)

nummonths = map_hm[rundays[-1] * 24]
m = list(range(1, nummonths + 1))

d = rundays.copy()
h3 = h.copy()
for H in h3:
    if map_hd[H] not in rundays:
        h.remove(H)

del h3
hours = len(h)
h2 = h.copy()
del h2[hours - 1]


[windcf_df, meanwincf, meanwincf_reduced] = get_VRE_cf(h, 'windcf.csv', get_file)
[solarcf_df, meansolarcf, meansolarcf_reduced] = get_VRE_cf(h, 'solarcf.csv', get_file)

##calculate the difference between hours in a row
time_diff = {}
for I in list(range(len(h) - 1)):
    time_diff[h[I]] = h[I + 1] - h[I]

gl2 = gl.copy()
for GL in gl2:
    if map_gl_to_pr[int(GL)] not in ap:
        gl.remove(GL)

###maximum wind and solar capacity that can be installed in each grid cell in MW per square km
maxwindonsperkmsq = config['Constraints']['maxwind_ons_perkmsq']  # 2
maxwindofsperkmsq = config['Constraints'][
    'maxwind_ofs_perkmsq']  # 2 -I feel like this number should be changed in config for offshore... -JGM
maxsolarperkmsq = config['Constraints']['maxsolarperkmsq']  # 31.28
maxwindons = {}
maxwindofs = {}
maxsolar = {}
for GL in gl:
    maxwindons[GL] = surface_area_wind_ons[int(GL)] * maxwindonsperkmsq
    maxwindofs[GL] = surface_area_wind_ofs[int(GL)] * maxwindofsperkmsq
    maxsolar[GL] = surface_area_solar[int(GL)] * maxsolarperkmsq

## Extant PHS data
extantstorage = pd.read_csv(get_file('extant_storage.csv'), header=0)
extantstorage = dataframe_processor(extantstorage, ap, prov_idx=0)
ba_storage_capacity = dict(zip(extantstorage['ABA'], extantstorage['current']))

# Calculating transmission loss coefficients
translossfixed = config['Transmission']['translossfixed']  # 0.02
translossperkm = config['Transmission']['translossperkm']  # 0.00003
transloss = {}
for ABA in aba:
    for ABBA in aba:
        if ABA + '.' + ABBA in distance:
            transloss[ABA + '.' + ABBA] = distance[ABA + '.' + ABBA] * translossperkm + translossfixed
## disaggregating demand based on population
populationaba = {}
for ABA in aba:
    populationaba[ABA] = 0
for ABA in aba:
    for GL in population:
        if map_gl_to_ba[int(GL)] == ABA:
            populationaba[ABA] = populationaba[ABA] + population[GL]
populationap = {}
demand = {}
demand_df = pd.DataFrame(index=pd.MultiIndex.from_product([pds, h]), columns=aba)

for PD in pds:
    for AP in ap:
        populationap[AP] = sum(populationaba[ABA] for ABA in aba if AP in ABA)
    for ABA in aba:
        pvba = ABA.replace('.a', '')
        pvba = pvba.replace('.b', '')
        demand_df.loc[PD, ABA] = (
                    all_demand_df.query("hour in @h").loc[PD, pvba] * (populationaba[ABA] / populationap[pvba])).values

## Creating extant generation dict
extant_thermal = {}
for AP in ap:
    for ABA in aba1:
        for TP in tplants:
            for PD in pds:
                extant_thermal[PD + '.' + AP + '.' + ABA + '.' + TP] = 0
                if PD + '.' + AP + '.' + ABA + '.' + TP in extant_capacity:
                    extant_thermal[PD + '.' + AP + '.' + ABA + '.' + TP] = extant_capacity[
                        PD + '.' + AP + '.' + ABA + '.' + TP]
                if CPO and TP == 'coal' and int(PD) >= 2030:
                    extant_thermal[PD + '.' + AP + '.' + ABA + '.' + TP] = 0

hydro_capacity = {}

# calculate max renewable generation


extant_solar_gen = pd.DataFrame(index=pd.MultiIndex.from_product([pds, h]), columns=aba, dtype=float).fillna(0)
extant_wind_ofs_gen = pd.DataFrame(index=pd.MultiIndex.from_product([pds, h]), columns=aba, dtype=float).fillna(0)
extant_wind_ons_gen = pd.DataFrame(index=pd.MultiIndex.from_product([pds, h]), columns=aba, dtype=float).fillna(0)

for PD in pds:
    for ABA in aba:
        aba_gl_solar_caps = extantwindsolar.query(f"type=='solar' and aba=='{ABA}'").set_index("gl")[PD]
        max_gen = solarcf_df.loc[:, aba_gl_solar_caps.index] @ aba_gl_solar_caps
        extant_solar_gen.loc[PD, ABA] = max_gen.values

        aba_gl_windons_caps = extantwindsolar.query(f"type=='wind_ons' and aba=='{ABA}'").set_index("gl")[PD]
        max_gen = windcf_df.loc[:, aba_gl_windons_caps.index] @ aba_gl_windons_caps
        extant_wind_ons_gen.loc[PD, ABA] = max_gen.values

        # this is currently not existing, i.e. no "wind_ofs" in extant_wind.csv
        aba_gl_windofs_caps = extantwindsolar.query(f"type=='wind_ofs' and aba=='{ABA}'").set_index("gl")[PD]
        max_gen = windcf_df.loc[:, aba_gl_windofs_caps.index] @ aba_gl_windofs_caps
        extant_wind_ofs_gen.loc[PD, ABA] = max_gen.values
        pass

## calculating hydro CF for different type of hydro power plant
ror_hydroout = {}
day_hydroout = {}
month_hydroout = {}
day_hydro_historic = {}
month_hydro_historic = {}
ror_hydro_capacity = {}
day_hydro_capacity = {}
month_hydro_capacity = {}
day_minflow = {}
month_minflow = {}
for PD in pds:
    for AP in ap:
        for ABA in aba1:
            ror_hydro_capacity[f'{PD}.' + AP + '.' + ABA] = 0
            day_hydro_capacity[f'{PD}.' + AP + '.' + ABA] = 0
            month_hydro_capacity[f'{PD}.' + AP + '.' + ABA] = 0
            if f'{PD}.' + AP + '.' + ABA + '.' + 'hydro_run' in extant_capacity:
                ror_hydro_capacity[f'{PD}.' + AP + '.' + ABA] = extant_capacity[
                    f'{PD}.' + AP + '.' + ABA + '.' + 'hydro_run']
                day_hydro_capacity[f'{PD}.' + AP + '.' + ABA] = extant_capacity[
                    f'{PD}.' + AP + '.' + ABA + '.' + 'hydro_daily']
                month_hydro_capacity[f'{PD}.' + AP + '.' + ABA] = extant_capacity[
                    f'{PD}.' + AP + '.' + ABA + '.' + 'hydro_monthly']
            day_minflow[f'{PD}.' + AP + '.' + ABA] = day_hydro_capacity[f'{PD}.' + AP + '.' + ABA] * hydro_minflow
            month_minflow[f'{PD}.' + AP + '.' + ABA] = month_hydro_capacity[f'{PD}.' + AP + '.' + ABA] * hydro_minflow
            for D in d:
                day_hydro_historic[f'{PD}.' + str(D) + '.' + AP + '.' + ABA] = 0

            for M in m:
                month_hydro_historic[f'{PD}.' + str(M) + '.' + AP + '.' + ABA] = 0
            for H in h:

                if AP + '.' + str(H) in hydro_cf:
                    ror_hydroout[f'{PD}.' + str(H) + '.' + AP + '.' + ABA] = ror_hydro_capacity[
                                                                                 f'{PD}.' + AP + '.' + ABA] * hydro_cf[
                                                                                 AP + '.' + str(H)]
                    day_hydroout[f'{PD}.' + str(H) + '.' + AP + '.' + ABA] = day_hydro_capacity[
                                                                                 f'{PD}.' + AP + '.' + ABA] * hydro_cf[
                                                                                 AP + '.' + str(H)]
                    month_hydroout[f'{PD}.' + str(H) + '.' + AP + '.' + ABA] = month_hydro_capacity[
                                                                                   f'{PD}.' + AP + '.' + ABA] * \
                                                                               hydro_cf[AP + '.' + str(H)]
                else:
                    ror_hydroout[f'{PD}.' + str(H) + '.' + AP + '.' + ABA] = 0
                    day_hydroout[f'{PD}.' + str(H) + '.' + AP + '.' + ABA] = 0
                    month_hydroout[f'{PD}.' + str(H) + '.' + AP + '.' + ABA] = 0

                day_hydro_historic[f'{PD}.' + str(map_hd[H]) + '.' + AP + '.' + ABA] = day_hydro_historic[
                                                                                           f'{PD}.' + str(map_hd[
                                                                                                              H]) + '.' + AP + '.' + ABA] + \
                                                                                       day_hydroout[f'{PD}.' + str(
                                                                                           H) + '.' + AP + '.' + ABA]
                month_hydro_historic[f'{PD}.' + str(map_hm[H]) + '.' + AP + '.' + ABA] = month_hydro_historic[
                                                                                             f'{PD}.' + str(map_hm[
                                                                                                                H]) + '.' + AP + '.' + ABA] + \
                                                                                         month_hydroout[f'{PD}.' + str(
                                                                                             H) + '.' + AP + '.' + ABA]

hydro_new = pd.read_csv(get_file('hydro_new.csv'), header=0)
## hydro renewal and greenfield development necessary inputs
# Depending on storage levels for hydro, different files can be read -CF
if hydro_development:
    if storage_continous and not just_small_hydro:
        hydro_new = pd.read_csv(get_file('hydro_new.csv'), header=0)
    elif not just_small_hydro:
        hydro_new = pd.read_csv(get_file('hydro_new.csv'), header=0)
    elif just_small_hydro:
        hydro_new = pd.read_csv(get_file('hydro_new.csv'), header=0)

    hydro_new = dataframe_processor(hydro_new, ap, prov_idx=2)

    hydro_renewal = list(hydro_new['Short Name'])
    cost_renewal = dict(zip(hydro_renewal, list(hydro_new.iloc[:][
                                                    'Annualized Capital Cost ($M/year)'])))  ##For some reason this was taken out of hydro_new.csv -JGM 24 MAY 2023
    capacity_renewal = dict(zip(hydro_renewal, list(hydro_new.iloc[:]['Additional Capacity (MW)'])))
    devperiod_renewal = dict(zip(hydro_renewal, list(hydro_new.iloc[:]['Development Time (years)'])))
    location_renewal = dict(zip(hydro_renewal, list(hydro_new.iloc[:]['Balancing Area'])))
    distance_renewal = dict(zip(hydro_renewal, list(hydro_new.iloc[:]['Distance to Grid (km)'])))
    type_renewal = dict(zip(hydro_renewal, list(hydro_new.iloc[:]['Generation Type - COPPER'])))
    fixed_o_m_renewal = dict(zip(hydro_renewal, list(hydro_new.iloc[:]['Fixed O&M ($/MW-year)'])))
    variable_o_m_renewal = dict(zip(hydro_renewal, list(hydro_new.iloc[:]['Variable O&M ($/MWh)'])))

    hr_ror = list()
    cost_ror_renewal = {}
    capacity_ror_renewal = {}
    hr_ror_location = {}

    hr_day = list()
    cost_day_renewal = {}
    capacity_day_renewal = {}
    hr_day_location = {}

    hr_mo = list()
    cost_month_renewal = {}
    capacity_month_renewal = {}
    hr_month_location = {}

    hr_pump = list()
    cost_pump_renewal = {}
    capacity_pump_renewal = {}
    hr_pump_location = {}
    for k in hydro_renewal:
        if foryear - 2020 >= devperiod_renewal[k]:
            if type_renewal[k] == 'hydro_run':
                hr_ror.append(k)
                cost_ror_renewal[k] = cost_renewal[k] * 1000000 + distance_renewal[k] * intra_ba_transcost * \
                                      capacity_renewal[k]
                capacity_ror_renewal[k] = capacity_renewal[k]
                hr_ror_location[k] = location_renewal[k]

            if type_renewal[k] == 'hydro_daily':
                hr_day.append(k)
                cost_day_renewal[k] = cost_renewal[k] * 1000000 + distance_renewal[k] * intra_ba_transcost * \
                                      capacity_renewal[k]
                capacity_day_renewal[k] = capacity_renewal[k]
                hr_day_location[k] = location_renewal[k]

            if type_renewal[k] == 'hydro_monthly':
                hr_mo.append(k)
                cost_month_renewal[k] = cost_renewal[k] * 1000000 + distance_renewal[k] * intra_ba_transcost * \
                                        capacity_renewal[k]
                capacity_month_renewal[k] = capacity_renewal[k]
                hr_month_location[k] = location_renewal[k]
            if type_renewal[k] == 'storage_PH':
                hr_pump.append(k)
                cost_pump_renewal[k] = cost_renewal[k] * 1000000 + distance_renewal[k] * intra_ba_transcost * \
                                       capacity_renewal[k]
                capacity_pump_renewal[k] = capacity_renewal[k]
                hr_pump_location[k] = location_renewal[k]

    ror_renewalout = {}
    for HR_ROR in hr_ror:
        for H in h:
            province_loc = hr_ror_location[HR_ROR].replace('.a', '')
            province_loc = province_loc.replace('.b', '')
            ror_renewalout[str(H) + '.' + HR_ROR] = capacity_ror_renewal[HR_ROR] * hydro_cf[province_loc + '.' + str(H)]

    day_renewal_historic = {}
    day_renewalout = {}
    for HR_DAY in hr_day:
        for D in d:
            day_renewal_historic[str(D) + '.' + HR_DAY] = 0
        for H in h:
            province_loc = hr_day_location[HR_DAY].replace('.a', '')
            province_loc = province_loc.replace('.b', '')
            day_renewalout[str(H) + '.' + HR_DAY] = capacity_day_renewal[HR_DAY] * hydro_cf[province_loc + '.' + str(H)]
            day_renewal_historic[str(map_hd[H]) + '.' + HR_DAY] = day_renewal_historic[str(map_hd[H]) + '.' + HR_DAY] + \
                                                                  day_renewalout[str(H) + '.' + HR_DAY]

    month_renewal_historic = {}
    month_renewalout = {}
    for HR_MO in hr_mo:
        for M in m:
            month_renewal_historic[str(M) + '.' + HR_MO] = 0
        for H in h:
            province_loc = hr_month_location[HR_MO].replace('.a', '')
            province_loc = province_loc.replace('.b', '')
            month_renewalout[str(H) + '.' + HR_MO] = capacity_month_renewal[HR_MO] * hydro_cf[
                province_loc + '.' + str(H)]
            month_renewal_historic[str(map_hm[H]) + '.' + HR_MO] = month_renewal_historic[
                                                                       str(map_hm[H]) + '.' + HR_MO] + month_renewalout[
                                                                       str(H) + '.' + HR_MO]

#####recontract input data ###########33
windonscost_recon = {}
windofscost_recon = {}
solarcost_recon = {}

for PD in pds:
    windonscost_recon[str(PD)] = config['Economics']['windcost_recon_ons'] * technology_evolution[f'wind_ons.{PD}']
    windofscost_recon[PD] = config['Economics']['windcost_recon_ofs'] * technology_evolution[f'wind_ofs.{PD}']
    solarcost_recon[PD] = config['Economics']['solarcost_recon'] * technology_evolution[f'solar.{PD}']

windsolarrecon = pd.read_csv(get_file('wind_solar_location_recon.csv'), header=0)

wind_ons_recon_capacity = {}
wind_ofs_recon_capacity = {}
solar_recon_capacity = {}
for PD in pds:
    for GL in gl:
        wind_ons_recon_capacity[PD + '.' + GL] = 0
        wind_ofs_recon_capacity[PD + '.' + GL] = 0
        solar_recon_capacity[PD + '.' + GL] = 0

        if str(GL) + '.' + 'wind_ons' in list(windsolarrecon[:]['location']):
            wind_ons_recon_capacity[PD + '.' + GL] = windsolarrecon[PD][
                list(windsolarrecon[:]['location']).index(str(GL) + '.' + 'wind_ons')]
        if str(GL) + '.' + 'wind_ofs' in list(windsolarrecon[:]['location']):
            wind_ofs_recon_capacity[PD + '.' + GL] = windsolarrecon[PD][
                list(windsolarrecon[:]['location']).index(str(GL) + '.' + 'wind_ofs')]
        if str(GL) + '.' + 'solar' in list(windsolarrecon[:]['location']):
            solar_recon_capacity[PD + '.' + GL] = windsolarrecon[PD][
                list(windsolarrecon[:]['location']).index(str(GL) + '.' + 'solar')]

cleared_data = gc.collect()
######### Duplicating some sets ###############
ttplants = tplants
ggl = gl
hh = h
app = ap
abba = aba

end = time.time()
print(f'\n==================================================\n\
Initializing input data time (Sec): {round((end - start) / 60)} Min and {round((end - start) % 60)} Sec \
\n==================================================')
start = time.time()

model = ConcreteModel()
########Creating the Optimization Model######################

# define index sets
model.pds = Set(initialize=pds)
model.gl = Set(initialize=gl)
model.h = Set(initialize=h)
model.aba = Set(initialize=aba)
model.abba = Set(initialize=abba)
model.tplants = Set(initialize=tplants)
model.st = Set(initialize=st)

#### Defining variables####
model.capacity_therm = Var(model.pds, model.aba, model.tplants, within=NonNegativeReals,
                           initialize=1)  # new thermal plant capacity in MW
model.retire_therm = Var(model.pds, model.aba, model.tplants, within=NonNegativeReals,
                         initialize=0)  # retire extant thermal capacity in MW
model.capacity_wind_ons = Var(model.pds, model.gl, within=NonNegativeReals,
                              initialize=0)  # onshore wind plant capacity in MW
model.capacity_wind_ofs = Var(model.pds, model.gl, within=NonNegativeReals,
                              initialize=0)  # offshore wind plant capacity in MW
model.capacity_solar = Var(model.pds, model.gl, within=NonNegativeReals, initialize=0)  # solar plant capacity in MW
model.fuel_cost = Var(model.pds, within=NonNegativeReals)

if storage_continous:
    model.capacity_storage = Var(model.pds, model.st, model.aba, within=NonNegativeReals,
                                 initialize=0)  # storage plant capacity in MW

model.supply = Var(model.pds, model.h, model.aba, model.tplants, within=NonNegativeReals,
                   initialize=0)  # fossil fuel supply in MW
model.windonsout = Var(model.pds, model.h, model.aba, within=NonNegativeReals,
                       initialize=0)  # onshore wind hourly power output
model.windofsout = Var(model.pds, model.h, model.aba, within=NonNegativeReals,
                       initialize=0)  # offshore wind hourly power output
model.solarout = Var(model.pds, model.h, model.aba, within=NonNegativeReals, initialize=0)  # solar hourly power output
model.load_shedding = Var(model.pds, model.h, model.aba, within=NonNegativeReals,
                          initialize=0)  # Hourly Load Shedding in MW
model.storageout = Var(model.pds, model.st, model.h, model.aba, within=NonNegativeReals,
                       initialize=0)  # pumped hydro hourly output
model.storagein = Var(model.pds, model.st, model.h, model.aba, within=NonNegativeReals,
                      initialize=0)  # pumped hydro hourly input
model.storageenergy = Var(model.pds, model.st, model.h, model.aba, within=NonNegativeReals,
                          initialize=0)  # total stored pump hydro energy in MWh
model.daystoragehydroout = Var(model.pds, model.h, model.aba, within=NonNegativeReals,
                               initialize=0)  # day storage hydro output in MW
model.monthstoragehydroout = Var(model.pds, model.h, model.aba, within=NonNegativeReals,
                                 initialize=0)  # month storage hydro output in MW
model.transmission = Var(model.pds, model.h, model.aba, model.abba, within=NonNegativeReals,
                         initialize=0)  # hourly transmission in MW from ap,ba to apa,abba
model.capacity_transmission = Var(model.pds, model.aba, model.abba, within=NonNegativeReals,
                                  initialize=0)  # transmission capacity in MW from ap,aba to app,abba
model.hydrorenewalccost = Var(model.pds, within=NonNegativeReals)
model.hydrorenewalomcost = Var(model.pds, within=NonNegativeReals)
model.newstorage_ccost = Var(model.pds, within=NonNegativeReals)
model.newstorage_omcost = Var(model.pds, within=NonNegativeReals)

if hydro_development:

    model.hr_ror = Set(initialize=hr_ror)
    model.hr_day = Set(initialize=hr_day)
    model.hr_mo = Set(initialize=hr_mo)
    model.hr_pump = Set(initialize=hr_pump)

    model.ror_renewal_binary = Var(model.pds, model.hr_ror, within=Binary, initialize=1)
    model.day_renewal_binary = Var(model.pds, model.hr_day, within=Binary, initialize=1)
    model.month_renewal_binary = Var(model.pds, model.hr_mo, within=Binary, initialize=1)
    model.dayrenewalout = Var(model.pds, model.h, model.hr_day, within=NonNegativeReals, initialize=0)
    model.monthrenewalout = Var(model.pds, model.h, model.hr_mo, within=NonNegativeReals, initialize=0)

    if not storage_continous:
        model.pumphydro = Var(model.pds, model.hr_pump, within=Binary, initialize=0)
model.capacity_wind_ons_recon = Var(model.pds, model.gl, within=NonNegativeReals,
                                    initialize=0)  # onshore wind recontract capacity in MW
model.capacity_wind_ofs_recon = Var(model.pds, model.gl, within=NonNegativeReals,
                                    initialize=0)  # offshore wind recontract capacity in MW
model.capacity_solar_recon = Var(model.pds, model.gl, within=NonNegativeReals,
                                initialize=0)  # solar recontract capacity in MW

## Intializing storages' energy
for ABA in aba:
    for PD in pds:
        for ST in st:
            model.storageenergy[PD, ST, h[0], ABA].fix(0)

m_counter = 2
for H in h:
    if map_hm[H] == m_counter:
        for ABA in aba:
            for PD in pds:
                for ST in st:
                    model.storageenergy[PD, ST, H, ABA].fix(0)
        m_counter += 1

model.capcost = Var(model.pds, within=NonNegativeReals, initialize=0)
model.fixOMcost = Var(model.pds, within=NonNegativeReals, initialize=0)
model.varOMcost = Var(model.pds, within=NonNegativeReals, initialize=0)


def cap_cost_constraint(model, PD):
    """
    Constrains model.capcost for a given period to be greater than or equal to the sum of 
    capacity[]*capitalcosts[] for all generation types + transmission_capacity*transcost*distance for interprovincial transmission
    if flag_itc variable in config.toml is set to true then capacity of ITCplants is multiplied by factor_itc in config.toml.

    Parameters:
        model (Object): The optimization model.
        PD (str): The specific period.

    Returns:
        Constraint (Object): pyomo.core.expr.relational_expr.InequalityExpression object.
    """

    ind = pds.index(PD)
    if flag_itc:
        summed_capacity = quicksum(
                                model.capacity_therm[PDD, ABA, ITC] * capitalcost[ABA + '.' + ITC] * factor_itc for PDD in pds[:ind + 1] for
                                ITC in ITCplants for ABA in aba if ITC in tplants and PDD in ['2025', '2030']) \
                        + quicksum(
                                model.capacity_therm[PDD, ABA, ITC] * capitalcost[ABA + '.' + ITC] for PDD in pds[:ind + 1] for ITC in
                                ITCplants for ABA in aba if ITC in tplants and PDD in ['2035', '2040', '2045', '2050']) \
                        + quicksum(
                                model.capacity_therm[PDD, ABA, TP] * capitalcost[ABA + '.' + TP] for PDD in pds[:ind + 1] for TP in tplants
                                for ABA in aba if TP not in ITCplants) \
                        + quicksum(model.capacity_wind_ons[PDD, GL] * factor_itc * windonscost[PDD + '.' + GL]
                                     + model.capacity_wind_ofs[PDD, GL] * factor_itc * windofscost[PDD + '.' + GL]
                                     + model.capacity_solar[PDD, GL] * factor_itc * solarcost[PDD + '.' + GL]
                                     + model.capacity_wind_ons_recon[PDD, GL] * factor_itc * windonscost_recon[PDD]
                                     + model.capacity_wind_ofs_recon[PDD, GL] * factor_itc * windofscost_recon[PDD]
                                     + model.capacity_solar_recon[PDD, GL] * factor_itc * solarcost_recon[PDD] for PDD
                                    in pds[:ind + 1] for GL in model.gl) \
                        + quicksum(
                                    model.capacity_transmission[PDD, ABA, ABBA] * factor_itc * transcost * distance[ABA + '.' + ABBA] 
                                    for PDD in pds[:ind + 1] for ABA in aba for ABBA in aba if ABA + '.' + ABBA in transmap)

        return model.capcost[PD] >= summed_capacity
    else:
        return model.capcost[PD] >= quicksum(
            model.capacity_therm[PDD, ABA, TP] * capitalcost[ABA + '.' + TP] for PDD in pds[:ind + 1] for TP in tplants
            for ABA in aba) \
            + quicksum(model.capacity_wind_ons[PDD, GL] * windonscost[PDD + '.' + GL]
                       + model.capacity_wind_ofs[PDD, GL] * windofscost[PDD + '.' + GL]
                       + model.capacity_solar[PDD, GL] * solarcost[PDD + '.' + GL]
                       + model.capacity_wind_ons_recon[PDD, GL] * windonscost_recon[PDD]
                       + model.capacity_wind_ofs_recon[PDD, GL] * windofscost_recon[PDD]
                       + model.capacity_solar_recon[PDD, GL] * solarcost_recon[PDD] for PDD in pds[:ind + 1] for GL in
                        model.gl) \
            + quicksum(model.capacity_transmission[PDD, ABA, ABBA] * transcost * distance[ABA + '.' + ABBA] for PDD in
                        pds[:ind + 1] for ABA in aba for ABBA in aba if ABA + '.' + ABBA in transmap)


model.capcost_constraint = Constraint(model.pds, rule=cap_cost_constraint)


def fom_cost_constraint(model, PD):
    """
    create constraints that ensure the total fixed operation and maintenance (FOM) costs across all energy generation and transmission
    types do not exceed the FOM cost limits set in the model for each period.

    Parameters:
        model (Object): The optimization model.
        PD: The specific period.

    Returns:
        object: pyomo.core.expr.relational_expr.InequalityExpression
    """
    #creates constraints for Fixed Operation and Maintenance costs for all plants existing and built by COPPER
    ind = pds.index(PD)
    wind_ons_key = 'wind_ons'
    wind_ofs_key = 'wind_ofs'
    solar_key = 'solar'

    thermal_sum = quicksum(
        (extant_thermal[pds[0] + '.' + ABA + '.' + TP] + quicksum(
            model.capacity_therm[PDD, ABA, TP] - model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind + 1])) * fixed_o_m[TP]
        for ABA in model.aba for TP in model.tplants)

    wind_solar_sum = quicksum(
        (model.capacity_wind_ons[PDD, GL] + model.capacity_wind_ons_recon[PDD, GL]) * fixed_o_m[wind_ons_key]
        + (model.capacity_wind_ofs[PDD, GL] + model.capacity_wind_ofs_recon[PDD, GL]) * fixed_o_m[wind_ofs_key]
        + (model.capacity_solar[PDD, GL] + model.capacity_solar_recon[PDD, GL]) * fixed_o_m[solar_key] for PDD in
        pds[:ind + 1] for GL in model.gl)

    extant_wind_solar_sum = quicksum(
        extant_wind_solar[ind][str(GL) + '.' + key] * fixed_o_m[key] for GL in model.gl for key in [wind_ons_key, wind_ofs_key, solar_key]
        if str(GL) + '.' + key in extant_wind_solar[0])

    transmission_sum = quicksum(
        model.capacity_transmission[PDD, ABA, ABBA] * trans_o_m for PDD in pds[:ind + 1] for ABA in model.aba for ABBA
        in model.aba if ABA + '.' + ABBA in transmap)

    extant_transmission_sum = quicksum(
        extant_transmission[ind][ABA + '.' + ABBA] * trans_o_m for ABA in model.aba for ABBA in model.aba if
        ABA + '.' + ABBA in extant_transmission[ind])

    hydro_sum = quicksum(ror_hydro_capacity[PD + '.' + ABA] * fixed_o_m['hydro_run']
                         + day_hydro_capacity[PD + '.' + ABA] * fixed_o_m['hydro_daily']
                         + month_hydro_capacity[PD + '.' + ABA] * fixed_o_m['hydro_monthly'] for ABA in model.aba)

    summed_fom = thermal_sum + wind_solar_sum + extant_wind_solar_sum + transmission_sum + extant_transmission_sum + hydro_sum

    return model.fixOMcost[PD] >= summed_fom



model.fom_cost_constraint = Constraint(model.pds, rule=fom_cost_constraint)

def vom_cost_constraint(model, PD):
    """
    Defines a constraint on the variable operation and maintenance (VOM) costs for various types of power generation 
    (thermal, wind, solar, and hydro) and load shedding during each economic dispatch period.

    This function calculates the total VOM costs for all power plant types across all hours and locations, scaled to an annual
    cost by multiplying by 365 and dividing by the number of days the model runs (`len(run_days)`). This is set in config.toml It 
    then ensures that this total VOM cost does not exceed the variable operation and maintenance budget specified in 
    the model for the given period (`PD`).

    Parameters:
        model (pyomo object): The Pyomo model instance containing all the decision variables, parameters, and sets used in the model.
        PD (string): The planning period as a string for which the constraint is being calculated eg. '2025'.

    Returns:
        Constraint expression (pyomo constraint): enforces the total annualized VOM costs is to be less 
        than or equal to the model's variable O&M cost allowance for the period `PD`.

    The function uses several model components:
    - `model.supply[PD, H, ABA, TP]`: Power supply from different plant types (TP) during each hour (H) and at each 
      location (ABA).
    - `model.windonsout`, `model.windofsout`, `model.solarout`: Power output from wind (onshore and offshore) and solar 
      units.
    - `ror_hydroout`, `model.daystoragehydroout`, `model.monthstoragehydroout`: Outputs from run-of-river, daily, and 
      monthly storage hydro units.
    - `model.load_shedding`: Power load shedding amount, indicating power that must be cut due to insufficient supply 
      or other issues.
    - `variable_o_m`: Dictionary of variable O&M cost coefficients for different types of energy sources.
    - `load_shedding_cost`: Cost associated with load shedding.

    Each term in the returned expression is multiplied by 365 and divided by the number of days in `run_days`, 
    representing an annualization of the calculated costs.
    """
    return model.varOMcost[PD] >= (
            quicksum(model.supply[PD, H, ABA, TP] * variable_o_m[TP] for H in h for ABA in aba for TP in
                     tplants) * 365 / len(run_days)
            + quicksum(model.windonsout[PD, H, ABA] * variable_o_m['wind_ons']
                       + model.windofsout[PD, H, ABA] * variable_o_m['wind_ofs']
                       + model.solarout[PD, H, ABA] * variable_o_m['solar']
                       + ror_hydroout[PD + '.' + str(H) + '.' + ABA] * variable_o_m['hydro_run']
                       + model.daystoragehydroout[PD, H, ABA] * variable_o_m['hydro_daily']
                       + model.monthstoragehydroout[PD, H, ABA] * variable_o_m['hydro_monthly'] for H in h for ABA in
                       aba) * 365 / len(run_days)
            + quicksum(model.load_shedding[PD, H, ABA] * load_shedding_cost for ABA in aba for H in h) * 365 / len(
        run_days)
    )


model.var_om_cost_constraint = Constraint(model.pds, rule=vom_cost_constraint)


def hydro_renewal_constraint(model, PD):
    """
    Defines a constraint on the capital costs associated with the renewal of hydro power generation capacities 
    within the model for a given period (PD). The constraint adjusts the cost calculations based on whether investment
    tax credits (flag_itc) are applicable, and differentiates between types of hydro technologies: run-of-river (HR_ROR),
    daily storage (HR_DAY), monthly storage (HR_MO), and pumped hydro (HR_PUMP), depending on their operation status
    and the year.

    Parameters:
        model (Pyomo Object): The Pyomo model instance that contains all the decision variables, parameters, and sets used in the model.
        PD (str): The planning period as a string for which the constraint is being calculated eg. '2025'.

    Returns:
        Constraint expression (pyomo constraint): sets the hydro renewal capital cost for the period PD.
        If hydro development is considered `hydro_development` is set to true in config.tom, the cost is calculated based on the renewal needs
        and the potential application of tax credits. If not, the cost is set to zero.

    Details:
        - The cost calculations consider if the plant operations are continuous (`storage_continous`) and apply
          different costs for different periods (2025, 2030 vs. 2035 to 2050).
        - The costs are calculated by summing up the product of the renewal binary decision variables for each plant type
          and their respective costs, optionally adjusted by a tax factor (`factor_itc`).
        - If `flag_itc` is true in config.toml, the costs for certain periods are adjusted by `factor_itc` also set in config.toml.
        - If `storage_continous` is false in config.toml, additional costs related to pumped hydro renewal are included.
        
        The constraint is modeled to ensure that `model.hydrorenewalccost[PD]` is at least the calculated sum of renewal
        costs, enforcing budget adherence for hydro renewals within the specified period.
    """
    if hydro_development:
        ind = pds.index(PD)
        if flag_itc:
            cost_sum = quicksum(
                cost_ror_renewal[HR_ROR] * factor_itc * model.ror_renewal_binary[PDD, HR_ROR] for PDD in pds[:ind + 1]
                for HR_ROR in hr_ror if PDD in ['2025', '2030']) \
                    + quicksum(
                cost_ror_renewal[HR_ROR] * model.ror_renewal_binary[PDD, HR_ROR] for PDD in pds[:ind + 1] for HR_ROR in
                hr_ror if PDD in ['2035', '2040', '2045', '2050']) \
                    + quicksum(
                cost_day_renewal[HR_DAY] * factor_itc * model.day_renewal_binary[PDD, HR_DAY] for PDD in pds[:ind + 1]
                for HR_DAY in hr_day if PDD in ['2025', '2030']) \
                    + quicksum(
                cost_day_renewal[HR_DAY] * model.day_renewal_binary[PDD, HR_DAY] for PDD in pds[:ind + 1] for HR_DAY in
                hr_day if PDD in ['2035', '2040', '2045', '2050']) \
                    + quicksum(
                cost_month_renewal[HR_MO] * factor_itc * model.month_renewal_binary[PDD, HR_MO] for PDD in pds[:ind + 1]
                for HR_MO in hr_mo if PDD in ['2025', '2030']) \
                    + quicksum(
                cost_month_renewal[HR_MO] * model.month_renewal_binary[PDD, HR_MO] for PDD in pds[:ind + 1] for HR_MO in
                hr_mo if PDD in ['2035', '2040', '2045', '2050'])
            if not storage_continous:
                cost_sum += quicksum(
                    cost_pump_renewal[HR_PUMP] * factor_itc * model.pumphydro[PDD, HR_PUMP] for PDD in pds[:ind + 1] for
                    HR_PUMP in hr_pump if PDD in ['2025', '2030']) \
                            + quicksum(
                    cost_pump_renewal[HR_PUMP] * model.pumphydro[PDD, HR_PUMP] for PDD in pds[:ind + 1] for HR_PUMP in
                    hr_pump if PDD in ['2035', '2040', '2045', '2050'])
        else:
            cost_sum = quicksum(
                cost_ror_renewal[HR_ROR] * model.ror_renewal_binary[PDD, HR_ROR] for PDD in pds[:ind + 1] for HR_ROR in
                hr_ror) \
                       + quicksum(
                cost_day_renewal[HR_DAY] * model.day_renewal_binary[PDD, HR_DAY] for PDD in pds[:ind + 1] for HR_DAY in
                hr_day) \
                       + quicksum(
                cost_month_renewal[HR_MO] * model.month_renewal_binary[PDD, HR_MO] for PDD in pds[:ind + 1] for HR_MO in
                hr_mo)
            if not storage_continous:
                cost_sum += quicksum(
                    cost_pump_renewal[HR_PUMP] * model.pumphydro[PDD, HR_PUMP] for PDD in pds[:ind + 1] for HR_PUMP in
                    hr_pump)

        return model.hydrorenewalccost[PD] >= cost_sum
    else:
        return model.hydrorenewalccost[PD] == 0


model.hydro_renewal_constraint = Constraint(model.pds, rule=hydro_renewal_constraint)


def hydro_renewal_om_constraint(model, PD):
    """
    Defines a constraint for the operational and maintenance (O&M) costs associated with hydro power renewal 
    for the specified planning period (PD) within the Pyomo model. The function calculates the O&M costs based 
    on fixed and variable cost components for different types of hydro technologies (run-of-river, daily storage,
    monthly storage, and potentially pumped storage if storage_continuous is set to true in config.toml). This cost 
    is annualized by scaling with the number of run days in a year.

    Parameters:
        model (object): A Pyomo model instance that includes all the decision variables, parameters, and sets used in the model.
        PD (str): The planning period as a string for which the constraint is being calculated (e.g., '2025').

    Returns:
        return model.hydrorenewalomcost[PD] >= om_cost_sum (constraint): A Pyomo Constraint expression that ensures the O&M costs for hydro renewal during the planning period PD
        are covered. If hydro development is not considered (hydro_development is False), the constraint sets the
        O&M costs to zero.

    Details:
        - The constraint is calculated only if hydro development is considered (`hydro_development` is set to True in config.toml).
        - It aggregates the costs from various sources:
          1. Fixed O&M costs for run-of-river, daily, and monthly hydro facilities, applied to their respective capacities.
          2. Variable O&M costs for these facilities, scaled by the output and adjusted for the number of days in the period.
        - Costs for each type of facility are summed using quicksum, considering each period up to the current one.
        - If storage is not continuous (`storage_continous` is False), additional costs for pumped hydro facilities are included.
        - Annualization is achieved by multiplying variable costs by 365 and dividing by the length of `run_days`.

        The function assumes that the O&M cost for the second economic dispatch should be equal to the first economic dispatch,
        as noted by JGM.
    """
    '''
    if hydro_development:
        ind = pds.index(PD)
        om_cost_sum = quicksum(
            capacity_ror_renewal[HR_ROR] * model.ror_renewal_binary[PDD, HR_ROR] * fixed_o_m_renewal[HR_ROR] for PDD in
            pds[:ind + 1] for HR_ROR in hr_ror) \
                      + quicksum(
            capacity_day_renewal[HR_DAY] * model.day_renewal_binary[PDD, HR_DAY] * fixed_o_m_renewal[HR_DAY] for PDD in
            pds[:ind + 1] for HR_DAY in hr_day) \
                      + quicksum(
            capacity_month_renewal[HR_MO] * model.month_renewal_binary[PDD, HR_MO] * fixed_o_m_renewal[HR_MO] for PDD in
            pds[:ind + 1] for HR_MO in hr_mo) \
                      + quicksum(
            ror_renewalout[str(H) + '.' + HR_ROR] * model.ror_renewal_binary[PDD, HR_ROR] * variable_o_m_renewal[HR_ROR]
            for PDD in pds[:ind + 1] for H in h for HR_ROR in hr_ror) * 365 / len(run_days) \
                      + quicksum(
            model.dayrenewalout[PD, H, HR_DAY] * variable_o_m_renewal[HR_DAY] for H in h for HR_DAY in
            hr_day) * 365 / len(run_days) \
                      + quicksum(
            model.monthrenewalout[PD, H, HR_MO] * variable_o_m_renewal[HR_MO] for H in h for HR_MO in
            hr_mo) * 365 / len(run_days)
        if not storage_continous:
            om_cost_sum += quicksum(
                capacity_pump_renewal[HR_PUMP] * model.pumphydro[PDD, HR_PUMP] * fixed_o_m_renewal[HR_PUMP] for PDD in
                pds[:ind + 1] for HR_PUMP in hr_pump)
            ##O&M cost for hydro renewal for second economic dispatch, should be equal to first economic dispatch -JGM
        return model.hydrorenewalomcost[PD] >= om_cost_sum
    else:
        return model.hydrorenewalomcost[PD] == 0
    '''
    if hydro_development:
        ind = pds.index(PD)
        om_cost_sum = quicksum(
            capacity_ror_renewal[HR_ROR] * model.ror_renewal_binary[PDD, HR_ROR] for PDD in
            pds[:ind + 1]) * quicksum(fixed_o_m_renewal[HR_ROR] for HR_ROR in hr_ror) \
                    + quicksum(
            capacity_day_renewal[HR_DAY] * model.day_renewal_binary[PDD, HR_DAY] for PDD in
            pds[:ind + 1]) * quicksum(fixed_o_m_renewal[HR_DAY] for HR_DAY in hr_day) \
                      + quicksum(
            capacity_month_renewal[HR_MO] * model.month_renewal_binary[PDD, HR_MO] for PDD in
            pds[:ind + 1]) * quicksum(fixed_o_m_renewal[HR_MO] for HR_MO in hr_mo) \
                      + quicksum(
            ror_renewalout[str(H) + '.' + HR_ROR] * model.ror_renewal_binary[PDD, HR_ROR] * variable_o_m_renewal[HR_ROR]
            for PDD in pds[:ind + 1] for H in h for HR_ROR in hr_ror) * 365 / len(run_days) \
                      + quicksum(
            model.dayrenewalout[PD, H, HR_DAY] * variable_o_m_renewal[HR_DAY] for H in h for HR_DAY in
            hr_day) * 365 / len(run_days) \
                      + quicksum(
            model.monthrenewalout[PD, H, HR_MO] * variable_o_m_renewal[HR_MO] for H in h for HR_MO in
            hr_mo) * 365 / len(run_days)
        if not storage_continous:
            om_cost_sum += quicksum(
                capacity_pump_renewal[HR_PUMP] * model.pumphydro[PDD, HR_PUMP] for PDD in
                pds[:ind + 1] for HR_PUMP in hr_pump) * quicksum(fixed_o_m_renewal[HR_PUMP] for HR_PUMP in hr_pump)
            ##O&M cost for hydro renewal for second economic dispatch, should be equal to first economic dispatch -JGM
        return model.hydrorenewalomcost[PD] >= om_cost_sum
    else:
        return model.hydrorenewalomcost[PD] == 0
    


model.hydro_renewal_om_constraint = Constraint(model.pds, rule=hydro_renewal_om_constraint)


def new_storage_ccost_constraint(model, PD):
    """
    Defines a constraint for the capital costs associated with new storage facilities within a given planning period (PD).
    The function computes these costs based on the model's storage capacity decision variables, adjusted for tax credits
    if applicable. The constraint ensures that the allocated capital cost for new storage does not exceed the calculated
    cost sum for the period.

    Parameters:
        model (Pyomo Object): A Pyomo model instance, containing all decision variables, parameters, and sets relevant to the model.
        PD (str): The planning period as a string for which the constraint is being calculated (e.g., '2025').

    Returns:
        model.newstorage_ccost[PD] >= storage_cost_sum (Pyomo Constraint): expression that conditions the new storage capital cost for the period PD based on whether
        storage is continuously operational and whether investment tax credits (flag_itc) apply.

    Details:
        - The constraint calculates the total capital costs by summing the product of storage capacity and per-unit cost,
          factoring in tax credits if `flag_itc` is True for earlier periods ('2025', '2030') and full costs for later
          periods ('2035', '2040', '2045', '2050').
        - If `storage_continous` is True in config.toml, the function calculates and sums the costs for all storage technologies (ST)
          in all balancing areas (ABA) up to the current period index found in `pds`.
        - If `storage_continous` is False, the function sets the new storage capital cost constraint to zero, indicating
          no storage capacity additions are considered.
        - `storage_cost[ST + '.' + PDD]` refers to the cost associated with each storage technology and period, and
          `model.capacity_storage[PDD, ST, ABA]` refers to the decision variable for new storage capacity.

    """
    if storage_continous:
        ind = pds.index(PD)
        if flag_itc:
            storage_cost_sum = quicksum(
                model.capacity_storage[PDD, ST, ABA] * factor_itc * storage_cost[ST + '.' + PDD] for PDD in
                pds[:ind + 1] for ST in st for ABA in aba if PDD in ['2025', '2030'])
            storage_cost_sum += quicksum(
                model.capacity_storage[PDD, ST, ABA] * storage_cost[ST + '.' + PDD] for PDD in pds[:ind + 1] for ST in
                st for ABA in aba if PDD in ['2035', '2040', '2045', '2050'])
        else:
            storage_cost_sum = quicksum(
                model.capacity_storage[PDD, ST, ABA] * storage_cost[ST + '.' + PDD] for PDD in pds[:ind + 1] for ST in
                st for ABA in aba)

        return model.newstorage_ccost[PD] >= storage_cost_sum
    else:
        return model.newstorage_ccost[PD] == 0


model.new_storage_ccost_constraint = Constraint(model.pds, rule=new_storage_ccost_constraint)


def new_storage_omcost_constraint(model, PD):
    """
    Establishes a constraint on the operational and maintenance (O&M) costs for new storage facilities within the model 
    for a specified planning period (PD). This constraint ensures that the O&M costs do not exceed the sum calculated 
    based on the storage capacities across different technologies and balancing areas, if storage is continuously 
    operational. If storage is not continuous, the O&M costs are set to zero for that period.

    Parameters:
        model: A Pyomo model instance, which includes all decision variables, parameters, and sets related to the model.
        PD: The planning period as a string for which the O&M cost constraint is being calculated (e.g., '2025').

    Returns:
        model.newstorage_omcost[PD] >= X (Pyomo Constraint): expression that sets the O&M costs for new storage facilities during the period PD. The costs
        are computed as the sum of products of fixed O&M costs per unit for each storage technology and their respective 
        capacities if storage operation is continuous. If not, the O&M costs are constrained to zero.

    Details:
        - The function checks if `storage_continous` is True as specified in config.toml. If so, it sums up the fixed O&M costs for all storage
          technologies (ST) and balancing areas (ABA) from the start of the planning series up to and including PD.
        - Fixed O&M costs for each storage type and period combination (ST + '.' + PDD) are multiplied by the capacity
          decisions (`model.capacity_storage[PDD, ST, ABA]`) to get the total O&M cost.
        - If `storage_continous` is False, it implies that no new storage facilities are considered operational, and thus
          the O&M costs are set to zero.
    """

    if storage_continous:
        ind = pds.index(PD)
        return model.newstorage_omcost[PD] >= quicksum(
            store_fix_o_m[ST + '.' + PDD] * model.capacity_storage[PDD, ST, ABA] for PDD in pds[:ind + 1] for ST in st
            for ABA in aba)
    else:
        return model.newstorage_omcost[PD] == 0


model.new_storage_omcost_constraint = Constraint(model.pds, rule=new_storage_omcost_constraint)


def fuel_cost_constraint(model, PD):
    """
    Defines a constraint for the annualized fuel cost of thermal generators during the economic dispatch for a 
    specified planning period (PD). This constraint ensures that the model's fuel cost for the period does not fall 
    below the computed cost based on the thermal plant supplies.

    The function multiplies the supply of each thermal plant by its fuel cost, aggregates these products over all 
    relevant periods, hours, and balancing areas, and scales the total to an annual basis by the number of running days.

    Parameters:
        model: Pyomo model instance containing all necessary decision variables, parameters, and sets.
        PD: Planning period as a string (e.g., '2025') for which the constraint is applied.

    Returns:
        Pyomo Constraint: expression that bounds the fuel cost for the period PD from below by the total 
        annualized fuel cost calculated from the thermal supply multiplied by their respective fuel costs and normalized 
        over the number of run days.
    """
    return model.fuel_cost[PD] >= quicksum(
        model.supply[PD, H, ABA, TP] * fuelcost[PD + '.' + TP + '.' + ABA] for H in h for ABA in aba for TP in
        tplants) * 365 / len(run_days)


model.fuel_cost_constraint = Constraint(model.pds, rule=fuel_cost_constraint)


###Objective function total cost minimization###
def obj_rule(model):
    """
    Objective function to minimize the total discounted and inflation-adjusted cost of an energy system over a set of 
    planning periods. This function aggregates the costs associated with capital, fuel, fixed and variable operations 
    and maintenance (O&M), hydro renewal, and new storage, both in terms of capital and O&M costs.

    Parameters:
        model: A Pyomo model instance, which contains all decision variables, parameters, and sets relevant to the energy 
               system being optimized.

    Returns:
        A single float value representing the total cost, which is the sum of discounted and inflation-adjusted costs 
        for capital expenditures, fuel, O&M, and hydro and storage related expenses across all planning periods.

    Details:
        - Each cost component (capcost, fuel_cost, varOMcost, fixOMcost, hydrorenewalccost, hydrorenewalomcost, 
          newstorage_ccost, newstorage_omcost) is multiplied by a discount coefficient (`disc_coef`) and an inflation 
          coefficient (`inf_coef`) to convert future costs to present value terms for comparison and aggregation.
        - The discount coefficient adjusts for the time value of money, decreasing the value of future expenditures. 
          This coefficient changes based on whether the current period is the last in the series or not, with special 
          treatment for the final period using a different discount rate (`discount_2050`). The final period is representative
		  of a single year, whereas the periods preceding are representative of 5-year periods. Periods that represent 5-year
		  periods are discounted back as a 5-year annuity series. The final period is discounted back to the present 
		  as a single future value.
        - The inflation coefficient scales costs based on expected inflation, adjusting the nominal cost to real terms 
          as compared to a base year (`refyear`).
        - The function iterates through all planning periods, accumulating the adjusted costs for each category, 
          and returns the total as the objective value to minimize.

    Usage:
        This function should be set as the objective in a Pyomo model setup to optimize the economic performance of an 
        energy system:
            model.obj = Objective(rule=obj_rule)

    Note:
        `discount`, `discount_2050`, `inflation`, and `refyear` are predefined in config.toml.
    """
    tcapcost = 0
    tfcost = 0
    tfixedOM = 0
    tvariableOM = 0
    thydrorenewalccost = 0
    thydrorenewalomcost = 0
    tnewstorage_ccost = 0
    tnewstorage_omcost = 0
    for PD in pds:
        ind = pds.index(PD)

        if ind != (len(pds) - 1):
            # We use disc_ann_coef as the coeffecient to bring an n-year series Annuity to a net present value
            first_term = (1 / (1 + discount) ** (int(pds[ind + 1]) - int(PD)))
            second_term = 1
            third_term = discount
            disc_ann_coef = ((second_term - first_term) / third_term)
            if (disc_ann_coef < 0):
                print(disc_ann_coef)
                sys.exit()
            disc_coef = disc_ann_coef * (1 / (1 + discount) ** ((
                                                                            int(PD) - 1) - refyear))  # New, we are using annuity series combined with future value to present value formula
        else:
            disc_coef = (1 / (1 + discount_2050) ** (
                        int(PD) - refyear))  ## Use this for the final subperiod since we are not annualizing it -- treat it as a future to present value
        inf_coef = ((1 + inflation) ** (int(PD) - refyear))

        tcapcost += model.capcost[PD] * disc_coef * inf_coef
        tfcost += model.fuel_cost[PD] * disc_coef * inf_coef  ##This is treating fuel cost like a single future value
        tvariableOM += model.varOMcost[PD] * disc_coef * inf_coef
        tfixedOM += model.fixOMcost[PD] * disc_coef * inf_coef
        thydrorenewalccost += model.hydrorenewalccost[PD] * disc_coef * inf_coef
        thydrorenewalomcost += model.hydrorenewalomcost[PD] * disc_coef * inf_coef
        tnewstorage_ccost += model.newstorage_ccost[PD] * disc_coef * inf_coef
        tnewstorage_omcost += model.newstorage_omcost[PD] * disc_coef * inf_coef
    return (
                tcapcost + tfcost + tvariableOM + tfixedOM + thydrorenewalccost + thydrorenewalomcost + tnewstorage_ccost + tnewstorage_omcost)


model.obj = Objective(rule=obj_rule, sense=minimize)

if flag_reserve_aggregated:
    ######Aggregated planning reserve requirement#####
    def planning_reserve_aggregated(model, PD, SEAS):
        """
        Calculates and ensures that the aggregated capacity across all power generation types in all applicable regions
        meets or exceeds the required planning reserve margin for a specified planning period and season. This function
        aggregates generation capacity from thermal, wind, solar, hydro, and storage sources, considering both existing and
        newly developed capacities.

        Parameters:
            model (Pyomo Object): Pyomo model instance containing decision variables, parameters, and sets for the energy model.
            PD (str): String representing the planning period for which the reserve requirement is calculated.
            SEAS (str): indicating the season (e.g., 'winter', 'summer') for which the calculation is relevant.

        Returns:
            Pyomo Constraint: checks if the total available generation capacity (cap_val) 
            is at least the product of peak demand (dem_val) for the period and season, and the required reserve margin.

        Details:
            - The function iterates over all areas (AP) and aggregates capacity values for all types of generation:
            thermal, wind (onshore and offshore, including reconstructed capacities), solar (including reconstructed),
            run-of-river, daily, and monthly hydro, and storage if continuous operation is assumed.
            - For each area, the capacity is adjusted for the specific capacity value of the season for wind and solar.
            - Hydro development and continuous storage operation are conditionally included in the capacity summation.
            - The aggregated capacity for each area is compared against the adjusted demand value, which is the peak demand 
            multiplied by the sum of 1 and the aggregate reserve margin (e.g., 1 + 15% reserve margin).
		Warning: Only use this constraint if you are simulating regions and would like to analyzed a shared planning reserve.
        """
        ind = pds.index(PD)
        cap_val = 0
        dem_val = peak_demand_aggregate[PD + '.' + SEAS]
        for AP in ap:
            cap_val += sum((extant_thermal[pds[0] + '.' + ABA + '.' + TP] + sum(
                model.capacity_therm[PDD, ABA, TP] - model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind + 1])) for ABA
                           in regions_per_province[AP] for TP in tplants) \
                       + sum((model.capacity_wind_ons[PDD, GL] + model.capacity_wind_ons_recon[PDD, GL]) * float(
                capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind + 1] for GL in gl if
                             AP == map_gl_to_pr[int(GL)]) + sum(
                extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'wind'] * float(
                    capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]]) for GL in gl if
                str(GL) + '.' + 'wind' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]) \
                       + sum((model.capacity_wind_ofs[PDD, GL] + model.capacity_wind_ofs_recon[PDD, GL]) * float(
                capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind + 1] for GL in gl if
                             AP == map_gl_to_pr[int(GL)]) \
                       + sum((model.capacity_solar[PDD, GL] + model.capacity_solar_recon[PDD, GL]) * float(
                capacity_value[SEAS]['solar'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind + 1] for GL in gl if
                             AP == map_gl_to_pr[int(GL)]) + sum(
                extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'solar'] * float(
                    capacity_value[SEAS]['solar'][map_gl_to_pr[int(GL)]]) for GL in gl if
                str(GL) + '.' + 'solar' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]) \
                       + sum(ror_hydro_capacity[PD + '.' + ABA] for ABA in regions_per_province[AP]) \
                       + sum(day_hydro_capacity[PD + '.' + ABA] for ABA in regions_per_province[AP]) \
                       + sum(month_hydro_capacity[PD + '.' + ABA] for ABA in regions_per_province[AP])
            if hydro_development:
                cap_val += sum(
                    capacity_ror_renewal[HR_ROR] * model.ror_renewal_binary[PDD, HR_ROR] for PDD in pds[:ind + 1] for
                    HR_ROR in hr_ror) \
                           + sum(
                    capacity_day_renewal[HR_DAY] * model.day_renewal_binary[PDD, HR_DAY] for PDD in pds[:ind + 1] for
                    HR_DAY in hr_day) \
                           + sum(
                    capacity_month_renewal[HR_MO] * model.month_renewal_binary[PDD, HR_MO] for PDD in pds[:ind + 1] for
                    HR_MO in hr_mo)
            if storage_continous:
                cap_val += sum(model.capacity_storage[PDD, ST, ABA] for PDD in pds[:ind + 1] for ST in st for ABA in
                               regions_per_province[AP])

        return cap_val >= dem_val * (float(reserve_margin_aggregate) + 1)


    model.planning_reserve_aggregated = Constraint(pds, season, rule=planning_reserve_aggregated)
else:
    cap_val_dict = capacity_value.astype(float).to_dict()


    def planning_reserve(model, PD, SEAS, AP):
        """
        Calculates and ensures that the available generation capacity within a province (AP is a list of all provinces defined in config.toml) and 
        season (SEAS) meets or exceeds the required planning reserve margin for the specified planning period (PD). This 
        function considers various sources of power generation including thermal, wind, solar, hydro, and storage, both 
        existing and potentially newly developed, to satisfy the demand plus a specified reserve margin.

        Parameters:
            model (Pyomo Object): A Pyomo model instance containing decision variables, parameters, and sets related to the energy model.
            PD (str): The planning period, represented as a string, for which the reserve capacity is being calculated.
            SEAS (str): The season (e.g., 'summer', 'winter') for which the capacity reserve requirement is assessed.
            AP (str): province, represented as a string, for which the calculation is performed.

        Returns:
            cap_val >= peak_demand[PD + '.' + SEAS + '.' + AP] * (float(reserve_margin_dict[AP]) + 1) (Pyomo Constraint): Evaluates to True if the total generation capacity within the province AP
            during the planning period PD and season SEAS meets or exceeds the peak demand multiplied by the (specified reserve margin + 1).

        Details:
            - This function aggregates the capacity from various sources including:
            * Existing and new thermal capacities, adjusted for retirements.
            * Wind and solar capacities, both existing and new, weighted by seasonal capacity factors.
            * Run-of-river, daily, and monthly hydro capacities.
            * Additional capacities from storage if storage is continuously operational.
            * Renewable capacities (if hydro development is assumed) like run-of-river, daily, and monthly renewals.
            - The capacities are adjusted for each generation type's relevance to the season and province.
            - The required reserve margin for AP is applied to the peak demand to determine the necessary total capacity.
            - Each generation source contributes to the total capacity, which must at least match the adjusted demand value
            considering the reserve margin.
        """
        ind = pds.index(PD)
        cap_val = (
            quicksum(
                (extant_thermal[pds[0] + '.' + ABA + '.' + TP] + quicksum(
                    model.capacity_therm[PDD, ABA, TP] - model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind + 1]))
                for ABA in regions_per_province[AP] for TP in tplants
            )
            + quicksum(
                extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'wind'] * cap_val_dict[(SEAS, "wind")][map_gl_to_pr[int(GL)]]
                for GL in gl if str(GL) + '.' + 'wind' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]
            )
            + quicksum(
                extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'solar'] * cap_val_dict[(SEAS, "solar")][map_gl_to_pr[int(GL)]]
                for GL in gl if str(GL) + '.' + 'solar' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]
            )
            + quicksum(
                (model.capacity_wind_ons[PDD, GL] + model.capacity_wind_ons_recon[PDD, GL]) *
                cap_val_dict[(SEAS, "wind")][map_gl_to_pr[int(GL)]]
                + (model.capacity_wind_ofs[PDD, GL] + model.capacity_wind_ofs_recon[PDD, GL]) *
                cap_val_dict[(SEAS, "wind")][map_gl_to_pr[int(GL)]]
                + (model.capacity_solar[PDD, GL] + model.capacity_solar_recon[PDD, GL]) *
                cap_val_dict[(SEAS, "solar")][map_gl_to_pr[int(GL)]]
                for PDD in pds[:ind + 1] for GL in gl if AP == map_gl_to_pr[int(GL)]
            )
            + quicksum(ror_hydro_capacity[PD + '.' + ABA] for ABA in regions_per_province[AP])
            + quicksum(day_hydro_capacity[PD + '.' + ABA] for ABA in regions_per_province[AP])
            + quicksum(month_hydro_capacity[PD + '.' + ABA] for ABA in regions_per_province[AP])
        )

        if hydro_development:
            cap_val += (
                quicksum(
                    capacity_ror_renewal[HR_ROR] * model.ror_renewal_binary[PDD, HR_ROR]
                    for PDD in pds[:ind + 1] for HR_ROR in hr_ror
                )
                + quicksum(
                    capacity_day_renewal[HR_DAY] * model.day_renewal_binary[PDD, HR_DAY]
                    for PDD in pds[:ind + 1] for HR_DAY in hr_day
                )
                + quicksum(
                    capacity_month_renewal[HR_MO] * model.month_renewal_binary[PDD, HR_MO]
                    for PDD in pds[:ind + 1] for HR_MO in hr_mo
                )
            )

        if storage_continous:
            cap_val += quicksum(
                model.capacity_storage[PDD, ST, ABA]
                for PDD in pds[:ind + 1] for ST in st for ABA in regions_per_province[AP]
            )

        return cap_val >= peak_demand[PD + '.' + SEAS + '.' + AP] * (float(reserve_margin_dict[AP]) + 1)


    model.planning_reserve = Constraint(pds, season, ap, rule=planning_reserve)


###constrain retirements to extant (or existing) plants
def retire(model, PD, ABA, TP):
    """
    Constrains the retirement of thermal plants within a specific balancing area and type to not exceed the 
    sum of extant capacity and any capacity added minus retirements in prior periods. This ensures that 
    retirements are limited to actual existing capacities.

    Parameters:
        model (pyomo model): A Pyomo model instance that contains decision variables, parameters, and sets relevant to the energy model.
        PD (string): The planning period as a string (e.g., '2025') during which the retirement constraint is applied.
        ABA (string): The balancing area as a string where the thermal plant is located.
        TP (string): The type of the thermal plant as a string, the list of possible strings comes from tplants which comes from thermal plants in generation_type_data.csv input sheet.

    Returns:
        Pyomo Constraint expression: Ensures that the thermal plant retirements in the planning period PD,
        within the balancing area ABA and of type TP, do not exceed the initial extant capacity plus any net
        increase in capacity from new construction minus prior retirements up to the current planning period.

    Details:
        - The function calculates the permissible retirement capacity by adding the initial extant capacity of the plant
          (at the beginning of the series) to the net capacity changes (new capacity minus retirements) up to the 
          period just before PD.
        - This constraint helps in accurate modeling of power system dynamics, ensuring retirements are within 
          realistic limits based on actual plant capacities.
    """
    ind = pds.index(PD)
    return model.retire_therm[PD, ABA, TP] <= extant_thermal[pds[0] + '.' + ABA + '.' + TP] + quicksum(
        model.capacity_therm[PDD, ABA, TP] - model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind])


model.retire = Constraint(pds, aba, tplants, rule=retire)


#### forces the model to retire the plants that are at end of life
def lifetime(model, PD, ABA, TP):
    """
    Enforces retirement of thermal plants in the model when they reach the end of their operational lifetime. 
    This function ensures that the total retired capacity up to and including a given planning period (PD) does not 
    exceed the initial extant capacity unless adjustments are made for units under construction, which might otherwise 
    cause infeasibility in the model due to premature retirements.

    Parameters:
        model (Pyomo model): instance containing all relevant decision variables, parameters, and sets.
        PD (str): The planning period as a string (e.g., '2025') during which the constraint is evaluated.
        ABA (str): The balancing area as a string, indicating the location of the thermal plant.
        TP (str): The type of the thermal plant as a string.

    Returns:
        A Pyomo Constraint expression: This constraint ensures that the cumulative retirement for the thermal 
        plant type TP in the balancing area ABA does not exceed its extant capacity from the start of the model, 
        adjusted for any significant changes due to new units becoming operational.

    Details:
        - The function first checks if the plant type in question has increased in capacity from one period to the 
          next, indicative of new units coming online. If there's an increase, a high threshold (100,000) is set 
          to prevent the constraint from forcing retirements that would render the model infeasible.
        - The function subtracts the cumulative retirements from the initial capacity (at the start of the model)
          and ensures this does not exceed the adjusted extant capacity (ex_thermal), which accounts for new units.

    Note:
        It is critical to correctly set up the initial capacities in the extant_capacity.csv input sheet to ensure 
        accurate implementation of this function.
    """
    ind = pds.index(PD)
    ex_thermal = extant_thermal[PD + '.' + ABA + '.' + TP]

    #######for under construction units, this will prevent infeasibility
    if ind >= 1 and extant_thermal[PD + '.' + ABA + '.' + TP] - extant_thermal[pds[ind - 1] + '.' + ABA + '.' + TP] > 0:
        ex_thermal = 100000
    return extant_thermal[pds[0] + '.' + ABA + '.' + TP] - quicksum(
        model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind + 1]) <= ex_thermal


model.lifetime = Constraint(pds, aba, tplants, rule=lifetime)


###wind generation limit
def windonsg(model, PD, H, ABA):
    """
    Constrains the onshore wind power output in a specific balancing area for each hour of simulation and planning period. 
    This function limits the power output to the sum of the potential generation from all relevant wind facilities within 
    the area, adjusted for capacity factor, plus any existing generation capacity.

    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets for the energy model.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The specific hour within the simulation and planning period.
        ABA: The balancing area as a string, indicating where the wind power generation is being considered.

    Returns:
        A Pyomo Constraint expression: This ensures that the wind power output (`model.windonsout`) for the given 
        hour H, planning period PD, and balancing area ABA does not exceed the theoretical maximum based on installed 
        capacity (both existing and newly constructed) and the capacity factors for that hour as stored in extant_wind_solar.csv input sheet.

    Details:
        - The function calculates the maximum output by aggregating the potential generation from all wind turbines
          (considering both existing and newly constructed capacities) within the specified balancing area. 
          The aggregation takes into account the capacity factors (`windcf_df`) which vary by hour and location.
        - The function also includes extant wind generation capacities (`extant_wind_ons_gen`) that are predefined 
          for the specific hour and balancing area.
        - The capacity factor for each generator is accessed from a dataframe (`windcf_df`) using the hour and generator
          location identifier, which adjusts the maximum generation based on typical resource availability during that hour.
    """

    ind = pds.index(PD)
    return model.windonsout[PD, H, ABA] <= quicksum(
        (model.capacity_wind_ons[PDD, GL] + model.capacity_wind_ons_recon[PDD, GL]) * windcf_df.loc[H, int(GL)] for PDD
        in pds[:ind + 1] for GL in gl if ABA == map_gl_to_ba[int(GL)]) + extant_wind_ons_gen.loc[(PD, H), ABA]

###Offshore wind generation limit
def windofsg(model, PD, H, ABA):
    """see windonsg() same as above but for offshore wind generation"""
    ind = pds.index(PD)
    return model.windofsout[PD, H, ABA] <= quicksum(
        (model.capacity_wind_ofs[PDD, GL] + model.capacity_wind_ofs_recon[PDD, GL]) * windcf_df.loc[H, int(GL)] for PDD
        in pds[:ind + 1] for GL in gl if ABA == map_gl_to_ba[int(GL)]) + extant_wind_ofs_gen.loc[(PD, H), ABA]


model.windonsg = Constraint(pds, h, aba, rule=windonsg)
model.windofsg = Constraint(pds, h, aba, rule=windofsg)


####solar generation limit
#TODO these sections solar and wind g are very slow
def solarg(model, PD, H, ABA):
    """
    Constrains the solar power output in a specific balancing area during a designated hour and planning period and simulated hours. 
    This function limits the solar power output to the sum of the calculated potential generation from all relevant 
    solar installations within the area, considering both existing and reconstructed capacity adjusted for the 
    capacity factor, plus any existing generation capacity.

    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets for the energy model.
        PD: The planning period, represented as a string (e.g., '2025'), during which the constraint is applied.
        H: The specific hour within the planning period for which the output constraint is relevant.
        ABA: The balancing area as a string, indicating where the solar power generation is being considered.

    Returns:
        A Pyomo Constraint expression: Ensures that the solar power output (`model.solarout`) for the specified 
        hour H, planning period PD, and balancing area ABA does not exceed the theoretical maximum based on installed 
        capacity (both existing and newly constructed) and the capacity factors for that hour.

    Details:
        - The maximum output is calculated by aggregating the potential generation from all solar panels (accounting 
          for both existing and newly constructed capacities) within the specified balancing area. The capacity factors,
          which vary by hour and location, are considered by accessing `solarcf_df`.
        - The function also incorporates extant solar generation capacities (`extant_solar_gen`) that are predefined 
          for the specific hour and balancing area.
        - The output capacity for each generator is adjusted based on typical resource availability during that hour,
          which is accessed from a dataframe (`solarcf_df`).

    Note:
        Ensure that the `solarcf_df` dataframe and the `extant_solar_gen` data structure and extant_wind_solar.csv are properly set up and populated
        with accurate capacity factor data and existing generation levels before using this function in your research.
    """
    ind = pds.index(PD)
    return model.solarout[PD, H, ABA] <= quicksum(
        (
                model.capacity_solar[PDD, GL]
                + model.capacity_solar_recon[PDD, GL]) * solarcf_df.loc[H, int(GL)] for PDD in pds[:ind + 1] \
        for GL in gl if ABA == map_gl_to_ba[int(GL)]) \
        + extant_solar_gen.loc[(PD, H), ABA]


model.solarg = Constraint(pds, h, aba, rule=solarg)

# Deleting windcf and solarcf to free some memory
del windcf_df
del solarcf_df


# Limiting offshore wind reconstruction capacity to existing end of life offshore wind capacity
def wind_ofs_recon(model, PD, GL):
    """
    Constrains the reconstruction capacity of offshore wind facilities to not exceed the existing end-of-life 
    capacity available for those facilities. This ensures that reconstructed capacity additions are within 
    the limits of previously installed capacities that are due for replacement or have been phased out.

    Parameters:
        model: A Pyomo model instance containing decision variables, parameters, and sets for the energy model.
        PD: The planning period as a string (e.g., '2025') for which the constraint is applied.
        GL: The grid location identifier for the offshore wind facility.

    Returns:
        A Pyomo Constraint expression: Limits the capacity of reconstructed offshore wind facilities in location GL 
        during the planning period PD to the sum of the designated reconstruction capacity and any unused 
        reconstruction capacity from prior periods.
    """
    ind = pds.index(PD)
    return model.capacity_wind_ofs_recon[PD, GL] <= wind_ofs_recon_capacity[PD + '.' + GL] + quicksum(
        wind_ofs_recon_capacity[PDD + '.' + GL] - model.capacity_wind_ofs_recon[PDD, GL] for PDD in pds[:ind])


model.wind_ofs_recon = Constraint(pds, gl, rule=wind_ofs_recon)


# Limiting onshore wind reconstruction capacity to existing end of life onshore wind capacity
def wind_ons_recon(model, PD, GL):
    """same as wind_ofs_recon() but for onshore wind facilities"""
    ind = pds.index(PD)
    return model.capacity_wind_ons_recon[PD, GL] <= wind_ons_recon_capacity[PD + '.' + GL] + quicksum(
        wind_ons_recon_capacity[PDD + '.' + GL] - model.capacity_wind_ons_recon[PDD, GL] for PDD in pds[:ind])


model.wind_ons_recon = Constraint(pds, gl, rule=wind_ons_recon)


# Limiting solar reconstruction capacity to existing end of life solar capacity
def solar_recon(model, PD, GL):
    """same as wind_ofs_recon() but for solar facilities"""
    ind = pds.index(PD)
    return model.capacity_solar_recon[PD, GL] <= solar_recon_capacity[PD + '.' + GL] + quicksum(
        solar_recon_capacity[PDD + '.' + GL] - model.capacity_solar_recon[PDD, GL] for PDD in pds[:ind])


model.solar_recon = Constraint(pds, gl, rule=solar_recon)


# Load shedding constraint for planning reserve (inclusion of us demand)
def loadShedding(model, PD, H, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the load shedding constraint is relevant.
        ABA: The balancing area as a string, indicating where the load shedding is being evaluated.

    Returns:
        A Pyomo Constraint expression: Limits the load shedding in a specified balancing area (ABA) during a given hour 
        (H) of the planning period (PD) to not exceed the sum of the local demand from `demand_df` and any additional 
        demand from the U.S. (`demand_us`) us_demand.csv input sheet, if applicable.

    Details:
        - This function determines the maximum permissible load shedding for each balancing area by comparing the planned 
          load shedding to the total demand, which includes local demand plus any cross-border electricity demand from 
          the U.S. linked to the specific hour and balancing area.
        - The constraint ensures that the amount of electricity curtailed during load shedding does not surpass the 
          combined demand, thus maintaining supply and demand balance while considering interconnections with neighboring 
          regions.
        - Load shedding may be considered as a last resort to balance the grid in situations where supply cannot meet 
          demand, and this constraint helps manage the extent to which this measure can be applied.
    """
    aux = [1]
    return model.load_shedding[PD, H, ABA] <= demand_df.loc[(PD, H), ABA] + quicksum(
        demand_us[ABA + '.' + str(H)] for i in aux if ABA + '.' + str(H) in demand_us)


model.loadShedding = Constraint(pds, h, aba, rule=loadShedding)

###supply and demand balance
aux = [1]


def demsup(model, PD, H, ABA):
    """
    Constrains the total energy supply within a specific balancing area during a designated hour and planning period to 
    meet or exceed the total demand, factoring in all energy generation types, storage operations, and renewable energy 
    renewals.

    Parameters:
        model (Pyomo model): instance containing decision variables, parameters, and sets relevant to the energy model.
        PD (str): The planning period as a string (e.g., '2025') during which the constraint is applied.
        H (str): The specific hour within the planning period for which the supply and demand balance is evaluated.
        ABA (str): The balancing area as a string, indicating where the energy supply and demand balance is being considered.

    Returns:
        A Pyomo Constraint expression: Ensures that the total energy supply from all sources including thermal plants, 
        wind, solar, hydro, storage, and any renewable energy renewals meets or exceeds the demand, including transmission 
        losses and potential load shedding within the area.

    Details:
        - The function aggregates energy supply from various sources:
          * Conventional thermal plant supply (`TP_supply`).
          * Wind and solar outputs (`wind_solar_supply`).
          * Hydro outputs from different hydro technologies (`hydro_supply`).
          * Net storage output (`storage_supply`).
          * Renewable energy supply from renewable sources under development (`renewal_supply`).
        - It accounts for adjustments like load shedding and transmission losses to ensure a realistic operational 
          balance.
        - The demand is adjusted for cross-border exchanges (`demand_us`) and includes external power flows considering 
          transmission losses between areas.
    Note:
        Ensure that all necessary parameters such as `demand_df`, `demand_us` demand/us_demand.csv, `transmap` extant_transmission.csv, and transmission loss 
        coefficients are properly set up and populated with accurate data in config.toml before using this function in a model.
    """
    TP_supply = quicksum(model.supply[PD, H, ABA, TP] for TP in tplants)
    wind_solar_supply = model.windonsout[PD, H, ABA] + model.windofsout[PD, H, ABA] + model.solarout[PD, H, ABA]
    hydro_supply = ror_hydroout[PD + '.' + str(H) + '.' + ABA] + model.daystoragehydroout[PD, H, ABA] + \
                   model.monthstoragehydroout[PD, H, ABA]
    storage_supply = quicksum(model.storageout[PD, ST, H, ABA] - model.storagein[PD, ST, H, ABA] for ST in st)
    renewal_supply = 0
    if hydro_development:
        renewal_supply = quicksum(
            ror_renewalout[str(H) + '.' + HR_ROR] * model.ror_renewal_binary[PD, HR_ROR] for HR_ROR in hr_ror if
            ABA == hr_ror_location[HR_ROR]) \
                         + quicksum(
            model.dayrenewalout[PD, H, HR_DAY] for HR_DAY in hr_day if ABA == hr_day_location[HR_DAY]) \
                         + quicksum(
            model.monthrenewalout[PD, H, HR_MO] for HR_MO in hr_mo if ABA == hr_month_location[HR_MO])
    return TP_supply + wind_solar_supply + hydro_supply + storage_supply + renewal_supply >= demand_df.loc[
        (PD, H), ABA] + quicksum(demand_us[ABA + '.' + str(H)] for i in aux if ABA + '.' + str(H) in demand_us) - \
        model.load_shedding[PD, H, ABA] \
        + quicksum(model.transmission[PD, H, ABA, ABBA] - (1 - transloss[ABBA + '.' + ABA]) * model.transmission[
            PD, H, ABBA, ABA] for ABBA in aba if ABA + '.' + ABBA in transmap)


model.demsup = Constraint(model.pds, model.h, model.aba, rule=demsup)

###maximum annual emissions for backup thermal plants
def maxannualemissions(model, PD, ABA, FTP):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the emissions constraint is applied.
        ABA: The balancing area as a string, indicating where the fossil fuel thermal plant is located.
        FTP: The type of fossil fuel thermal plant (e.g., 'coal', 'gas').

    Returns:
        A Pyomo Constraint expression: Ensures that the annual carbon dioxide emissions from a specific type of 
        fossil fuel thermal power plant (FTP) in the specified balancing area (ABA) do not exceed the emissions 
        capacity limit, calculated based on the plant's capacity and a maximum emissions limit (`max_em_limit`).

    Details:
        - This function calculates the total annual CO2 emissions by multiplying the hourly power supply by the 
          CO2 emission factor for each type of plant, adjusted for the number of run days in a year to provide an 
          annualized value.
        - The allowable emissions are determined by the net capacity of the plant, factoring in any new installations 
          or retirements up to the planning period, and multiplied by the `max_em_limit`, which is a regulatory 
          limit set in the configuration file (`config.toml` under 'Regulations' -> 'CER' -> 'max_em_limit').
        - The constraint is critical for compliance with environmental regulations that aim to limit greenhouse gas 
          emissions from thermal power generation, contributing to broader climate action goals.
        - only applied if flag_cer and applies for hard coded years 2035, 2040, 2045, and 2050.
    """

    ind = pds.index(PD)
    return sum(model.supply[PD, H, ABA, FTP] * carbondioxide[FTP] for H in h) * (365 / len(rundays)) <= (
                sum(model.capacity_therm[PDD, ABA, FTP] - model.retire_therm[PDD, ABA, FTP] for PDD in pds[:ind + 1]) +
                extant_thermal[pds[0] + '.' + ABA + '.' + FTP]) * max_em_limit
                #max_em_limit is set in config.toml ['Regulations']['CER']['max_em_limit']

if flag_cer:
    model.maxannualemissions = Constraint(['2035', '2040', '2045', '2050'], aba, ftplants, rule=maxannualemissions)


###maximum annual capacity factor for thermal plants
def maxcapfactor(model, PD, ABA, TP):
    """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            ABA: The balancing area as a string, indicating where the thermal plant is located.
            TP: The type of thermal plant as a string.

        Returns:
            A Pyomo Constraint expression: Limits the total generation from a specific type of thermal power plant (TP) 
            within a balancing area (ABA) during the planning period (PD) to not exceed the product of its maximum 
            capacity factor, the total hours in the period, and the net capacity of the plant after accounting for new 
            installations and retirements.

        Details:
            - This constraint calculates the allowable generation by multiplying the net capacity (including both extant 
            capacity at the start of the planning period and any capacity adjustments due to installations or retirements) 
            by the number of hours in the period and the maximum capacity factor specified for that plant type (`max_cap_fact[TP]`).
            - The maximum capacity factor, which represents the maximum percentage of time the plant can operate at full 
            capacity over the planning period, is configured in an external data source (`generation_type_data.csv`).
            - This constraint is critical for ensuring that thermal power plants operate within their technical and 
            operational limits, aiding in reliable energy production and planning while preventing over-utilization of 
            resources.
    """
    ind = pds.index(PD)
    return quicksum(model.supply[PD, H, ABA, TP] for H in h) <= (
                quicksum(model.capacity_therm[PDD, ABA, TP] - model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind + 1])
                + extant_thermal[pds[0] + '.' + ABA + '.' + TP]) * hours * max_cap_fact[TP]


model.maxcapfactor = Constraint(pds, aba, tplants, rule=maxcapfactor)


##transmission capacity constraint
def transcap(model, PD, H, ABA, ABBA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the transmission capacity constraint is applied.
        H: The hour within the planning period for which the transmission constraint is relevant.
        ABA: The originating area balancing authority as a string.
        ABBA: The receiving area balancing authority as a string.

    Returns:
        A Pyomo Constraint expression: Limits the electricity transmission between two specific area balancing authorities 
        (ABA and ABBA) to not exceed the sum of the planned and existing transmission capacities.

    Details:
        - This function calculates the maximum allowable transmission capacity between two balancing authorities by 
          summing up the capacities planned up to and including the specified planning period and any existing capacities 
          noted at the beginning of the period.
        - The capacities considered include those added in previous periods and still existing, ensuring that the limit reflects
          both the physical capacity and the operational constraints.
        - The constraint ensures that the transmission does not exceed infrastructure limits, supporting reliable and 
          efficient grid operations and facilitating compliance with regulatory standards for inter-regional electricity transfers.
    """

    ind = pds.index(PD)
    return model.transmission[PD, H, ABA, ABBA] <= (quicksum(
        model.capacity_transmission[PDD, ABA, ABBA] for PDD in pds[:ind + 1] for i in aux if
        ABA + '.' + ABBA in transmap)
                                                    + quicksum(
                extant_transmission[pds.index(PD)][ABA + '.' + ABBA] for i in aux if
                ABA + '.' + ABBA in extant_transmission[pds.index(PD)]))


model.transcap = Constraint(pds, h, aba, abba, rule=transcap)

##constrained transmission expansion
if CTE_extant:
    def transmission_expansion_cap(model, ABA, ABBA):
        """
        existing transmission extant_transmission values are sourced from 'extant_transmission.csv' input sheet.
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the transmission capacity constraint is applied.
            H: The hour within the planning period for which the transmission constraint is relevant.
            ABA: The originating area balancing authority as a string.
            ABBA: The receiving area balancing authority as a string.

        Returns:
            A Pyomo Constraint expression: Limits the electricity transmission between two specific area balancing authorities 
            (ABA and ABBA) to not exceed the sum of the planned and existing transmission capacities.

        Details:
            - This function calculates the maximum allowable transmission capacity between two balancing authorities by 
            summing up the capacities planned up to and including the specified planning period and any existing capacities 
            noted at the beginning of the period.
            - The capacities considered include those added in previous periods and those that are still in existance.
            - The constraint ensures that the transmission capacity does not exceed potential build limits.
        """

        return quicksum(model.capacity_transmission[PD, ABA, ABBA] for PD in pds) <= CTE_coef * quicksum(
            extant_transmission[0][ABA + '.' + ABBA] for i in aux if ABA + '.' + ABBA in extant_transmission[0])


    model.TEcap = Constraint(aba, abba, rule=transmission_expansion_cap)
elif CTE_custom:
    def transmission_expansion_custom_cap(model, ABA, ABBA):
        """
        Evaluates the transmission expansion capability for a given pair of balancing areas.

        This function checks the total capacity of transmission between two specified balancing areas
        against the predefined expansion limits. If the combination of these areas is listed in the
        transmission expansion limits, it uses that limit; otherwise, it defaults to a high limit of 100000.

        Parameters:
            model (Model): The optimization model that includes the transmission capacities.
            ABA (str): The abbreviation of the first balancing area.
            ABBA (str): The abbreviation of the second balancing area (connected to ABA).

        Returns:
            Constraint: A constraint expression that ensures the sum of transmission capacities from
        all planning decisions (PD) between ABA and ABBA does not exceed the predefined limit (TEL).

        Note: 
            The function assumes that 'trans_expansion_limits', 'model.capacity_transmission', and 'pds'
            are accessible within the scope where this function is called.
        """
        if ABA + '.' + ABBA in trans_expansion_limits:
            TEL = trans_expansion_limits[ABA + '.' + ABBA]
        else:
            TEL = 100000

        return quicksum(model.capacity_transmission[PD, ABA, ABBA] for PD in pds) <= TEL


    model.TEcap = Constraint(aba, abba, rule=transmission_expansion_custom_cap)


#####capacity constraints for thermal plants
def cap(model, PD, H, ABA, TP):
    """
    Calculate and enforce the capacity constraint for a given planning district, hour, area balancing authority, and time period.
    This function ensures that the supply does not exceed the sum of the thermal capacities minus retirements up to and including 
    the current planning district, adjusted by the extant thermal capacity.

    Parameters:
        model (Pyomo Object): The optimization model object, which includes parameters and sets used in the calculation.
        PD (str): the time period under consideration as a string.
        H: The specific hour for which the capacity constraint is being calculated.
        ABA: The area balancing authority identifier, which specifies the region under consideration.
		TP (str): Themal Plant Type
    Returns:
    constraint : Constraint
        A constraint object that limits the supply for the specified inputs to not exceed the calculated capacity.
    Note:
        The capacity is calculated as the sum of the capacities from all the planning periods up to and including the current one, adjusted for any retirements and added to the initial extant thermal capacity for the area and time period.
    """
    ind = pds.index(PD)
    return model.supply[PD, H, ABA, TP] <= quicksum(
        model.capacity_therm[PDD, ABA, TP] - model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind + 1]) + \
        extant_thermal[pds[0] + '.' + ABA + '.' + TP]


model.cap = Constraint(pds, h, aba, tplants, rule=cap)

### this constraint limits the PHS retrofit capacity to percentage of available hydro reservior facility in each BA
if storage_continous:
    def pumpretrofitlimit(model, PD, ABA):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            ABA: The balancing area as a string, indicating where the capacity constraint is being considered.

        Returns:
            A Pyomo Constraint expression: Limits the total retrofitted pumped hydro storage capacity up to the 
            planning period PD in the balancing area ABA to not exceed the sum of the daily and monthly hydro generator capacities 
            at the start of the period series, multiplied by a config.toml limit factor (`pump_ret_limit`).

        Details:
            - This function sums the capacities of pumped hydro storage (identified by 'storage_PH') for all previous 
            periods up to and including PD.
            - The limit for the total capacity is set based on a percentage of the initial day and month hydro capacities,
            facilitating controlled growth in storage capacity based on existing hydro infrastructure.
            - The calculation takes into account the sum of daily and monthly hydro reservoir capacities at the model's start and
            applies a factor (`pump_ret_limit`) to set the maximum allowable retrofitted capacity.
        """
        ind = pds.index(PD)
        return quicksum(model.capacity_storage[PDD, 'storage_PH', ABA] for PDD in pds[:ind + 1]) <= (
                    day_hydro_capacity[pds[0] + '.' + ABA] + month_hydro_capacity[pds[0] + '.' + ABA]) * pump_ret_limit


    model.pumpretrofitlimit = Constraint(pds, aba, rule=pumpretrofitlimit)

def pumpen(model, PD, ST, H, ABA):
    """
    pumped hydro energy storage - energy balance constraint
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the energy storage calculations are performed.
        ST: The storage type identifier as a string.
        H: The hour within the planning period for which the energy balance is calculated.
        ABA: The balancing area as a string, indicating where the energy storage is located.

    Returns:
        A Pyomo Constraint expression: Equates the energy content in storage at hour H + time_diff[H] to the energy 
        content at hour H, adjusted for outflows, inflows, and efficiency losses/gains.

    Details:
        - The function calculates the energy balance for a storage unit by updating its energy content from one hour to the next.
        - It considers the energy output from the storage (`model.storageout`), the energy input into the storage (`model.storagein`),
          and the storage efficiency (`storage_efficiency[ST]`), which scales the energy input.
        - The `time_diff[H]` is used to link consecutive operational hours, ensuring continuity in the energy accounting.
    """
    return model.storageenergy[PD, ST, H + time_diff[H], ABA] == model.storageenergy[PD, ST, H, ABA] - model.storageout[
        PD, ST, H, ABA] + model.storagein[PD, ST, H, ABA] * storage_efficiency[ST]


model.pumpen = Constraint(pds, st, h2, aba, rule=pumpen)


######pumped hydro energy storage
def pumcap(model, PD, ST, H, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2040') during which the constraint is applied.
        ST: The storage type identifier as a string, specifically referring to types like 'storage_LI'.
        H: The hour within the planning period for which the storage capacity constraint is relevant.
        ABA: The balancing area as a string, indicating where the energy storage is located.

    Returns:
        A Pyomo Constraint expression: Limits the energy content in the storage facility at hour H during the planning 
        period PD to not exceed the sum of the base area storage capacity, potential new capacity additions, and any 
        capacity attributed to hydro development, scaled by the number of operational hours the storage can cover.

    Details:
        - This function calculates the maximum allowable storage capacity for a given type of storage in a specific 
          balancing area and hour, considering both newly constructed capacity and capacity from hydro development.
        - The capacity for new construction (`pump_new_con`) is calculated differently based on the storage type and 
          the planning period, with specific conditions for 'storage_LI' starting from 2040.
        - If hydro development is active and storage is not continuous, additional capacity from pumped hydro retrofits 
          (`pump_integer_cap`) is considered, based on the location-specific renewal capacity.
        - The total permissible storage capacity is calculated by adding the base storage capacity from `ba_storage_capacity` 
          to the capacities calculated from new construction and hydro development, then multiplying by the operational 
          hours specified in `storage_hours[ST]`.
    """
    ind = pds.index(PD)
    pump_new_con = 0
    pump_integer_cap = 0

    if storage_continous:
        if ST == 'storage_LI' and int(PD) >= 2040:
            pump_new_con = quicksum(model.capacity_storage[PDD, ST, ABA] for PDD in pds[ind - 2:ind + 1])
        else:
            pump_new_con = quicksum(model.capacity_storage[PDD, ST, ABA] for PDD in pds[:ind + 1])
    if hydro_development and not storage_continous:
        pump_integer_cap = quicksum(
            model.pumphydro[PDD, HR_PUMP] * capacity_pump_renewal[HR_PUMP] for PDD in pds[:ind + 1] for HR_PUMP in
            hr_pump if hr_pump_location[HR_PUMP] == ABA)
    return model.storageenergy[PD, ST, H, ABA] <= (
                ba_storage_capacity[ABA + '.' + ST] + pump_integer_cap + pump_new_con) * storage_hours[ST]


model.pumcap = Constraint(pds, st, h, aba, rule=pumcap)


######pump hydro power capacity - maximum storage out
def storageoutmax(model, PD, ST, H, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2040') during which the constraint is applied.
        ST: The storage type identifier as a string.
        H: The hour within the planning period for which the output capacity constraint is relevant.
        ABA: The balancing area as a string, indicating where the storage is located.

    Returns:
        A Pyomo Constraint expression: Limits the output from the storage facility at hour H during the planning 
        period PD to not exceed the sum of the base area storage capacity, potential new capacity additions, and 
        any capacity attributed to hydro development if applicable.

    Details:
        - The function determines the maximum allowable output from storage facilities by accounting for base 
          storage capacity and additional capacities from new constructions or hydro developments.
        - For continuous storage scenarios, the function calculates additional new capacity based on the type 
          of storage and the specific conditions from the planning period.
        - If hydro development is active and storage operations are not continuous, the function includes additional 
          capacity from hydro pump retrofits based on specific conditions.
        - The total permissible output from the storage facility is then set by summing the base capacity and any 
          additional capacities, providing a limit to ensure that the output does not exceed operational capabilities.
    """
    ind = pds.index(PD)
    pump_new_con = 0
    pump_integer_cap = 0

    if storage_continous:
        if ST == 'storage_LI' and int(PD) >= 2040:
            pump_new_con = quicksum(model.capacity_storage[PDD, ST, ABA] for PDD in pds[ind - 2:ind + 1])
        else:
            pump_new_con = quicksum(model.capacity_storage[PDD, ST, ABA] for PDD in pds[:ind + 1])
    if hydro_development and not storage_continous:
        pump_integer_cap = quicksum(
            model.pumphydro[PDD, HR_PUMP] * capacity_pump_renewal[HR_PUMP] for PDD in pds[:ind + 1] for HR_PUMP in
            hr_pump if hr_pump_location[HR_PUMP] == ABA)
    return model.storageout[PD, ST, H, ABA] <= ba_storage_capacity[ABA + '.' + ST] + pump_integer_cap + pump_new_con


model.storageoutmax = Constraint(pds, st, h, aba, rule=storageoutmax)


#####pump hydro storage capacity - maximum storage in
def storageinmax(model, PD, ST, H, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2040') during which the constraint is applied.
        ST: The storage type identifier as a string.
        H: The hour within the planning period for which the input capacity constraint is relevant.
        ABA: The balancing area as a string, indicating where the storage is located.

    Returns:
        A Pyomo Constraint expression: Limits the input to the storage facility at hour H during the planning 
        period PD to not exceed the sum of the base area storage capacity, potential new capacity additions, and 
        any capacity attributed to hydro development if applicable.
    Details:
        - The function calculates the maximum permissible input to storage facilities, accounting for base 
          storage capacity and additional capacities from new constructions or hydro developments.
        - For continuous storage scenarios, it computes additional new capacity based on the storage type 
          and specific conditions from the planning period.
        - In scenarios involving hydro development with non-continuous storage, the function includes additional 
          capacity from hydro pump retrofits, specific to conditions and locations.
        - This constraint helps manage the inflow to storage facilities, ensuring that inputs do not exceed 
          the facility's capacity to store energy efficiently and safely.
    """
    ind = pds.index(PD)

    pump_new_con = 0
    pump_integer_cap = 0

    if storage_continous:
        if ST == 'storage_LI' and int(PD) >= 2040:
            pump_new_con = quicksum(model.capacity_storage[PDD, ST, ABA] for PDD in pds[ind - 2:ind + 1])
        else:
            pump_new_con = quicksum(model.capacity_storage[PDD, ST, ABA] for PDD in pds[:ind + 1])
    if hydro_development and not storage_continous:
        pump_integer_cap = quicksum(
            model.pumphydro[PDD, HR_PUMP] * capacity_pump_renewal[HR_PUMP] for PDD in pds[:ind + 1] for HR_PUMP in
            hr_pump if hr_pump_location[HR_PUMP] == ABA)

    return model.storagein[PD, ST, H, ABA] <= ba_storage_capacity[ABA + '.' + ST] + pump_integer_cap + pump_new_con


model.storageinmax = Constraint(pds, st, h, aba, rule=storageinmax)

##### forces the first hour of months energy to zero
m_counter = 2
fhom = list()
for H in h:
    if map_hm[H] == m_counter:
        fhom.append(H)
        m_counter += 1


def storage_rest_monthly(model, PD, ST, FHOM, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        ST: The storage type identifier as a string.
        FHOM: The first hour of the month within the planning period for which the storage energy is set to zero.
        ABA: The balancing area as a string, indicating where the storage is located.

    Returns:
        A Pyomo Constraint expression: Forces the energy level in the storage facility to be zero at the first hour 
        of each month during the planning period, for the specified storage type and balancing area.

    Details:
        - This function is used to simulate operational resets or policy-driven requirements where storage facilities 
          must start each month with zero stored energy.
        - It helps in modeling scenarios where monthly operational strategies are recalibrated or where regulatory 
          requirements necessitate such conditions.
    """
    return model.storageenergy[PD, ST, FHOM, ABA] == 0


model.storage_rest_monthly = Constraint(pds, st, fhom, aba, rule=storage_rest_monthly)


#####hydro storage for systems with intra-day storage
def hydro_daystorage(model, PD, D, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        D: The day within the planning period for which the output from day-storage hydro is constrained.
        ABA: The balancing area as a string, indicating where the day-storage hydro facility is located.

    Returns:
        A Pyomo Constraint expression: Limits the total output from day-storage hydroelectric facilities in the 
        specified balancing area (ABA) during the specified day (D) of the planning period (PD) to not exceed 
        the historical daily output capacity.

    Details:
        - This function aggregates the hourly outputs for a specific day-storage hydro facility within a 
          designated balancing area and ensures that the total does not surpass historical operation limits.
        - It utilizes a mapping (`map_hd`) to associate each hour with its corresponding day within the planning 
          period, facilitating the aggregation of hourly outputs to enforce the daily constraint based on historic 
          capacity data (`day_hydro_historic`).
    """
    return quicksum(model.daystoragehydroout[PD, H, ABA] for H in h if map_hd[H] == D) <= day_hydro_historic[
        PD + '.' + str(D) + '.' + ABA]


model.hydro_daystorage = Constraint(pds, d, aba, rule=hydro_daystorage)

if hydro_development:
    def hydro_dayrenewal(model, PD, D, HR_DAY):
        """
        Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        D: The day within the planning period for which the output from renewable day-storage hydro is constrained.
        HR_DAY: The identifier for the renewable day-storage hydro facility.

        Returns:
        A Pyomo Constraint expression: Limits the total output from renewable day-storage hydroelectric facilities 
        identified by HR_DAY during the specified day (D) of the planning period (PD) to not exceed the historical 
        daily output capacity.

        Details:
        - This function aggregates the hourly outputs for the specified renewable day-storage hydro facility and 
          ensures that the total does not exceed the historical daily operation limits.
        - It utilizes a mapping (`map_hd`) to determine which hours belong to the specified day (D), aiding in the 
          aggregation of hourly outputs to enforce the daily constraint based on historic capacity data 
          (`day_renewal_historic`).
        """
        return quicksum(model.dayrenewalout[PD, H, HR_DAY] for H in h if map_hd[H] == D) <= day_renewal_historic[
            str(D) + '.' + HR_DAY]


    model.hydro_dayrenewal = Constraint(pds, d, hr_day, rule=hydro_dayrenewal)

try:
    ###hydro storage for systems with intra-month storage
    def hydro_monthstorage(model, PD, M, ABA):
        """
        Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        M: The month within the planning period for which the output from month-storage hydro is constrained.
        ABA: The balancing area as a string, indicating where the month-storage hydro facility is located.

        Returns:
        A Pyomo Constraint expression: Limits the total output from month-storage hydroelectric facilities in the 
        specified balancing area (ABA) during the specified month (M) of the planning period (PD) to not exceed 
        the historical monthly output capacity.

        Details:
        - This function aggregates the hourly outputs for a specific month-storage hydro facility within a designated 
          balancing area and ensures that the total does not surpass historical operation limits.
        - It utilizes a mapping (`map_hm`) to associate each hour with its corresponding month within the planning 
          period, facilitating the aggregation of hourly outputs to enforce the monthly constraint based on historic 
          capacity data (`month_hydro_historic`).
        """
        return quicksum(model.monthstoragehydroout[PD, H, ABA] for H in h if map_hm[H] == M) <= month_hydro_historic[
            PD + '.' + str(M) + '.' + ABA]


    model.hydro_monthstorage = Constraint(pds, m, aba, rule=hydro_monthstorage)

    if hydro_development:
        def hydro_monthrenewal(model, PD, M, HR_MO):
            """
            Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            M: The month within the planning period for which the output from renewable month-storage hydro is constrained.
            HR_MO: The identifier for the renewable month-storage hydro facility.

            Returns:
            A Pyomo Constraint expression: Limits the total output from renewable month-storage hydroelectric facilities 
            identified by HR_MO during the specified month (M) of the planning period (PD) to not exceed the historical 
            monthly output capacity.

            Details:
            - This function aggregates the hourly outputs for the specified renewable month-storage hydro facility and 
            ensures that the total does not exceed the historical monthly operation limits.
            - It utilizes a mapping (`map_hm`) to determine which hours belong to the specified month (M), aiding in the 
            aggregation of hourly outputs to enforce the monthly constraint based on historic capacity data 
            (`month_renewal_historic`).
            """
            return quicksum(model.monthrenewalout[PD, H, HR_MO] for H in h if map_hm[H] == M) <= month_renewal_historic[
                str(M) + '.' + HR_MO]


        model.hydro_monthrenewal = Constraint(pds, m, hr_mo, rule=hydro_monthrenewal)
except ValueError as e:
    if "expression resolved to a trivial Boolean (True)" in str(e):
        raise ValueError(
            f"You might have specified rundays in a way, that don't represent consecutive months. {rundays=}") from e


####hydro minimum flow constraints for systems with intra-day storage
def hydro_dayminflow(model, PD, H, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the minimum flow constraint is relevant.
        ABA: The balancing area as a string, indicating where the day-storage hydro facility is located.

    Returns:
        A Pyomo Constraint expression: Ensures that the hourly output from day-storage hydroelectric facilities 
        in the specified balancing area (ABA) during the specified hour (H) of the planning period (PD) does not 
        fall below the historical minimum flow requirement.

    Details:
        - This function enforces a minimum flow output for day-storage hydro facilities to ensure ecological and 
          operational stability.
        - The minimum flow requirement (`day_minflow`) is specified for each planning period and balancing area, 
          reflecting regulatory or ecological standards that must be maintained.
    """
    return model.daystoragehydroout[PD, H, ABA] >= day_minflow[PD + '.' + ABA]


model.hydro_dayminflow = Constraint(pds, h, aba, rule=hydro_dayminflow)

if hydro_development:
    def renewal_dayminflow(model, PD, H, HR_DAY):
        """
        Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the minimum flow constraint is relevant.
        HR_DAY: The identifier for the renewable day-storage hydro facility.

        Returns:
        A Pyomo Constraint expression: Ensures that the hourly output from renewable day-storage hydro facilities,
        identified by HR_DAY, during the specified hour (H) of the planning period (PD) does not fall below a 
        calculated minimum flow requirement.

        Details:
        - The minimum flow is calculated as the product of the renewable facility's capacity (`capacity_day_renewal[HR_DAY]`), 
          a predefined minimum flow coefficient (`hydro_minflow`), and a binary variable that indicates whether the facility 
          is operational (`model.day_renewal_binary[PD, HR_DAY]`) during that hour.
        - This constraint is crucial for maintaining ecological balance and ensuring regulatory compliance regarding 
          water flow rates from hydro facilities.
        """
        return model.dayrenewalout[PD, H, HR_DAY] >= capacity_day_renewal[HR_DAY] * hydro_minflow * \
            model.day_renewal_binary[PD, HR_DAY]


    model.renewal_dayminflow = Constraint(pds, h, hr_day, rule=renewal_dayminflow)


####hydro minimum flow constraints for systems with intra-month storage
def hydro_monthminflow(model, PD, H, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the minimum flow constraint is relevant.
        ABA: The balancing area as a string, indicating where the month-storage hydro facility is located.

    Returns:
        A Pyomo Constraint expression: Ensures that the hourly output from month-storage hydroelectric facilities 
        in the specified balancing area (ABA) during the specified hour (H) of the planning period (PD) does not 
        fall below the historical minimum flow requirement.

    Details:
        - This function enforces a minimum flow output for month-storage hydro facilities to ensure ecological, 
          operational, and regulatory compliance.
        - The minimum flow requirement (`month_minflow`) is specified for each planning period and balancing area, 
          reflecting constraints that ensure the stability and sustainability of water use and ecological impacts.
    """
    return model.monthstoragehydroout[PD, H, ABA] >= month_minflow[PD + '.' + ABA]


model.hydro_monthminflow = Constraint(pds, h, aba, rule=hydro_monthminflow)

if hydro_development:
    def renewal_monthminflow(model, PD, H, HR_MO):
        """
        Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the minimum flow constraint is relevant.
        HR_MO: The identifier for the renewable month-storage hydro facility.

        Returns:
        A Pyomo Constraint expression: Ensures that the hourly output from renewable month-storage hydro facilities,
        identified by HR_MO, during the specified hour (H) of the planning period (PD) does not fall below a 
        calculated minimum flow requirement.

        Details:
        - The minimum flow is determined by multiplying the renewable facility's designated capacity (`capacity_month_renewal[HR_MO]`), 
        a specific minimum flow coefficient (`hydro_minflow`), and a binary operation status indicator (`model.month_renewal_binary[PD, HR_MO]`).
        - This constraint supports regulatory compliance and ecological stability by ensuring consistent water flow from 
        renewable hydro facilities.
        """
        return model.monthrenewalout[PD, H, HR_MO] >= capacity_month_renewal[HR_MO] * hydro_minflow * \
            model.month_renewal_binary[PD, HR_MO]


    model.renewal_monthminflow = Constraint(pds, h, hr_mo, rule=renewal_monthminflow)


##hydro capacity constraints for systems with intra-day storage
def hydro_daycap(model, PD, H, ABA):

    return model.daystoragehydroout[PD, H, ABA] <= day_hydro_capacity[PD + '.' + ABA]


model.hydro_daycap = Constraint(pds, h, aba, rule=hydro_daycap)

if hydro_development:
    def renewal_daycap(model, PD, H, HR_DAY):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            H: The hour within the planning period for which the capacity constraint is relevant.
            ABA: The balancing area as a string, indicating where the day-storage hydro facility is located.

        Returns:
            A Pyomo Constraint expression: Limits the hourly output from day-storage hydroelectric facilities in the 
            specified balancing area (ABA) during the specified hour (H) of the planning period (PD) to not exceed 
            the designated capacity limit.

        Details:
            - This function ensures that the output from day-storage hydro facilities does not surpass the maximum capacity 
            specified for that facility in the balancing area.
            - The maximum capacity (`day_hydro_capacity`) is predefined for each planning period and balancing area, reflecting
            operational and regulatory limits that ensure the sustainability and efficiency of hydro facility operations.
        """
        return model.dayrenewalout[PD, H, HR_DAY] <= capacity_day_renewal[HR_DAY] * model.day_renewal_binary[PD, HR_DAY]


    model.renewal_daycap = Constraint(pds, h, hr_day, rule=renewal_daycap)


#####hydro capacity constraints for systems with intra-month storage
def hydro_monthcap(model, PD, H, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the capacity constraint is relevant.
        ABA: The balancing area as a string, indicating where the month-storage hydro facility is located.

    Returns:
        A Pyomo Constraint expression: Limits the hourly output from month-storage hydroelectric facilities in the 
        specified balancing area (ABA) during the specified hour (H) of the planning period (PD) to not exceed 
        the designated capacity limit.

    Details:
        - This function ensures that the output from month-storage hydro facilities does not exceed the maximum capacity 
          specified for that facility in the balancing area.
        - The maximum capacity (`month_hydro_capacity`) is predefined for each planning period and balancing area, 
          providing a limit that aligns with operational and regulatory requirements to ensure the stability and 
          efficiency of hydro facility operations.
    """
    return model.monthstoragehydroout[PD, H, ABA] <= month_hydro_capacity[PD + '.' + ABA]


model.hydro_monthcap = Constraint(pds, h, aba, rule=hydro_monthcap)

if hydro_development:
    def renewal_monthcap(model, PD, H, HR_MO):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            H: The hour within the planning period for which the capacity constraint is relevant.
            HR_MO: The identifier for the renewable month-storage hydro facility.

        Returns:
            A Pyomo Constraint expression: Limits the hourly output from renewable month-storage hydroelectric facilities, 
            identified by HR_MO, during the specified hour (H) of the planning period (PD) based on its designated 
            renewable capacity and whether it is operational during that period.

        Details:
            - This function ensures that the output from renewable month-storage hydro facilities does not exceed the
            maximum capacity designated for that facility, multiplied by a binary variable indicating whether the facility 
            is operational (`model.month_renewal_binary[PD, HR_MO]`) in that hour.
            - The constraint uses the product of the facility's renewable capacity (`capacity_month_renewal[HR_MO]`) and 
            the operational status to determine the allowable output, aligning with strategies for flexible and sustainable 
            hydro facility operation.
        """
        return model.monthrenewalout[PD, H, HR_MO] <= capacity_month_renewal[HR_MO] * model.month_renewal_binary[
            PD, HR_MO]


    model.renewal_monthcap = Constraint(pds, h, hr_mo, rule=renewal_monthcap)

###End of second economic dispatch constraints

##### The following constraints ensure that the model does not build a hydro renewal or greenfield project more than one time during all periods (pds)
if hydro_development:
    def ror_onetime(model, PD, HR_ROR):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            HR_ROR: The identifier for the run-of-river (RoR) hydro facility.

        Returns:
            A Pyomo Constraint expression: Ensures that if a run-of-river hydro facility's renewal is activated in one 
            planning period, it must remain activated in all subsequent periods.

        Details:
            - This function imposes a non-decreasing constraint on the binary variable associated with the renewal of 
            run-of-river hydro facilities. If the binary variable is set to 1 (activated) in any given period, it cannot 
            be set back to 0 in any future period.
            - The constraint leverages a time series approach, where the binary status of the renewal in the current 
            period (`PD`) must be greater than or equal to its status in the previous period. This enforces a policy 
            or operational requirement that once renewal is committed, the decision cannot be reversed.
        """
        return (model.ror_renewal_binary[PD, HR_ROR]
                >= model.ror_renewal_binary[pds[pds.index(PD) - 1], HR_ROR])


    model.ror_onetime = Constraint(pds[1:], hr_ror, rule=ror_onetime)


    def day_onetime(model, PD, HR_DAY):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            HR_DAY: The identifier for the day-storage hydro facility.

        Returns:
            A Pyomo Constraint expression: Ensures that once the day-storage hydro facility's renewal is activated 
            (represented by a binary variable being set to 1), it cannot be deactivated (set back to 0) in any subsequent 
            planning period.

        Details:
            - This constraint is applied to the binary variable (`model.day_renewal_binary`) associated with the renewal 
            of day-storage hydro facilities.
            - The constraint ensures that the binary status in the current planning period (`PD`) must be greater than 
            or equal to its status in the previous period, effectively mandating a non-decreasing behavior. This ensures 
            that once a facility is marked for renewal (activated), the process cannot be reversed.
            - It is particularly important in energy models where commitments to renewals are irreversible due to 
            regulatory or policy requirements.
        """
        return (model.day_renewal_binary[PD, HR_DAY]
                >= model.day_renewal_binary[pds[pds.index(PD) - 1], HR_DAY])


    model.day_onetime = Constraint(pds[1:], hr_day, rule=day_onetime)


    def month_onetime(model, PD, HR_MO):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            HR_MO: The identifier for the month-storage hydro facility.

        Returns:
            A Pyomo Constraint expression: Ensures that once the month-storage hydro facility's renewal is activated 
            (represented by a binary variable being set to 1), it cannot be deactivated (set back to 0) in any subsequent 
            planning period.

        Details:
            - This function imposes a non-decreasing constraint on the binary variable associated with the renewal of 
            month-storage hydro facilities.
            - The constraint verifies that the binary status in the current planning period (PD) must be greater than or 
            equal to its status in the previous period, enforcing a policy or operational requirement that once a renewal 
            decision is made, it must be maintained in future periods.
            - This is crucial for long-term energy planning and regulatory compliance where continuity in renewable 
            investments or upgrades is mandated.
        """
        return (model.month_renewal_binary[PD, HR_MO]
                >= model.month_renewal_binary[pds[pds.index(PD) - 1], HR_MO])


    model.month_onetime = Constraint(pds[1:], hr_mo, rule=month_onetime)


# up ramp limit
def ramp_up(model, PD, H, ABA, TP):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the ramp-up constraint is relevant.
        ABA: The balancing area as a string, indicating where the thermal plant is located.
        TP: The type of thermal plant as a string.

    Returns:
        A Pyomo Constraint expression: Limits the increase in output of a thermal power plant from one hour to the next. 
        The output at hour H + time_diff[H] cannot exceed the output at hour H plus the allowable ramp based on 
        the plant's ramp rate percentage and its capacity after accounting for retirements.

    Details:
        - This constraint ensures that the rate at which thermal power plants increase their generation does not exceed 
          their specified ramp rate capacity.
        - The ramp rate (`ramp_rate_percent[TP]`) is applied to the sum of the plant's capacity after accounting for 
          retirements and the initial extant capacity.
        - `time_diff[H]` represents the time difference to the next relevant hour, scaling the ramp rate accordingly 
          to maintain stability and reliability in power supply.
    """
    ind = pds.index(PD)
    return model.supply[PD, H + time_diff[H], ABA, TP] <= model.supply[PD, H, ABA, TP] + (quicksum(
        model.capacity_therm[PDD, ABA, TP] - model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind + 1]) +
                                                                                          extant_thermal[pds[
                                                                                                             0] + '.' + ABA + '.' + TP]) * \
        ramp_rate_percent[TP] * time_diff[H]


model.ramp_up = Constraint(pds, h2, aba, ['nuclear'], rule=ramp_up)


# down ramp limit
def ramp_down(model, PD, H, ABA, TP):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the ramp-down constraint is relevant.
        ABA: The balancing area as a string, indicating where the thermal plant is located.
        TP: The type of thermal plant as a string.

    Returns:
        A Pyomo Constraint expression: Ensures that the decrease in output of a thermal power plant from one hour to the next 
        does not exceed the allowable ramp, calculated as a percentage of the plant's capacity minus any retired capacity. 
        The output at hour H + time_diff[H] must not be less than the output at hour H minus this calculated ramp-down capacity.

    Details:
        - This function constrains the rate at which thermal power plants decrease their generation, adhering to a specified 
          ramp rate percentage.
        - The ramp rate (`ramp_rate_percent[TP]`) is applied to the sum of the plant's capacity after accounting for retirements 
          and the initial extant capacity.
        - `time_diff[H]` represents the time difference to the next relevant hour, adjusting the ramp rate accordingly to 
          ensure operational safety and stability in power grid management.
    """
    ind = pds.index(PD)
    return model.supply[PD, H + time_diff[H], ABA, TP] >= model.supply[PD, H, ABA, TP] - (quicksum(
        model.capacity_therm[PDD, ABA, TP] - model.retire_therm[PDD, ABA, TP] for PDD in pds[:ind + 1]) +
                                                                                          extant_thermal[pds[
                                                                                                             0] + '.' + ABA + '.' + TP]) * \
        ramp_rate_percent[TP] * time_diff[H]


model.ramp_down = Constraint(pds, h2, aba, ['nuclear'], rule=ramp_down)


# capacity limit for wind plants
def windofscaplimit(model, GL):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        GL: The geographical location identifier for the offshore wind facility.

    Returns:
        A Pyomo Constraint expression: Limits the total installed capacity of offshore wind facilities at a specific 
        geographical location (GL) to not exceed a predefined maximum capacity (`maxwindofs[GL]`).

    Details:
        - This function aggregates the capacity of offshore wind facilities, both existing and reconstructed, across all 
          planning periods to ensure it does not surpass the maximum allowable capacity defined for that location.
        - The constraint supports planning and regulatory compliance by maintaining capacity within sustainable and approved limits.
    """
    return quicksum(model.capacity_wind_ofs[PD, GL] + model.capacity_wind_ofs_recon[PD, GL] for PD in pds) <= \
        maxwindofs[GL]


model.windsofscaplimit = Constraint(gl, rule=windofscaplimit)

def windonscaplimit(model, GL):
    """same as windofscaplimit but for wind ons plants"""
    return quicksum(model.capacity_wind_ons[PD, GL] + model.capacity_wind_ons_recon[PD, GL] for PD in pds) <= \
        maxwindons[GL]


model.windsonscaplimit = Constraint(gl, rule=windonscaplimit)


##Setting the pre-2025 thermal capacity balance - Transitioning capacity must pick ccs, fuel blending, backup, or retire.
#TODO this section can be turned into a loop with a list of the thermal plants to constrain
def diesel_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        ABA: The balancing area as a string, indicating where the diesel plants are located.

    Returns:
        A Pyomo Constraint expression: Ensures that the total capacity reduction of various types of diesel plants 
        in the specified balancing area (ABA) during the planning period (PD) does not exceed the retired capacity 
        of diesel plants labeled as 'pre2025'.

    Details:
        - This function totals the capacity of three specific types of diesel plants: 'diesel_retire_pre2025', 
          'diesel_backup_pre2025', and 'diesel_bio_pre2025'.
        - The constraint limits this total to not exceed the capacity retired under the 'diesel_pre2025' category, 
          reflecting strategic reductions or phase-outs in response to policy changes or environmental targets.
        - It is used to monitor and control the decommissioning of older diesel facilities to align with broader 
          environmental and regulatory goals.
    """

    return quicksum(
        [model.capacity_therm[PD, ABA, "diesel_retire_pre2025"], model.capacity_therm[PD, ABA, "diesel_backup_pre2025"],
         model.capacity_therm[PD, ABA, "diesel_bio_pre2025"]]) <= model.retire_therm[PD, ABA, 'diesel_pre2025']


model.diesel_pre2025_capacity_loss_over_period_less = Constraint(pds, aba,
                                                                 rule=diesel_pre2025_capacity_loss_over_period_less)


def diesel_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum(
        [model.capacity_therm[PD, ABA, "diesel_retire_pre2025"], model.capacity_therm[PD, ABA, "diesel_backup_pre2025"],
         model.capacity_therm[PD, ABA, "diesel_bio_pre2025"]]) >= model.retire_therm[PD, ABA, 'diesel_pre2025']


model.diesel_pre2025_capacity_loss_over_period_more = Constraint(pds, aba,
                                                                 rule=diesel_pre2025_capacity_loss_over_period_more)


def gas_cc_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum(
        [model.capacity_therm[PD, ABA, "gasCC_ccs_pre2025"], model.capacity_therm[PD, ABA, "gasCC_rng_pre2025"],
         model.capacity_therm[PD, ABA, "gasCC_retire_pre2025"],
         model.capacity_therm[PD, ABA, "gasCC_backup_pre2025"]]) >= model.retire_therm[PD, ABA, 'gasCC_pre2025']


model.gas_cc_pre2025_capacity_loss_over_period_more = Constraint(pds, aba,
                                                                 rule=gas_cc_pre2025_capacity_loss_over_period_more)


def gas_cc_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum(
        [model.capacity_therm[PD, ABA, "gasCC_ccs_pre2025"], model.capacity_therm[PD, ABA, "gasCC_rng_pre2025"],
         model.capacity_therm[PD, ABA, "gasCC_retire_pre2025"],
         model.capacity_therm[PD, ABA, "gasCC_backup_pre2025"]]) <= model.retire_therm[PD, ABA, 'gasCC_pre2025']


model.gas_cc_pre2025_capacity_loss_over_period_less = Constraint(pds, aba,
                                                                 rule=gas_cc_pre2025_capacity_loss_over_period_less)


def coal_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum(
        [model.capacity_therm[PD, ABA, "coal_ccs_pre2025"], model.capacity_therm[PD, ABA, "coal_retire_pre2025"]]) >= \
        model.retire_therm[PD, ABA, 'coal_pre2025']


model.coal_pre2025_capacity_loss_over_period_more = Constraint(pds, aba,
                                                               rule=coal_pre2025_capacity_loss_over_period_more)


def coal_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum(
        [model.capacity_therm[PD, ABA, "coal_ccs_pre2025"], model.capacity_therm[PD, ABA, "coal_retire_pre2025"]]) <= \
        model.retire_therm[PD, ABA, 'coal_pre2025']


model.coal_pre2025_capacity_loss_over_period_less = Constraint(pds, aba,
                                                               rule=coal_pre2025_capacity_loss_over_period_less)


def gas_sc_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum(
        [model.capacity_therm[PD, ABA, "gasSC_ccs_pre2025"], model.capacity_therm[PD, ABA, "gasSC_rng_pre2025"],
         model.capacity_therm[PD, ABA, "gasSC_retire_pre2025"],
         model.capacity_therm[PD, ABA, "gasSC_backup_pre2025"]]) >= model.retire_therm[PD, ABA, 'gasSC_pre2025']


model.gas_sc_pre2025_capacity_loss_over_period_more = Constraint(pds, aba,
                                                                 rule=gas_sc_pre2025_capacity_loss_over_period_more)


def gas_sc_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum(
        [model.capacity_therm[PD, ABA, "gasSC_ccs_pre2025"], model.capacity_therm[PD, ABA, "gasSC_rng_pre2025"],
         model.capacity_therm[PD, ABA, "gasSC_retire_pre2025"],
         model.capacity_therm[PD, ABA, "gasSC_backup_pre2025"]]) <= model.retire_therm[PD, ABA, 'gasSC_pre2025']


model.gas_sc_pre2025_capacity_loss_over_period_less = Constraint(pds, aba,
                                                                 rule=gas_sc_pre2025_capacity_loss_over_period_less)


def gas_cg_restricted_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum([model.capacity_therm[PD, ABA, "gasCG_restricted_ccs_pre2025"],
                     model.capacity_therm[PD, ABA, "gasCG_restricted_rng_pre2025"],
                     model.capacity_therm[PD, ABA, "gasCG_restricted_retire_pre2025"],
                     model.capacity_therm[PD, ABA, "gasCG_restricted_backup_pre2025"]]) >= model.retire_therm[
        PD, ABA, 'gasCG_restricted_pre2025']


model.gas_cg_restricted_pre2025_capacity_loss_over_period_more = Constraint(pds, aba,
                                                                            rule=gas_cg_restricted_pre2025_capacity_loss_over_period_more)


def gas_cg_restricted_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum([model.capacity_therm[PD, ABA, "gasCG_restricted_ccs_pre2025"],
                     model.capacity_therm[PD, ABA, "gasCG_restricted_rng_pre2025"],
                     model.capacity_therm[PD, ABA, "gasCG_restricted_retire_pre2025"],
                     model.capacity_therm[PD, ABA, "gasCG_restricted_backup_pre2025"]]) <= model.retire_therm[
        PD, ABA, 'gasCG_restricted_pre2025']


model.gas_cg_restricted_pre2025_capacity_loss_over_period_less = Constraint(pds, aba,
                                                                            rule=gas_cg_restricted_pre2025_capacity_loss_over_period_less)


def gas_cg_free_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum([model.capacity_therm[PD, ABA, "gasCG_free_retire_pre2025"]]) >= model.retire_therm[
        PD, ABA, 'gasCG_free_pre2025']


model.gas_cg_free_pre2025_capacity_loss_over_period_more = Constraint(pds, aba,
                                                                      rule=gas_cg_free_pre2025_capacity_loss_over_period_more)


def gas_cg_free_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    """see diesel_pre2025_capacity_loss_over_period_less() for details"""
    return quicksum([model.capacity_therm[PD, ABA, "gasCG_free_retire_pre2025"]]) <= model.retire_therm[
        PD, ABA, 'gasCG_free_pre2025']


model.gas_cg_free_pre2025_capacity_loss_over_period_less = Constraint(pds, aba,
                                                                      rule=gas_cg_free_pre2025_capacity_loss_over_period_less)


# one for =< and another for >= for each capacity equality

###Constrain generation of all cogeneration - JGM

def cg_limit_supply(model, PD, H, ABA, CG):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the generation limit is relevant.
        ABA: The balancing area as a string, indicating where the cogeneration plant is located.
        CG: The identifier for the cogeneration plant type.

    Returns:
        A Pyomo Constraint expression: Limits the electricity generation from cogeneration plants at the specified
        location and time to not exceed the allowable output calculated from the total effective capacity of these
        plants multiplied by a predefined cogeneration contribution rate defined here as the proportion of a unit's
		total supply that is exported to the transmission system.

    Details:
        - This constraint calculates the maximum generation limit for cogeneration plants by considering both the
          existing capacity at the start of the period and any capacity changes due to new installations or retirements.
        - The total effective capacity is then multiplied by a cogeneration contribution rate (e.g., 0.37 as the default COPPER value),
          which represents the proportion of a unit's total supply that is exported to the transmission system.
        - The cogeneration rate is typically configured externally (such as in a configuration file like `config.toml`),
          allowing for flexibility in scenario analysis.
    """
    ind = pds.index(PD)
    return model.supply[PD, H, ABA, CG] <= (quicksum(
        model.capacity_therm[PDD, ABA, CG] - model.retire_therm[PDD, ABA, CG] for PDD in pds[:ind + 1]) +
                                            extant_thermal[pds[0] + '.' + ABA + '.' + CG]) * cogeneration_rate #The COPPER default value is 0.37 this cogeneration rate is set in config.toml


model.cg_limit_supply = Constraint(pds, h, aba, cg_tech, rule=cg_limit_supply)


##Constrain generation of retired generators to zero for first economic dispatch
#RTP is the retired thermal plants
def retire_no_gen(model, PD, H, ABA, RTP):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the generation constraint is relevant.
        ABA: The balancing area as a string, indicating where the retired power generator is located.
        RTP: The identifier for the retired power generator type.

    Returns:
        A Pyomo Constraint expression: Ensures that the electricity generation from the specified retired power 
        generators is zero, aligning with operational decommissioning policies or regulatory requirements.

    Details:
        - This function specifically targets generators from retired_generators in config.toml, prohibiting any generation 
          output from them during the specified hour and planning period.
        - It is critical in ensuring compliance with decommissioning or retirement policies, where such units should not 
          contribute to the power grid's electricity supply.
    """
    return model.supply[PD, H, ABA, RTP] <= 0


model.retire_no_gen = Constraint(pds, h, aba, retired_generators, rule=retire_no_gen)


##Constraints to restrict development of pre2025 fossil generators - JGM

def diesel_capacity_no_more_pre_2025(model, PD, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        ABA: The balancing area as a string, indicating where the diesel generators are located.

    Returns:
        A Pyomo Constraint expression: Enforces that the total new capacity attributed to pre-2025 diesel generators 
        in the specified balancing area must be zero across all planning periods.

    Details:
        - This function aims to ensure that diesel generators categorized as "pre2025" are no longer created, these are
		older unregulated units. All newly created units must be designated for regulation.
    """
    return quicksum(model.capacity_therm[PD, ABA, "diesel_pre2025"] for PD in pds) <= 0


model.diesel_capacity_no_more_pre_2025 = Constraint(pds, aba, rule=diesel_capacity_no_more_pre_2025)


def coal_capacity_no_more_pre_2025(model, PD, ABA):
    """see diesel_capacity_no_more_pre_2025() for details"""
    return quicksum(model.capacity_therm[PD, ABA, "coal_pre2025"] for PD in pds) <= 0


model.coal_capacity_no_more_pre_2025 = Constraint(pds, aba, rule=coal_capacity_no_more_pre_2025)


def gasSC_capacity_no_more_pre_2025(model, PD, ABA):
    """see diesel_capacity_no_more_pre_2025() for details"""
    return quicksum(model.capacity_therm[PD, ABA, "gasSC_pre2025"] for PD in pds) <= 0


model.gasSC_capacity_no_more_pre_2025 = Constraint(pds, aba, rule=gasSC_capacity_no_more_pre_2025)


def gasCC_capacity_no_more_pre_2025(model, PD, ABA):
    """see diesel_capacity_no_more_pre_2025() for details"""
    return quicksum(model.capacity_therm[PD, ABA, "gasCC_pre2025"] for PD in pds) <= 0


model.gasCC_capacity_no_more_pre_2025 = Constraint(pds, aba, rule=gasCC_capacity_no_more_pre_2025)


def gasCG_free_capacity_no_more_pre_2025(model, PD, ABA):
    """see diesel_capacity_no_more_pre_2025() for details"""
    return quicksum(model.capacity_therm[PD, ABA, "gasCG_free_pre2025"] for PD in pds) <= 0


model.gasCG_free_capacity_no_more_pre_2025 = Constraint(pds, aba, rule=gasCG_free_capacity_no_more_pre_2025)


def gasCG_restricted_capacity_no_more_pre_2025(model, PD, ABA):
    """see diesel_capacity_no_more_pre_2025() for details"""
    return quicksum(model.capacity_therm[PD, ABA, "gasCG_restricted_pre2025"] for PD in pds) <= 0


model.gasCG_restricted_capacity_no_more_pre_2025 = Constraint(pds, aba, rule=gasCG_restricted_capacity_no_more_pre_2025)


##The constraint below is intended to eliminate backup generation capacity, for the business as usual case
def no_backup_generators(model, PD, ABA, BG):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        ABA: The balancing area as a string, indicating where the backup generators are located.
        BG: The identifier for the type of backup generator.

    Returns:
        A Pyomo Constraint expression: Ensures that the total capacity for backup generators in the specified 
        balancing area remains at zero across all planning periods.

    Details:
        - Only used if flag_cer is set to False in the configuration file. 
        - BG comes from the backup_generators set in config.toml, representing the different types of backup generators
        - Backup generation is not a defined activity except under the CER regulation, therefore they are not allowed for BAU scenario
        - By summing the capacities of these generators across all planning periods and requiring this sum to 
          not exceed zero, the model effectively eliminates the ability to develop backup generators.
    """
    return quicksum(model.capacity_therm[PD, ABA, BG] for PD in pds) <= 0


if flag_cer == False:
    model.no_backup_generators = Constraint(pds, aba, backup_generators, rule=no_backup_generators)


def gasCC_free_capacity_no_post_2025_CER(model, PD, ABA):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        ABA: The balancing area as a string, indicating where the gas combined cycle generators are located.

    Returns:
        A Pyomo Constraint expression: Ensures that the total capacity allocated to post-2025 free-emission 
        gas combined cycle (CC) generators in the specified balancing area remains at zero across all planning periods.

    Details:
        - This function targets new types of gas combined cycle generators that might be considered under future 
          technological or regulatory scenarios, specifically those intended to be free of carbon regulations post-2025.
        - The constraint sums the capacities of these generators across all planning periods and requires this sum 
          to not exceed zero, aligning with stringent carbon emission reduction goals or policies.
        - Enforcing this constraint helps model scenarios where strict emissions standards are anticipated, and 
          planning needs to account for significant shifts in energy production technology and policy.
    """
    return quicksum(model.capacity_therm[PD, ABA, "gasCC_free_post2025"] for PD in pds) <= 0


if flag_cer:
    model.gasCC_free_capacity_no_post_2025_CER = Constraint(pds, aba, rule=gasCC_free_capacity_no_post_2025_CER)


# capacity limit for solar plants
def solarcaplimit(model, GL):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        GL: The geographical location identifier for the solar power facility.

    Returns:
        A Pyomo Constraint expression: Limits the total installed solar power capacity at the specified 
        geographical location (GL) to not exceed a predefined maximum capacity (`maxsolar[GL]`).

    Details:
        - This function aggregates the capacity of solar power facilities, both existing and reconstructed, across 
          all planning periods, ensuring that the total does not surpass the maximum allowable capacity defined for 
          that location.
        - The constraint is important for managing the growth of solar power installations within sustainable limits 
          and aligning with regional energy production policies or grid capacity constraints.
    """
    return quicksum(model.capacity_solar[PD, GL] + model.capacity_solar_recon[PD, GL] for PD in pds) <= maxsolar[GL]


model.solarcaplimit = Constraint(gl, rule=solarcaplimit)

###################carbon limit constraint can be on or off##################
if provincial_emission_limit:
    def provincialcarbonlimit(model, AP):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            AP: The provincial identifier where the emission limits are to be enforced.

        Returns:
            A Pyomo Constraint expression: Ensures that the total carbon dioxide emissions from power plants 
            within the specified province (AP) do not exceed the regulatory emission limit for that province.

        Details:
            - This function calculates the total emissions by aggregating the carbon dioxide output from all thermal power 
            plants in the province, factoring in the operational hours, the carbon emission factors of each plant type 
            (`carbondioxide[TP]`), and the corresponding supply amount at the last planning period within each hour.
            - The emissions are normalized to a daily basis considering the number of run days in a year, allowing the model 
            to conform to annual provincial emission limits (`carbon_limit[AP]`).
            - This constraint is crucial for adhering to provincial or national environmental policies aimed at reducing 
            greenhouse gas emissions from the power sector.
        """
        return quicksum(
            model.supply[pds[-1], H, ABA, TP] * carbondioxide[TP] / 1000000 for H in h for TP in tplants for ABA in aba
            if AP in ABA) <= carbon_limit[AP] / (365 / len(rundays))


    model.provincialcarbonlimit = Constraint(ap, rule=provincialcarbonlimit)

if national_emission_limit:
    def nationalcarbonlimit(model, PD):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the national emission limit is applied.

        Returns:
            A Pyomo Constraint expression: Restricts the total carbon dioxide emissions from all thermal power plants 
            across the nation during the specified planning period (PD) to not exceed the national emission limit for that period.

        Details:
            - This function calculates the total emissions by aggregating the carbon dioxide output from all thermal power 
            plants across different balancing areas, taking into account the carbon emission factors of each plant type 
            (`carbondioxide[TP]`), and the amount supplied during the specific planning period.
            - The emissions are normalized to a daily basis considering the number of run days in a year, ensuring compliance 
            with the national emission limits (`nat_em_limit[PD]`), which are predefined for each planning period.
            - The constraint is essential for meeting national environmental objectives and adhering to international 
            climate agreements targeting reduced greenhouse gas emissions in the power sector.
        """
        return quicksum(
            model.supply[PD, H, ABA, TP] * carbondioxide[TP] / 1000000 for H in h for TP in tplants for ABA in aba) <= \
            nat_em_limit[PD] / (365 / len(rundays))


    model.nationalcarbonlimit = Constraint(pds, rule=nationalcarbonlimit)

if min_installed_LB_PHP:
    def installedgasPHP(model, ABA):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            ABA: The balancing area as a string, indicating where the minimum capacity requirements for pumped hydro storage are enforced.

        Returns:
            A Pyomo Constraint expression: Ensures that the total installed capacity of pumped hydro storage in the specified 
            balancing area (ABA) meets or exceeds a predefined lower bound (`LB_PHP_installed_limit[ABA]`).

        Details:
            - This function aggregates the capacity of all pumped hydro storage facilities within the balancing area, both from 
            the model's variable capacities (`model.capacity_storage`) across all planning periods and the baseline installed 
            capacity (`ba_storage_capacity[ABA + '.storage_PH']`).
            - The aggregated total is then compared against the minimum installed capacity limit set for the area, 
            ensuring that the region maintains sufficient pumped hydro storage capacity to meet regulatory or operational 
            standards.
            - This constraint is crucial for maintaining energy storage capacity at levels that support grid stability and 
            compliance with regional energy policies.
        """
        return quicksum(model.capacity_storage[PD, ST, ABA] for PD in pds for ST in st) + ba_storage_capacity[
            ABA + '.storage_PH'] >= LB_PHP_installed_limit[ABA]


    model.installedgasPHP = Constraint(aba, rule=installedgasPHP)

if new_thermal_limit:
    def newthermallimit(model, PD, LTP):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            LTP: The type of new thermal plant (e.g., 'coal', 'gas') for which the installation is to be limited.

        Returns:
            A Pyomo Constraint expression: Ensures that the total capacity of new thermal plants of type LTP 
            installed during the specified planning period (PD) across all balancing areas does not exceed zero, 
            effectively prohibiting the addition of new capacity for this plant type.

        Details:
            - This function sums the capacities of all new thermal power plants of a specific type (LTP) across different 
            balancing areas during a given planning period, and enforces a constraint that this total capacity must be zero.
            - The constraint is significant for scenarios where environmental policies or strategic energy transitions 
            require a halt or reduction in the development of certain types of thermal power plants to meet carbon 
            reduction goals or other environmental targets.
        """
        return quicksum(model.capacity_therm[PD, ABA, LTP] for ABA in aba) <= 0


    model.newthermallimit = Constraint(pds, limited_tplants, rule=newthermallimit)

if thermal_phase_out:

    def phaseout_rule(model, PHOT, ABA, PDD):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PHOT: The identifier for the type of thermal plant being phased out (e.g., 'coal', 'oil').
            ABA: The balancing area as a string, indicating where the phase-out is taking place.

        Returns:
            A Pyomo Constraint expression: Ensures that the combined existing and net installed capacity of thermal 
            power plants of type PHOT in the specified balancing area (ABA) does not exceed zero by the end of the 
            designated phase-out period.

        Details:
            - The function calculates the total net capacity of the specified type of thermal plant up to the year 
            designated for phase-out, which is determined based on the planning period index mapped in `phase_out_type_year[PHOT]`.
            - This includes the initial extant capacity and any changes due to capacity additions or retirements up to 
            and including the phase-out year.
            - The constraint is crucial for aligning with environmental policies or agreements that mandate the reduction 
            or elimination of certain types of fossil-fuel-based generation to mitigate climate change or reduce 
            environmental impacts.
        """
        try:
            existing_cap = extant_thermal[phase_out_type_year[PDD] + '.' + ABA + '.' + PHOT]
        except KeyError:
            existing_cap = 0

        return (existing_cap + model.capacity_therm[PDD, ABA, PHOT]) <= model.retire_therm[PDD, ABA, PHOT]

    def aggregate_phaseout_rule(model, PHOT, ABA):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PHOT: The identifier for the type of thermal plant being phased out (e.g., 'coal', 'oil').
            ABA: The balancing area as a string, indicating where the phase-out is taking place.

        Returns:
            A Pyomo Constraint expression: Ensures that the total existing and installed capacity of thermal power plants
            of type PHOT in all balancing areas over the relevant planning periods are <= the retirement value in the phaseout 
            year set in config.toml 
            phase-out year.
        """
        ind = pds.index(phase_out_type_year[PHOT])
        peak_existing_capacity = 0
        #Finds the peak existing capacity of a given generation type
        for PDD in pds[0:ind+1]:
            try:
                if extant_thermal[PDD + '.' + ABA + '.' + PHOT] > peak_existing_capacity:
                    peak_existing_capacity = extant_thermal[PDD + '.' + ABA + '.' + PHOT]
            except KeyError:
                pass
        #forces the sum of retirement values to be equal to existing and built capacity by the retirment year
        return peak_existing_capacity + quicksum(model.capacity_therm[PDD, ABA, PHOT] for PDD in pds[0:ind+1]) <= quicksum(model.retire_therm[PDD, ABA, PHOT] for PDD in pds[0:ind+1])

    def phaseout_constraint_generator(model):
        """
        Generator function to create phaseout constraints for the model.
        """
        constraints = []
        for PHOT in ph_out_t:
            ind = pds.index(phase_out_type_year[PHOT])
            for ABA in aba:
                for PDD in pds[ind:]:
                    constraints.append(((PHOT, ABA, PDD), phaseout_rule(model, PHOT, ABA, PDD)))
                # Add aggregate constraint for the total capacity over the phaseout period
                constraints.append(((PHOT, ABA, 'years_leading_up'), aggregate_phaseout_rule(model, PHOT, ABA)))
        return constraints

    # Create constraints dynamically
    for (PHOT, ABA, PDD), constraint_expr in phaseout_constraint_generator(model):
        setattr(model, f'phaseout_{PHOT}_{ABA}_{PDD}', Constraint(expr=constraint_expr))
###Maximum biomass capacity (By Balancing Area)

if max_bio_aba:
    def maxbioaba(model, PD, ABA):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            ABA: The balancing area as a string, indicating where the biomass power plants are located.

        Returns:
            A Pyomo Constraint expression: Ensures that the total net capacity of biomass power plants in the 
            specified balancing area (ABA) does not exceed a predefined maximum limit (`max_bio[ABA]`).

        Details:
            - The function calculates the total net capacity of biomass power plants in the balancing area, taking into account 
            the initial existing capacity and any subsequent capacity changes due to new installations or retirements.
            - The cumulative capacity from the beginning of the period up to and including the current planning period is considered, 
            ensuring that the capacity does not surpass the maximum allowed as specified in `max_bio[ABA]`.
            - This constraint is important for managing the scale of biomass energy production in compliance with regional energy 
            policies or sustainability goals, which may limit the expansion of biomass facilities to control emissions or due 
            to resource availability.
        """
        ind = pds.index(PD)
        return extant_thermal[pds[0] + '.' + ABA + '.' + 'biomass'] + quicksum(
            model.capacity_therm[PDD, ABA, 'biomass'] - model.retire_therm[PDD, ABA, 'biomass'] for PDD in
            pds[:ind + 1]) <= max_bio[ABA]


    model.maxbioaba = Constraint(pds, aba, rule=maxbioaba)

###Maximum nuclear capacity (By Balancing Area)

if max_nuclear_aba:
    def maxnuclearaba(model, PD, ABA):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            ABA: The balancing area as a string, indicating where the nuclear power plants are located.

        Returns:
            A Pyomo Constraint expression: Limits the total net capacity of nuclear power plants in the specified 
            balancing area (ABA) to not exceed a predefined maximum limit (`max_nuclear[ABA]`).

        Details:
            - This function calculates the total net capacity of nuclear power plants in the balancing area, accounting 
            for the initial existing capacity and any subsequent capacity changes due to new installations or retirements.
            - The cumulative capacity from the beginning of the period up to and including the current planning period is 
            assessed to ensure that it remains within the maximum capacity limits set by `max_nuclear[ABA]`.
            - Enforcing this constraint is crucial for managing nuclear power generation within safety and regulatory limits,
            which are often strictly controlled due to the sensitive nature of nuclear energy production.
        """
        ind = pds.index(PD)
        return extant_thermal[pds[0] + '.' + ABA + '.' + 'nuclear'] + quicksum(
            model.capacity_therm[PDD, ABA, 'nuclear'] - model.retire_therm[PDD, ABA, 'nuclear'] for PDD in
            pds[:ind + 1]) <= max_nuclear[ABA]


    model.maxnuclearaba = Constraint(pds, aba, rule=maxnuclearaba)

###Maximum SMR capacity (By Balancing Area)

if max_smr_aba:
    def maxsmraba(model, PD, ABA):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
            ABA: The balancing area as a string, indicating where the SMR nuclear power plants are located.

        Returns:
            A Pyomo Constraint expression: Ensures that the total net capacity of SMR nuclear power plants in the specified 
            balancing area (ABA) does not exceed a predefined maximum limit (`max_smr[ABA]`).

        Details:
            - This function calculates the total net capacity of SMR nuclear plants in the balancing area, considering 
            the initial existing capacity and any subsequent capacity changes due to new installations or retirements.
            - The cumulative capacity from the beginning of the period up to and including the current planning period is 
            assessed to ensure that it stays within the maximum capacity limits set by `max_smr[ABA]`.
            - The constraint is vital for managing the development and deployment of SMR technology within regulatory 
            and safety limits, aligning with energy policies that promote the use of nuclear energy while ensuring 
            it is developed within sustainable and safe parameters.
        """
        ind = pds.index(PD)
        return extant_thermal[pds[0] + '.' + ABA + '.' + 'nuclear_SMR'] + quicksum(
            model.capacity_therm[PDD, ABA, 'nuclear_SMR'] - model.retire_therm[PDD, ABA, 'nuclear_SMR'] for PDD in
            pds[:ind + 1]) <= max_smr[ABA]


    model.maxsmraba = Constraint(pds, aba, rule=maxsmraba)


###Limiting CCS to Prarie Provinces (AB and SK)

def no_CCS_in_specified_aba(model, NOCCS, CCST):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        NOCCS: The balancing area as a string, specifying where the CCS capacity constraint is to be enforced.
        CCST: The CCS technology type as a string, for which the installation is to be limited.

    Returns:
        A Pyomo Constraint expression: Ensures that the total capacity of a specific CCS technology type within 
        the specified balancing area remains at zero across all planning periods.

    Details:
        - This function sums the capacities of a specific CCS technology type across all planning periods within 
          a designated balancing area, and enforces that this total capacity must not exceed zero.
        - This zero-capacity constraint reflects specific policy decisions or environmental strategies aimed at 
          either phasing out particular CCS technologies or preventing their adoption within certain regions.
        - The enforcement of this constraint is crucial for meeting regional or national environmental targets 
          or adhering to local regulations that restrict the use of certain CCS technologies.
    """
    return quicksum(model.capacity_therm[PDD, NOCCS, CCST] for PDD in pds) <= 0


model.no_CCS_in_specified_aba = Constraint(no_CCS_aba, CCS_tech, rule=no_CCS_in_specified_aba)

## No new CCS after 2035 for AB and SK
#TODO duplicate of this function below, likely due to merge issue
if flag_noNewCCS_2035:
    def noNewCCSafter2035(model, ABACCS, CCST, PDD):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            ABACCS: The balancing area as a string, specifying where the new CCS capacity constraint is to be enforced.
            CCST: The CCS technology type as a string, for which the new installation is to be prohibited.
            PDD: The specific planning period after 2035 (e.g., '2036', '2040', etc.) during which the constraint is applied.

        Returns:
            A Pyomo Constraint expression: Ensures that no new capacity for the specified CCS technology is installed 
            in the designated balancing area during the specified planning period after 2035.

        Details:
            - This function enforces a policy that no new installations of specified CCS technologies are allowed in 
            certain regions after the year 2035, reflecting a strategic move to either phase out these technologies 
            or prevent their expansion due to environmental, economic, or policy considerations.
            - By setting the capacity to zero for the specified technology and planning periods, the model effectively 
            restricts the development of new CCS facilities in response to evolving climate policies or emissions targets.
        """
        return model.capacity_therm[PDD, ABACCS, CCST] <= 0


    model.noNewCCSafter2035 = Constraint([a for a in aba if a in ['Alberta.a', 'Saskatchewan.a', 'Manitoba.a']], CCS_tech,
                                         [y for y in pds if y in['2035', '2040', '2045', '2050']], rule=noNewCCSafter2035)


###Parasitic load for CCS thermal plants
def parloadccs(model, PD, H, ABA, CCST):
    """
    Parameters:
        model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
        PD: The planning period as a string (e.g., '2025') during which the constraint is applied.
        H: The hour within the planning period for which the partial load constraint is relevant.
        ABA: The balancing area as a string, indicating where the CCS-equipped thermal plant is located.
        CCST: The CCS technology type equipped on the thermal plant.

    Returns:
        A Pyomo Constraint expression: Restricts the hourly supply of CCS-equipped thermal power plants to no more 
        than 80% of their calculated available capacity, considering both installed and retired capacities up to 
        the current planning period.

    Details:
        - This function assesses the maximum permissible supply from CCS-equipped thermal plants by calculating 
          their total effective capacity—considering both extant capacity at the start of the planning periods and 
          adjustments due to capacity installations or retirements.
        - The supply limitation to 80% of available capacity is to represent the 20% parasitic load to support CCS.

    """
    ind = pds.index(PD)
    return model.supply[PD, H, ABA, CCST] <= (quicksum(
        model.capacity_therm[PDD, ABA, CCST] - model.retire_therm[PDD, ABA, CCST] for PDD in pds[:ind + 1]) +
                                              extant_thermal[pds[0] + '.' + ABA + '.' + CCST]) * (1-parasitic_load)


model.parloadccs = Constraint(pds, h, aba, CCS_tech, rule=parloadccs)
if non_emitting_limit:
    def nonemittinglimit(model, PD):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the non-emitting generation limit is applied.

        Returns:
            A Pyomo Constraint expression: Ensures that the sum of electricity generated from non-emitting sources 
            (including wind, solar, hydro, and other non-emitting thermal plants) within the specified planning period 
            (PD) constitutes at least a certain percentage (defined by `nonemitting_limit[PD]`) of the total electricity 
            generation from all sources.

        Details:
            - This function calculates the total generation from non-emitting sources, which includes output from wind, 
            solar, run-of-river hydro, day and month storage hydro, and non-emitting thermal plants, and compares this 
            to the overall generation, which includes these sources plus all other thermal power plant outputs.
            - The required minimum percentage of total generation from non-emitting sources is specified for each planning 
            period in `nonemitting_limit[PD]`.
            - Enforcing this constraint is crucial for regions aiming to meet sustainability targets or reduce greenhouse 
            gas emissions through increased reliance on renewable and non-emitting energy sources.
        """
        return quicksum(
            model.supply[PD, H, ABA, NEP] for H in h for ABA in aba for NEP in non_emitting_tplants) + quicksum(
            model.windonsout[PD, H, ABA] + model.windofsout[PD, H, ABA] + model.solarout[PD, H, ABA] + ror_hydroout[
                PD + '.' + str(H) + '.' + ABA] + model.daystoragehydroout[PD, H, ABA] + model.monthstoragehydroout[
                PD, H, ABA] for H in h for ABA in aba) >= \
            nonemitting_limit[PD] * (
                        quicksum(model.supply[PD, H, ABA, TP] for H in h for ABA in aba for TP in tplants) + quicksum(
                    model.windonsout[PD, H, ABA] + model.windofsout[PD, H, ABA] + model.solarout[PD, H, ABA] +
                    ror_hydroout[PD + '.' + str(H) + '.' + ABA] + model.daystoragehydroout[PD, H, ABA] +
                    model.monthstoragehydroout[PD, H, ABA] for H in h for ABA in aba))


    model.nonemittinglimit = Constraint(pds, rule=nonemittinglimit)

if limited_new_thermal_gen:
    def limited_new_thermal(model, GT):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            GT: The identifier for the type of new thermal generation technology (e.g., 'gas', 'coal').

        Returns:
            A Pyomo Constraint expression: Ensures that the total installed capacity of the specified new thermal generation 
            technology across all balancing areas does not exceed a predefined limit (`limited_new_thermal_gen[GT]`).

        Details:
            - This function aggregates the capacity of the specified new thermal generation technology across all planning 
            periods and balancing areas, enforcing that this total capacity remains within the set limits.
            - The limit (`limited_new_thermal_gen[GT]`) is a policy-driven or regulatory cap designed to control the expansion 
            of certain types of thermal power plants, often to meet environmental standards or transition towards less 
            carbon-intensive energy systems. It is set in config.toml value ['Thermal']['limited_new_thermal_gen']
            - Applying this constraint is critical for managing the scale and environmental impact of new thermal power 
            developments, ensuring that they align with broader energy policy goals or compliance with climate action plans.
        """
        return quicksum(model.capacity_therm[PD, ABA, GT] for PD in pds for ABA in aba) <= limited_new_thermal_gen[GT]


    model.limited_new_thermal = Constraint(list(limited_new_thermal_gen.keys()), rule=limited_new_thermal)

if renewable_portfolio_standards['wind_ons.max']:
    def rpf_wind_ons_max(model, PD, AP):
        """
        Parameters:
            model: A Pyomo model instance containing all relevant decision variables, parameters, and sets.
            PD: The planning period as a string (e.g., '2025') during which the capacity limit is applied.
            AP: The administrative province as a string, specifying where the wind capacity limit is to be enforced.

        Returns:
            A Pyomo Constraint expression: Ensures that the combined installed and extant capacity of onshore wind 
            generation in the specified administrative province (AP) does not exceed the maximum capacity limit 
            defined by `wind_ons_max_ap_limit[PD][AP]`.

        Details:
            - This function sums the capacity of both newly installed and existing onshore wind power facilities up to 
            the given planning period (PD) and ensures that the total does not exceed the limit specified in the 
            `wind_ons_max_ap_limit` for that province and period. The limit is set in renewable_portfolio_max.csv
            - The enforcement of this constraint is contingent on the presence of a defined maximum capacity limit for 
            onshore wind in the renewable portfolio standards, specifically under `renewable_portfolio_standards['wind_ons.max']`.
            - This constraint is critical for regions aiming to balance the development of wind energy with other 
            forms of energy production, ensuring that wind energy development aligns with broader energy policy 
            objectives and does not exceed planned capacity for environmental, economic, or grid reliability reasons.
        """
        ind = pds.index(PD)
        return quicksum(
            (model.capacity_wind_ons[PDD, GL] + model.capacity_wind_ons_recon[PDD, GL]) for PDD in pds[:ind + 1] for GL
            in gl if AP == map_gl_to_pr[int(GL)]) + quicksum(
            extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'wind_ons'] for GL in gl if
            str(GL) + '.' + 'wind_ons' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]) <= \
            wind_ons_max_ap_limit[PD][AP]


    model.rpf_wind_ons_max = Constraint(pds, ap, rule=rpf_wind_ons_max)

if renewable_portfolio_standards['wind_ofs.max']:
    def rpf_wind_ofs_max(model, PD, AP):
        """see rpf_wind_ons_max() for details, same implementation but for wind offshore generation"""
        ind = pds.index(PD)
        return quicksum(
            (model.capacity_wind_ofs[PDD, GL] + model.capacity_wind_ofs_recon[PDD, GL]) for PDD in pds[:ind + 1] for GL
            in gl if AP == map_gl_to_pr[int(GL)]) + quicksum(
            extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'wind_ofs'] for GL in gl if
            str(GL) + '.' + 'wind_ofs' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]) <= \
            wind_ofs_max_ap_limit[PD][AP]


    model.rpf_wind_ofs_max = Constraint(pds, ap, rule=rpf_wind_ofs_max)

if renewable_portfolio_standards['wind_ons.min']:
    def rpf_wind_ons_min(model, PD, AP):
        """
        Constraint function for renewable portfolio standards on minimum wind (onsite) capacity.

        Parameters:
        model (pyomo.environ.ConcreteModel): Pyomo model instance.
        PD (str): Period identifier.
        AP (str): Area or region identifier.

        Returns:
        pyomo.environ.Constraint: A Pyomo constraint representing the requirement that the total
        capacity of onsite wind energy sources satisfies the minimum wind capacity standards
        for the given period and area.
        
        Details:
        The function calculates the total onsite wind capacity from existing and proposed installations
        and ensures it meets or exceeds the minimum capacity standards specified in the 'wind_ons_min_ap_limit'
        dictionary for the given period and area. These minimum values are defined in renewable_portfolio_min.csv
        """
        ind = pds.index(PD)
        return quicksum(
            (model.capacity_wind_ons[PDD, GL] + model.capacity_wind_ons_recon[PDD, GL]) for PDD in pds[:ind + 1] for GL
            in gl if AP == map_gl_to_pr[int(GL)]) + quicksum(
            extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'wind_ons'] for GL in gl if
            str(GL) + '.' + 'wind_ons' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]) >= \
            wind_ons_min_ap_limit[PD][AP]


    model.rpf_wind_ons_min = Constraint(pds, ap, rule=rpf_wind_ons_min)

if renewable_portfolio_standards['wind_ofs.min']:
    def rpf_wind_ofs_min(model, PD, AP):
        """see rpf_wind_ons_min() for details, same implementation but for wind offshore generation except input values are read from These minimum values are defined in renewable_portfolio_max.csv"""
        ind = pds.index(PD)
        return quicksum(
            (model.capacity_wind_ofs[PDD, GL] + model.capacity_wind_ofs_recon[PDD, GL]) for PDD in pds[:ind + 1] for GL
            in gl if AP == map_gl_to_pr[int(GL)]) + quicksum(
            extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'wind_ofs'] for GL in gl if
            str(GL) + '.' + 'wind_ofs' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]) >= \
            wind_ofs_min_ap_limit[PD][AP]


    model.rpf_wind_ofs_min = Constraint(pds, ap, rule=rpf_wind_ofs_min)

if renewable_portfolio_standards['solar.max']:
    def rpf_solar_max(model, PD, AP):
        """
        Constraint function for renewable portfolio standards on maximum solar capacity.

        Parameters:
        model (pyomo.environ.ConcreteModel): Pyomo model instance.
        PD (str): Period identifier.
        AP (str): Area or region identifier.

        Returns:
        pyomo.environ.Constraint: A Pyomo constraint representing the requirement that the total
        capacity of solar energy sources does not exceed the maximum solar capacity standards
        for the given period and area.

        The function calculates the total solar capacity from existing and proposed installations
        and ensures it does not exceed the maximum capacity standards specified in the 'solar_max_ap_limit'
        dictionary for the given period and area. These values are read from the renewable_portfolio_max.csv input sheet
        """
        ind = pds.index(PD)
        return quicksum(
            (model.capacity_solar[PDD, GL] + model.capacity_solar_recon[PDD, GL]) for PDD in pds[:ind + 1] for GL in gl
            if AP == map_gl_to_pr[int(GL)]) + quicksum(
            extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'solar'] for GL in gl if
            str(GL) + '.' + 'solar' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]) <= solar_max_ap_limit[PD][
            AP]


    model.rpf_solar_max = Constraint(pds, ap, rule=rpf_solar_max)

if renewable_portfolio_standards['solar.min']:
    def rpf_solar_min(model, PD, AP):
        """
        Constraint function for renewable portfolio standards on minimum solar capacity.

        Parameters:
        model (pyomo.environ.ConcreteModel): Pyomo model instance.
        PD (str): Period identifier.
        AP (str): Area or region identifier.

        Returns:
        pyomo.environ.Constraint: A Pyomo constraint representing the requirement that the total
        capacity of solar energy sources satisfies the minimum solar capacity standards
        for the given period and area.

        The function calculates the total solar capacity from existing and proposed installations
        and ensures it meets or exceeds the minimum capacity standards specified in the 'solar_min_ap_limit'
        dictionary for the given period and area. These minimum values are defined in renewable_portfolio_min.csv
        """
        ind = pds.index(PD)
        return quicksum(
            (model.capacity_solar[PDD, GL] + model.capacity_solar_recon[PDD, GL]) for PDD in pds[:ind + 1] for GL in gl
            if AP == map_gl_to_pr[int(GL)]) + quicksum(
            extant_wind_solar[pds.index(PD)][str(GL) + '.' + 'solar'] for GL in gl if
            str(GL) + '.' + 'solar' in extant_wind_solar[0] and AP == map_gl_to_pr[int(GL)]) >= solar_min_ap_limit[PD][
            AP]


    model.rpf_solar_min = Constraint(pds, ap, rule=rpf_solar_min)

end = time.time()

print(f'\n==================================================\n\
Creating model time: {round((end - start) / 60)} Min and {round((end - start) % 60)} Sec \
\n==================================================')

start = time.time()
# solving the Optimization Problem
model.dual = Suffix(direction=Suffix.IMPORT_EXPORT)

opt = SolverFactory(solver)

# Check solver to pass options
match solver:    
    case 'cplex':
        if seed != None:
            opt.options['randomseed'] = config['Solver']['cplex']['randomseed']#seed
        opt.options['mip tolerances absmipgap'] = config['Solver']['cplex']['absmipgap']#0.05
    case 'cbc':
        opt.options['randomS'] = config['Solver']['cbc']['randomS'] #seed
        opt.options['ratio'] = config['Solver']['cbc']['ratio'] #0.05 # MIP gap tolerance [0.0, 1.0]
        opt.options['maxSo'] = config['Solver']['cbc']['maxSo'] #1 # Maximum number of solutions. Halts solver execution after n solutions are found    
    case 'glpk':
        if seed != None:
            opt.options['seed'] = config['Solver']['glpk']['seed'] #seed
        opt.options['mipgap'] = config['Solver']['glpk']['mipgap'] #0.05
    case _:
        pass
    
result_obj = opt.solve(model,tee=False)
result_obj.write()

# Model lp
if config['Simulation_Settings']['save_lp']:
    model.write('model.lp', io_options={'symbolic_solver_labels': True})

end = time.time()

print(f'\n==================================================\n\
Solving process time: {round((end - start) / 60)} Min and {round((end - start) % 60)} Sec\
\n==================================================')

start_postprocessing = time.time()

############Saving results in .csv files################
folder_name = os.path.join(os.getcwd(), '..', '..', destination_folder, scenario)
folder_name += '_' + datetime.now().strftime(r"%Y%m%d_%H%M")

if test:
    folder_name += '_Test'

if not os.path.exists(folder_name):
    os.makedirs(folder_name, exist_ok=True)
os.chdir(folder_name)

export_duals(model, folder_name, config["Economics"]["duals_of_interest"])

# Saving results in .csv files
save_model_vars(model, folder_name)
variable_frame(model)

# Saving other parameters
gen_tech_data.to_csv('generation_type_data.csv')
hydro_new.to_csv('hydro_new.csv')
gridcell_data.to_csv('gridcells.csv')
extant_wind_solar_raw.to_csv('extant_wind_solar.csv')
extant_capacity_raw.to_csv('extant_capacity.csv')
transmission_data.to_csv('transmission_data.csv')
tech_evolution.to_csv('technology_evolution.csv')
capacity_value.to_csv('capacity_value.csv')
demand_piv.to_csv('demand.csv')
demand_growth.to_csv('annual_growth.csv')
hydrocf.to_csv('hydro_cf.csv')


pd.DataFrame.from_dict(ror_hydroout, orient='index').to_csv('ror_hydroout.csv')
if hydro_development:
    pd.DataFrame.from_dict(ror_renewalout, orient='index').to_csv('rorrenewalout.csv')
annual_avg_prices = approximate_electricity_prices(folder_name, config,
                                                   scenario,
                                                   gasprice,
                                                   ctax,
                                                   carbondioxide,
                                                   carbondioxide_obps_2025,
                                                   carbondioxide_obps_2030, )

annual_avg_prices.to_csv(f"{folder_name}/annual_avg_prices.csv")

pd.DataFrame([model.obj()], columns=["Objective_function_value"]).to_csv("obj_value.csv")

### Saving config data
with open('COPPER config.txt', 'w') as f:
    f.write(f'Planning for the target year {foryear} considering {refyear} as the reference year\n')
    f.write(f'modeled {pds} palnning periods and ran {len(rundays)} representative days in each period\n')
    f.write(f'Carbon price = {ctax}\n')
    f.write(f'pumped hydro retrofit limit = {pump_ret_limit}\n')
    f.write(f'test run ? {test}\n')
    f.write(f'hydro development on ? {hydro_development}\n')
    f.write(f'autarky on ? {autarky}\n')
    f.write(f'pump as continous variable ? {storage_continous}\n')
    f.write(
        f'provincial_emission_limit on?  {provincial_emission_limit}   {carbon_reduction} carbon reduction compared to refrence year {emission_limit_ref_year}\n')
    f.write(f'national_emission_limit on?  {national_emission_limit}  {nat_em_limit}\n')
    f.write(f'local gas price on? {local_gas_price}\n')
    f.write(f'OBPS on? {OBPS_on}\n')
    f.write(
        f'thermal phase out on? {thermal_phase_out} >>>>> if true these types will be phased out by the specified year {phase_out_type_year}\n')
    f.write(f'min installed gas PHP requirement on ? {min_installed_LB_PHP}\n')
    f.write(
        f'is developping new thermal banned ? {new_thermal_limit} >>>>> if true development of these types are banned {limited_tplants}\n')
    f.write(
        f'just small hydro on ? {just_small_hydro} >>>>> if true, the model just consider development of small hydro projects (under 100 MW)\n')
    f.write(f'is  technology evolution on? {technology_evolution_on}\n')
    f.write(f'is  GPS on? {GPS}\n')
    f.write(f'is  CPO on? {CPO}\n')
    f.write(
        f'is  tranmission expansion constrained? CTE extant = {CTE_extant} CTE coefficient =>> {CTE_coef}, CTE custom = {CTE_custom}\n')
    f.write(f'is  non_emitting limit on? {non_emitting_limit}  {nonemitting_limit}\n')
    f.write(f'limited new thermal generation expansion? {limited_new_thermal_gen} \n')
    f.write(f'The discount rate is {discount} and the inflation is {inflation} \n')

toml.dump(config, open("config.toml", "w"))

post_process = post_process_copper(run_days, scenario)
post_process.save_results_summary_excel()
post_process.output_summary()
if flag_disaggregate_generators:
    post_process.disaggregate_generators()

# Set the destination folder under results/LastRun
last_run_folder = os.path.join(folder_name, '..', 'LastRun')

# Create the LastRun folder if it doesn't exist
os.makedirs(last_run_folder, exist_ok=True)

# Copy results from folder_name to LastRun
shutil.rmtree(last_run_folder)  # Remove the entire folder and its contents
shutil.copytree(folder_name, last_run_folder)

### Creates a report when the model is infeasible
with open('solver_report.txt', 'w') as f:
    # model.load(result_obj) # Loading solution into results object
    if (result_obj.solver.status == SolverStatus.ok) and (
            result_obj.solver.termination_condition == TerminationCondition.optimal):
        f.write('Terminated optimal and normal\n\n')
    elif (result_obj.solver.termination_condition == TerminationCondition.infeasible):
        f.write('WARNING:    -------- INFEASIBLE :(  -------\n\n')
    else:
        # Something else is wrong
        f.write('Solver Status:' + str(result_obj.solver.status) + '\n\n')
    result_obj.write()

end_postprocessing = time.time()
print(f'\n==================================================\n\
postprocess time: {round((end_postprocessing - start_postprocessing) / 60)} Min and {round((end_postprocessing - start_postprocessing) % 60)} Sec\
\n==================================================')
