import os

import numpy as np
import openpyxl
import pandas as pd

base_folder = './AllRegionsTest'
test_folder = './results/LastRun'
files = set(os.listdir(base_folder)).union(set(os.listdir(test_folder)))
processed_files = set(os.listdir(os.path.join(base_folder, 'processed_results'))).union(
    set(os.listdir(os.path.join(test_folder, 'processed_results')))
)
processed_files = set([os.path.join('processed_results', file) for file in processed_files])
files = files.union(processed_files)
#remove the Variables_DataFrame.xlsx file
files = [file for file in files if file != 'Variables_DataFrame.xlsx']

processed_results = os.listdir(os.path.join(base_folder, 'processed_results'))
processed_results = [os.path.join('processed_results', file) for file in processed_results]
base_files = os.listdir(base_folder) + processed_results

processed_results = os.listdir(os.path.join(test_folder, 'processed_results'))
processed_results = [os.path.join('processed_results', file) for file in processed_results]
review_files = os.listdir(test_folder) + processed_results


def get_differences(df1, df2, tol=1e2):
    try:
        pd.testing.assert_frame_equal(df1, df2, check_exact=False, atol=tol)
    except AssertionError as e:
        try:
            pd.testing.assert_index_equal(df1.index, df2.index)
            pd.testing.assert_index_equal(df1.columns, df2.columns)
        except AssertionError:
            return (
            f'Index or columns are different, cannot compare dataframes. Shape: {df1.shape}, Columns: {df1.columns}\n',
            f'Index or columns are different, cannot compare dataframes. Shape: {df2.shape}, Columns: {df2.columns}\n')
        # make sure the data types are correctly inferred
        df1 = df1.infer_objects()
        df2 = df2.infer_objects()
        numeric_columns = df1.select_dtypes(include=np.number).columns
        numeric_diff = abs(df1[numeric_columns] - df2[numeric_columns]) > tol
        non_numeric_diff = df1.drop(columns=numeric_columns) != df2.drop(columns=numeric_columns)
        diff = pd.concat([numeric_diff, non_numeric_diff], axis=1)
        diffcols = diff.any(axis=0)
        diffrows = diff.any(axis=1)
        return df1.loc[diffrows, diffcols].to_markdown(), df2.loc[diffrows, diffcols].to_markdown()
    return None, None


markdown_report = {}
table_of_contents = "# Table of Contents\n"
idx = 0
for file in files:
    anchor = file.replace(" ", "-")
    if file not in base_files:
        table_of_contents += f"{idx + 1}. [{file}](#{anchor})\n"
        idx += 1
        markdown_report[file] = {'report':
                                     f"\nThe file: {file} that is in base case {base_folder} is not in test case {test_folder}\n",
                                 'anchor': anchor
                                 }
    if file not in review_files:
        markdown_report[
            file] = {
            'report': f"\nThe file: {file} that is in test case {test_folder} is not in base case {base_folder}\n",
            'anchor': anchor
        }

    if file in base_files and file in review_files:
        file_a = os.path.join(base_folder, file)
        file_b = os.path.join(test_folder, file)

        if file.endswith('.csv'):
            df1 = pd.read_csv(file_a).infer_objects()
            df2 = pd.read_csv(file_b).infer_objects()
            if file == 'capacity_value.csv':
                df1.columns = ['region',
                                'winter.wind',
                                'winter.solar',
                                'summer.wind',
                                'summer.solar',
                                ]
                df1 = df1.drop(0)

                df2.columns = ['region',
                                'winter.wind',
                                'winter.solar',
                                'summer.wind',
                                'summer.solar',
                                ]
                df2 = df2.drop(0)
            diff1, diff2 = get_differences(df1, df2)
            if diff1 is not None:
                table_of_contents += f"{idx + 1}. [{file}](#{anchor})\n"
                idx += 1
                markdown_report[file] = {
                    'report': f"## Base \n{diff1}\n## Test \n{diff2}\n",
                    'anchor': anchor
                }

        if file.endswith('.xlsx'):
            wb_a = openpyxl.load_workbook(file_a, read_only=True)
            wb_b = openpyxl.load_workbook(file_b, read_only=True)
            sheets_a = wb_a.sheetnames
            sheets_b = wb_b.sheetnames
            sheets = set(sheets_a).union(set(sheets_b))
            for sheet in sheets:
                anchor = f"{file}-{sheet}".replace(" ", "-")
                data_name = f'{file}-{sheet}'
                if sheet not in sheets_a:
                    table_of_contents += f"{idx + 1}. [{data_name}](#{anchor})\n"
                    idx += 1
                    markdown_report[data_name] = {
                        'report': f"The sheet: {sheet} in file {file} that is in base case {base_folder} is not in test case {test_folder}\n",
                        'anchor': anchor
                    }
                if sheet not in sheets_b:
                    table_of_contents += f"{idx + 1}. [{data_name}](#{anchor})\n"
                    idx += 1
                    markdown_report[data_name] = {
                        'report': f"The sheet: {sheet} in file {file} that is in test case {test_folder} is not in base case {base_folder}\n",
                        'anchor': anchor
                    }

                if sheet in sheets_a and sheet in sheets_b:
                    df1 = pd.read_excel(file_a, sheet_name=sheet).infer_objects()
                    df2 = pd.read_excel(file_b, sheet_name=sheet).infer_objects()
                    diff1, diff2 = get_differences(df1, df2)
                    table_of_contents += f"{idx + 1}. [{data_name}](#{anchor})\n"
                    idx += 1
                    if diff1 is not None:
                        markdown_report[data_name] = {
                            'report': f"## Base \n{diff1}\n## Test \n{diff2}\n",
                            'anchor': anchor
                        }

# get all images in the images folder
images = os.listdir('images')
# filter to only get files that are images
images = [f'images/{img}' for img in images if img.endswith('.png')]

with open('report.md', 'w') as f:
    # add images at the top of the report
    for img in images:
        f.write(f"![{img}]({img})\n")
    f.write(table_of_contents)
    for key, value in markdown_report.items():
        f.write("\n---\n")
        f.write(f"<a name='{value['anchor']}'></a>")
        f.write(f'\n# {key}\n')
        f.write(value['report'])
        f.write("\n")
