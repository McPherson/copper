import os

import numpy as np
import pandas as pd
import openpyxl
import pytest
import difflib

base_folder = './AllRegionsTest'
test_folder = './results/LastRun'

# files are the union of the files in the two folders
def list_excel_sheets(file):
    if file.endswith(".xlsx"):
        wb = openpyxl.load_workbook(file, read_only=True)
        return wb.sheetnames
    return []

def gather_tests():
    base_files = os.listdir(base_folder)
    tests = []
    for file in base_files:
        if file != 'Variables_DataFrame.xlsx':
            if file.endswith(".xlsx"):
                sheets = list_excel_sheets(os.path.join(base_folder, file))
                for sheet in sheets:
                    tests.append((file, sheet))
            elif file.endswith(".csv"):
                tests.append((file, None))
    test_files = os.listdir(test_folder)
    for file in test_files:
        if file != 'Variables_DataFrame.xlsx':
            if file.endswith(".xlsx"):
                sheets = list_excel_sheets(os.path.join(test_folder, file))
                for sheet in sheets:
                    tests.append((file, sheet))
            elif file.endswith(".csv"):
                tests.append((file, None))
    for file in os.listdir(os.path.join(base_folder, 'processed_results')):
        if file != 'Variables_DataFrame.xlsx':
            if file.endswith(".xlsx"):
                sheets = list_excel_sheets(os.path.join(base_folder, 'processed_results', file))
                for sheet in sheets:
                    tests.append((os.path.join('processed_results', file),
                                  sheet))
            else:
                tests.append((os.path.join('processed_results', file),
                              None))
    for file in os.listdir(os.path.join(test_folder, 'processed_results')):
        if file != 'Variables_DataFrame.xlsx':
            if file.endswith(".xlsx"):
                sheets = list_excel_sheets(os.path.join(test_folder, 'processed_results', file))
                for sheet in sheets:
                    tests.append((os.path.join('processed_results', file),
                                  sheet))
            else:
                tests.append((os.path.join('processed_results', file),
                              None))

    tests = list(set(tests))
    return tests

@pytest.fixture
def base_files():
    processed_results = os.listdir(os.path.join(base_folder, 'processed_results'))
    processed_results = [os.path.join('processed_results', file) for file in processed_results]

    base = os.listdir(base_folder) + processed_results

    # remove all files that do not end with .csv, .xlsx
    return [file for file in base if file.endswith('.csv') or file.endswith('.xlsx')]

@pytest.fixture
def review_files():
    processed_results = os.listdir(os.path.join(test_folder, 'processed_results'))
    processed_results = [os.path.join('processed_results', file) for file in processed_results]

    test = os.listdir(base_folder) + processed_results

    # remove all files that do not end with .csv, .xlsx
    return [file for file in test if file.endswith('.csv') or file.endswith('.xlsx')]


def assert_frame_equal_extended_diff(df1, df2, tol=1e2):
    try:
        pd.testing.assert_frame_equal(df1, df2, check_exact=False, atol=tol)

    except AssertionError as e:
        # if this was a shape or index/col error, then re-raise
        try:
            pd.testing.assert_index_equal(df1.index, df2.index)
            pd.testing.assert_index_equal(df1.columns, df2.columns)
        except AssertionError:
            raise e

        # make sure the data types are correctly inferred
        df1 = df1.infer_objects()
        df2 = df2.infer_objects()

        numeric_columns = df1.select_dtypes(include=np.number).columns

        # For numeric columns, use abs(df1 - df2) > tol
        numeric_diff = abs(df1[numeric_columns] - df2[numeric_columns]) > tol

        # For non-numeric columns, use direct comparison
        non_numeric_diff = df1.drop(columns=numeric_columns) != df2.drop(columns=numeric_columns)

        # Join the diffs
        diff = pd.concat([numeric_diff, non_numeric_diff], axis=1)

        diffcols = diff.any(axis=0)
        diffrows = diff.any(axis=1)

        cmp = pd.concat(
            {'base case': df1.loc[diffrows, diffcols], 'test output': df2.loc[diffrows, diffcols]},
            names=['dataframe'],
            axis=1,
        )

        raise AssertionError(e.args[0] + f'\n\nDifferences:\n{cmp}') from None


@pytest.mark.parametrize("file,sheet", gather_tests())
def test_compare_results(file, sheet, base_files, review_files):
    assert file in review_files, f"The file: {file} that is in base case {base_folder} is not in test case {test_folder}"
    assert file in base_files, f"The file: {file} that is in test case {test_folder} is not in base case {base_folder}"
    file_a = os.path.join(base_folder, file)
    file_b = os.path.join(test_folder, file)
    if file.endswith(".csv"):
        df_a = pd.read_csv(file_a).infer_objects()
        df_b = pd.read_csv(file_b).infer_objects()
        if file == 'capacity_value.csv':
            df_a.columns = ['region',
                               'winter.wind',
                               'winter.solar',
                               'summer.wind',
                               'summer.solar',
                               ]
            df_a = df_a.drop(0)

            df_b.columns = ['region',
                               'winter.wind',
                               'winter.solar',
                               'summer.wind',
                               'summer.solar',
                               ]
            df_b = df_b.drop(0)


        assert_frame_equal_extended_diff(df_a, df_b)
    elif file.endswith(".xlsx"):
        assert sheet is not None, f"No sheet found in Excel file: {file}"
        wb_a = openpyxl.load_workbook(file_a, read_only=True)
        wb_b = openpyxl.load_workbook(file_b, read_only=True)
        sheets_a = wb_a.sheetnames
        sheets_b = wb_b.sheetnames

        sheets = set(sheets_a).union(set(sheets_b))

        # compare sheets_a and sheets_b and have meaningful error messages if they are not equal

        assert sheet in sheets_b, f"The sheet: {sheet} that is in base case {base_folder} is not in test case {test_folder}"
        assert sheet in sheets_a, f"The sheet: {sheet} that is in test case {test_folder} is not in base case {base_folder}"
        df_a = pd.read_excel(file_a, sheet_name=sheet).infer_objects()
        df_b = pd.read_excel(file_b, sheet_name=sheet).infer_objects()
        assert_frame_equal_extended_diff(df_a, df_b)


    elif file.endswith(".txt"):

        with open(file_a, 'r') as f:

            a = f.readlines()

        with open(file_b, 'r') as f:

            b = f.readlines()

        if a != b:
            diff = difflib.unified_diff(a, b)

            diff_report = ''.join(diff)

            assert False, f"The file: {file} in base case {base_folder} is not equal to the file in test case {test_folder}. Differences are:\n{diff_report}"
    else:
        pytest.fail(f"Unexpected file type: {file}")

if __name__ == '__main__':
    pytest.main()



