import pandas as pd
import pytest

base_file = './AllRegionsTest/processed_results/BAU_scenario_output_summary_IAMC_names.csv'
test_file = './results/LastRun/processed_results/BAU_scenario_output_summary_IAMC_names.csv'
base_df = pd.read_csv(base_file)
test_df = pd.read_csv(test_file)
base_df['type'] = base_df['variable'].apply(lambda x: x.split('|')[0])
test_df['type'] = test_df['variable'].apply(lambda x: x.split('|')[0])
base_df = base_df.groupby(['type']).sum().reset_index()
test_df = test_df.groupby(['type']).sum().reset_index()
base_variables = base_df['type'].unique()
test_variables = test_df['type'].unique()


# Helper function to print success message
def print_success_message(variable, diff, base_value, test_value):
    print(
        f"\nPass: Relative difference of {variable} is {diff}. Base: {base_value}, Test: {test_value} \n Absolute difference: {abs(base_value - test_value)}")


def test_compare_variables():
    assert set(base_variables) == set(
        test_variables), f"Variables in base and test files are different. Base: {base_variables}, Test: {test_variables}, Difference: {set(base_variables).symmetric_difference(set(test_variables))}"


to_check = set(base_variables).intersection(set(test_variables))
get_vars = lambda: to_check


@pytest.mark.parametrize("variable", get_vars())
def test_compare_files(variable):
    base = base_df[base_df['type'] == variable]
    test = test_df[test_df['type'] == variable]
    base = base.drop(columns=['type'])
    test = test.drop(columns=['type'])
    diff = (base['value'] / test['value']).values[0]
    # assert relative difference is less than 2%
    if variable == 'Carbon Costs':
        assert diff > 0.94 and diff < 1.06, f"Relative difference of {variable} is {diff}. Base: {base['value'].values[0]}, Test: {test['value'].values[0]} \n Absolute difference: {abs(base['value'].values[0] - test['value'].values[0])}"
    elif variable == 'Emissions':
        assert diff > 0.96 and diff < 1.04, f"Relative difference of {variable} is {diff}. Base: {base['value'].values[0]}, Test: {test['value'].values[0]} \n Absolute difference: {abs(base['value'].values[0] - test['value'].values[0])}"
    else:
        assert diff > 0.98 and diff < 1.02, f"Relative difference of {variable} is {diff}. Base: {base['value'].values[0]}, Test: {test['value'].values[0]} \n Absolute difference: {abs(base['value'].values[0] - test['value'].values[0])}"
    # Print success message if test passes
    print_success_message(variable, diff, base['value'].values[0], test['value'].values[0])


if __name__ == '__main__':
    pytest.main()
