import os

import numpy as np
import openpyxl
import pandas as pd

base_folder = './AllRegionsTest'
test_folder = './results/LastRun'

# files are the union of the files in the two folders
files = set(os.listdir(base_folder)).union(set(os.listdir(test_folder)))
processed_files = set(os.listdir(os.path.join(base_folder, 'processed_results'))).union(
    set(os.listdir(os.path.join(test_folder, 'processed_results')))
)
processed_files = set([os.path.join('processed_results', file) for file in processed_files])
files = files.union(processed_files)
# remove the Variables_DataFrame.xlsx file
files = [file for file in files if file != 'Variables_DataFrame.xlsx']

processed_results = os.listdir(os.path.join(base_folder, 'processed_results'))
processed_results = [os.path.join('processed_results', file) for file in processed_results]
base_files = os.listdir(base_folder) + processed_results

processed_results = os.listdir(os.path.join(test_folder, 'processed_results'))
processed_results = [os.path.join('processed_results', file) for file in processed_results]
review_files = os.listdir(test_folder) + processed_results


def get_differences(df1, df2, tol=1e2):
    try:

        pd.testing.assert_frame_equal(df1, df2, check_exact=False, atol=tol)

    except AssertionError as e:
        # if this was a shape or index/col error, then re-raise
        try:
            pd.testing.assert_index_equal(df1.index, df2.index)
            pd.testing.assert_index_equal(df1.columns, df2.columns)
        except AssertionError:
            return (
            f'Index or columns are different, cannot compare dataframes. Shape: {df1.shape}, Columns: {df1.columns}',
            f'Index or columns are different, cannot compare dataframes. Shape: {df2.shape}, Columns: {df2.columns}')
        # make sure the data types are correctly inferred
        df1 = df1.infer_objects()
        df2 = df2.infer_objects()
        numeric_columns = df1.select_dtypes(include=np.number).columns

        # For numeric columns, use abs(df1 - df2) > tol
        numeric_diff = abs(df1[numeric_columns] - df2[numeric_columns]) > tol

        # For non-numeric columns, use direct comparison
        non_numeric_diff = df1.drop(columns=numeric_columns) != df2.drop(columns=numeric_columns)

        # Join the diffs
        diff = pd.concat([numeric_diff, non_numeric_diff], axis=1)

        diffcols = diff.any(axis=0)
        diffrows = diff.any(axis=1)
        if not diff.empty:
            return df1.loc[diffrows, diffcols].to_html(), df2.loc[diffrows, diffcols].to_html()
        else:
            return None, None
    return None, None


# Initialize a dict for all HTML strings
html_report = {}

for file in files:
    if file not in base_files:
        html_report[
            file] = f"<h1>{file}</h1><p>The file: {file} that is in base case {base_folder} is not in test case {test_folder}</p>"
    if file not in review_files:
        html_report[
            file] = f"<h1>{file}</h1><p>The file: {file} that is in test case {test_folder} is not in base case {base_folder}</p>"
    if file in base_files and file in review_files:
        file_a = os.path.join(base_folder, file)
        file_b = os.path.join(test_folder, file)
        if file.endswith('.csv'):
            df1 = pd.read_csv(file_a).infer_objects()
            df2 = pd.read_csv(file_b).infer_objects()
            diff1, diff2 = get_differences(df1, df2)
            if diff1 is not None:
                html_report[
                    file] = f"<div><h3>Base</h3>{diff1}</div><div><h3>Test</h3>{diff2}</div>"

        if file.endswith('.xlsx'):
            wb_a = openpyxl.load_workbook(file_a, read_only=True)
            wb_b = openpyxl.load_workbook(file_b, read_only=True)
            sheets_a = wb_a.sheetnames
            sheets_b = wb_b.sheetnames

            sheets = set(sheets_a).union(set(sheets_b))
            for sheet in sheets:
                if sheet not in sheets_a:
                    html_report[
                        file] = f"<h1>{file}</h1><p>The sheet: {sheet} in file {file} that is in base case {base_folder} is not in test case {test_folder}</p>"
                if sheet not in sheets_b:
                    html_report[
                        file] = f"<h1>{file}</h1><p>The sheet: {sheet} in file {file} that is in test case {test_folder} is not in base case {base_folder}</p>"
                if sheet in sheets_a and sheet in sheets_b:
                    df1 = pd.read_excel(file_a).infer_objects()
                    df2 = pd.read_excel(file_b).infer_objects()
                    diff1, diff2 = get_differences(df1, df2)

                    data_name = f'{file} - {sheet}'
                    if diff1 is not None:
                        html_report[
                            data_name] = f"<h3>Base</h3>{diff1}</div><div><h3>Test</h3>{diff2}</div>"

css_styles = """
<style>
body {
    font-family: Arial, sans-serif;
}

.tab {
    overflow: auto;
    white-space: nowrap;
    border-bottom: 1px solid #ccc;
}

.tablinks {
    display: inline-block;
    color: black;
    text-align: center;
    text-decoration: none;
    transition: background-color 0.3s;
    font-size: 17px;
    margin-right: 3px;
    padding: 14px 16px;
    background-color: white;
    border: none; 
    border-right: 1px solid #ccc;
}

.tablinks:hover {
    background-color: lightblue;
}

.tablinks.active {
    background-color: lightblue;
}

.tab-content {
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.flex-container {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    margin-bottom: 20px;
}

.flex-item {
    flex: 50%;
    padding: 10px;
    box-sizing: border-box;
}

table {
    width: 100%;
    border-collapse: collapse;
}
</style>
"""

# get all images in the images folder
images = os.listdir('images')
# filter to only get files that are images
images = [f'images/{img}' for img in images if img.endswith('.png')]

# HTML Tab Head
html_tab_head = """"""
# HTML Tab Content
html_tab_content = """"""

# add a plot tab if images are present and add the images to the report
if images:
    html_tab_head += f"<button class='tablinks' onclick=\"open_tab(event, 'Plots')\">Plots</button>\n"
    html_tab_content += f"<div id='Plots' class='tab-content'><h1>Plots</h1>\n <div class='flex-container'>"
    for img in images:
        html_tab_content += f"<div class='flex-item'><img src='{img}'></div>"
    html_tab_content += "</div></div>\n"

for file, html_diff in html_report.items():
    html_tab_head += f"<button class='tablinks' onclick=\"open_tab(event, '{file}')\">{file}</button>\n"

    html_tab_content += f"<div id='{file}' class='tab-content'><h1>{file}</h1>\n <div class='flex-container'> {html_diff} </div></div>\n"

# Write all HTML strings to a report.html file, each tab in its own div
with open('report.html', 'w') as f:
    f.write(f"""
    <!DOCTYPE html>
    <html>
    {css_styles}
    <body>
    <div class="tab">{html_tab_head}</div>{html_tab_content}
    <script>
    function open_tab(evt, file_name) {{
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {{
            tabcontent[i].style.display = "none";
        }}
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {{
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }}
        document.getElementById(file_name).style.display = "block";
        evt.currentTarget.className += " active";
    }}
    </script>
    </body>
    </html>
    """)
