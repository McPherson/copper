import os

import openpyxl
import pandas as pd
import plotly.express as px


def is_different(df1, df2, tol=1e-5):
    try:
        pd.testing.assert_frame_equal(df1, df2,
                                      check_exact=False)
        return False

    except AssertionError as e:
        print(e)
        return True


base_df = pd.read_csv('./AllRegionsTest/processed_results/BAU_scenario_output_summary_IAMC_names.csv')
base_df['region'] = base_df['region'].str.replace('Canada', 'CAN')
if 'BAU_scenario_output_summary_IAMC_names.csv' in os.listdir('results/LastRun/processed_results'):
    df = pd.read_csv('results/LastRun/processed_results/BAU_scenario_output_summary_IAMC_names.csv')
    df['region'] = df['region'].str.replace('Canada', 'CAN')
    classes = df['variable'].str.split('|', expand=True)[0].unique()

    for c in classes:
        df_class = df[df['variable'].str.startswith(c)].copy()
        base_class = base_df[base_df['variable'].str.startswith(c)].copy()
        regions = df_class['region'].unique().tolist()
        base_regions = base_class['region'].unique().tolist()
        if 'CAN' not in regions:
            can_df = df_class.groupby(['time', 'variable']).sum().reset_index()
            can_df['region'] = 'CAN'
            df_class = pd.concat([df_class, can_df], ignore_index=True)

        if 'CAN' not in base_regions:
            can_df = base_class.groupby(['time', 'variable']).sum().reset_index()
            can_df['region'] = 'CAN'
            base_class = pd.concat([base_class, can_df], ignore_index=True)
        df_class = df_class[df_class['region'] == 'CAN']
        base_class = base_class[base_class['region'] == 'CAN']

        diff = pd.merge(base_class, df_class, on=['time', 'variable', 'region'], suffixes=('_base', '_test'))
        diff['value'] = diff.value_test.fillna(0) - diff.value_base.fillna(0)

        diff = diff[['time', 'value']]
        diff = diff.groupby(['time']).sum().reset_index()

        fig = px.bar(diff, x='time', y='value', title=f'{c} difference between base and test (summed over all regions)')

        # save the plot
        fig.write_image(f'./images/{c}.png')






else:
    print('No BAU_scenario_output_summary_IAMC_names.csv found in results/LastRun')
