import argparse

import pandas as pd
import os

def process(df):
    df.rename(columns={'Unnamed: 0': 'variable'}, inplace=True)
    # melt the dataframe
    df = pd.melt(df, id_vars=['variable'], var_name='year', value_name='value')


    region = 'CAN'
    pyam_ls = []
    for _, row in df.iterrows():
        variable = row.iloc[0]
        year = row.iloc[1]
        value = row.iloc[2]

        # convert year and hour to datetime object
        date = pd.to_datetime(year, format='%Y').strftime('%Y')
        pyam_ls.append({
            'model': 'copper',
            'region': region,
            'variable': variable,
            'unit': 'MW',
            'time': date,
            'value': value
        })

    return pd.DataFrame(pyam_ls)

