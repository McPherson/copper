import argparse

import pandas as pd
import os

def process(df):

    df = df.iloc[:, 1:]
    pyam_ls = []
    for _, row in df.iterrows():
        entry = row.iloc[0]
        year, region, variable = entry.split('.')
        value = row.iloc[1]

        # convert year and hour to datetime object
        date = pd.to_datetime(year, format='%Y').strftime('%Y')
        pyam_ls.append({
            'model': 'copper',
            'region': region,
            'variable': variable,
            'unit': 'MtCO2',
            'time': date,
            'value': value
        })

    return pd.DataFrame(pyam_ls)