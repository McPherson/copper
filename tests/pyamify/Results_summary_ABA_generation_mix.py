import argparse

import pandas as pd
import os

def process(df):

    pyam_ls = []
    for _, row in df.iterrows():
        year = row.iloc[0]
        variable = row.iloc[1]
        region = row.iloc[2]
        value = row.iloc[3]

        # convert year and hour to datetime object
        date = pd.to_datetime(year, format='%Y').strftime('%Y')
        pyam_ls.append({
            'model': 'copper',
            'region': region,
            'variable': variable,
            'unit': 'MW',
            'time': date,
            'value': value
        })

    pyam_df = pd.DataFrame(pyam_ls)
    return pyam_df

