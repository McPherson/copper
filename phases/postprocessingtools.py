from pathlib import Path
from openpyxl import Workbook
from openpyxl.utils.dataframe import dataframe_to_rows
import pandas as pd
import dill as pickle
from pyomo.environ import (
    ConcreteModel,
    Var,
    PositiveIntegers,
    Binary,
    Constraint,
)
import logging
from tools.COPPER_tools import init_input

def get_all_disc_coeffs(config):
    pds = config["Simulation_Settings"]["pds"]

    return {PD: get_disc_coeff(PD, config) for PD in pds}


def get_disc_coeff(PD, config):
    discount = config["Economics"]["discount"]
    discount_2050 = config["Economics"]["discount_2050"]
    pds = config["Simulation_Settings"]["pds"]
    ind = pds.index(PD)
    if ind != (len(pds) - 1):
        # We use disc_ann_coef as the coeffecient to bring an n-year series Annuity to a net present value
        first_term = 1 / (1 + discount) ** (int(pds[ind + 1]) - int(PD))
        second_term = 1
        third_term = discount
        disc_ann_coef = (second_term - first_term) / third_term
        weird_factor = 1 / (1 + discount) ** ((int(PD) - 1) - 2021)
        disc_coef = (
            disc_ann_coef * weird_factor
        )  # New, we are using annuity series combined with future value to present value formula
    else:
        disc_coef = 1 / (1 + discount_2050) ** (int(PD) - 2021)
    return disc_coef


def unpickle_model(path) -> ConcreteModel:
    """Unpickles a previously pickled Pyomo model from file using `dill`.

    Args:
        path: Path to the pickled Pyomo model file

    Returns:
        model: The unpickled Pyomo ConcreteModel

    """
    model_path = Path(path)
    with model_path.open("rb") as fi:
        model = pickle.load(fi)
    return model


def pickle_model(model, path) -> None:
    """Serializes a Pyomo model using `dill` imported as `pickle`.

    ## Args:
        model: The Pyomo model to serialize.
        path: The path to save the pickled model to.

    ## Returns:
        None

    """
    model_path = Path(path)
    with model_path.open("wb") as fo:
        try:
            # this takes ~ 5 min for a test model. Probably would be faster
            # without serializing all the functions
            pickle.dump(model, fo, byref=True, recurse=True)
        except Exception as e:
            print(e.with_traceback())
            logging.error("An exeption was trown", exc_info=True, stack_info=True)


def load_csv_results(path):
    """loads results from *.csv files in a directory

    Args:
        path (str): The directory containing the results
    """

    result_files = Path(path).glob("*.csv")
    dfs = [pd.read_csv(file) for file in result_files]
    return dfs


def read_multi_index_csv(path) -> pd.DataFrame:
    df = pd.read_csv(path)
    idx_cols = df.columns.tolist()[:-1]
    return df.set_index(idx_cols)


def model_vars_as_dfs(model, debug=False) -> dict[str, pd.DataFrame]:
    df_dict = {}
    # loop over all variables
    for m_var in model.component_objects(Var, active=True):
        # store values in a series
        var_values = pd.Series(m_var.get_values())

        # get names of indices
        subsets = list(m_var.index_set().subsets())
        subset_names = [x.name for x in subsets]

        # print information if desired
        if debug:
            print(f"{m_var} is indexed in {subset_names}")

        # transform to dataframe and store in dictionary
        df = var_values.to_frame().reset_index()
        df.columns = subset_names + ["value"]
        df_dict[f"{m_var}"] = df.copy()

    return df_dict


def variable_frame(instance):
    # Set-up step        
    variable_series = []
    sheet_names = []
    model_vars = instance.component_map(ctype=Var)
    
    # Get resulting variables values
    for k in model_vars.keys():
        v = model_vars[k]
        series = pd.Series(v.extract_values(), index=v.extract_values().keys())
        try:
            if isinstance(series.index[0], tuple):
                series = series.unstack(level=1)
            else:
                series = pd.DataFrame(series)
        except IndexError:
            series = pd.DataFrame(series)
        series.columns = pd.MultiIndex.from_tuples([(k, t) for t in series.columns])
        sheet_names.append(series.columns[0][0])
        variable_series.append(series)
    
    # Write to Excel file
    Variables_file = pd.ExcelWriter("Variables_DataFrame.xlsx", engine="openpyxl")
    for i, temp_df in enumerate(variable_series):
        temp_df.to_excel(Variables_file, sheet_name=f"{sheet_names[i]}", startrow=1, header=True, index=True)
    Variables_file.close()


def save_model_vars(model, results_dir, debug=False):
    # get model vars as dfs
    model_var_dfs = model_vars_as_dfs(model)

    # loop over dict and write csv's
    for name, df in model_var_dfs.items():
        df.to_csv(f"{results_dir}/{name}.csv", index=False)


def export_duals(model, folder_name, duals_of_interest):
    is_bin_or_int = []
    for c in model.component_objects(Var):
        if c.is_indexed():
            first_key = next(c.keys())
            first_val = c[first_key]
            prohibits_duals = (
                first_val.domain is PositiveIntegers or first_val.domain is Binary
            )
        else:
            prohibits_duals = c.domain is PositiveIntegers or c.domain is Binary
        is_bin_or_int.append(prohibits_duals)
    export_duals = not any(is_bin_or_int)

    if export_duals:
        print("No binary or integer variables found, exporting duals.")
        with open(f"{folder_name}/duals.csv", "w") as f:
            f.write("eq,year,hour,node,dual_price\n")
            for c in model.component_objects(Constraint, active=True):
                # export only the marginals of constraints in that list
                if str(c) in duals_of_interest:
                    for index in c:
                        year_hours_node = ",".join(str(x) for x in index)
                        f.write(f"{c},{year_hours_node},{model.dual[c[index]]}\n")
    else:
        print(
            "Model has binary or integer variables. Not exporting dual prices of demand and supply."
        )


def get_all_supply(results_dir) -> pd.DataFrame:
    supply = read_multi_index_csv(f"{results_dir}/supply.csv")
    wind_ons_out = read_multi_index_csv(f"{results_dir}/windonsout.csv")
    wind_ons_out["tplants"] = "wind_ons"
    wind_ofs_out = read_multi_index_csv(f"{results_dir}/windofsout.csv")
    wind_ofs_out["tplants"] = "wind_ofs"
    solar_out = read_multi_index_csv(f"{results_dir}/solarout.csv")
    solar_out["tplants"] = "solar"
    solar_out
    total_supply_df = pd.concat(
        [df.reset_index() for df in [supply, wind_ons_out, wind_ofs_out, solar_out]]
    ).rename({"tplants": "Type"}, axis=1)
    total_supply_df.set_index(["pds", "aba", "Type", "h"], inplace=True)
    return total_supply_df


def approximate_electricity_prices(
    results_dir: str,
    config: dict,
    scenario: str,
    gasprice: dict,
    ctax: dict,
    carbondioxide: dict,
    carbondioxide_obps_2025: dict,
    carbondioxide_obps_2030: dict,
    hourly=False,
):
    """Calculates the average annual electricity price in CAD/MWh (default).
    If `hourly`==True, the marginal hourly prices are returned instead.

    Returns:
        pd.DataFrame : DataFrame with years, provinces (and hours) as index.
    """

    # within COPPER.py, many calls to `os.chdir` necessitate the following
    repo_root = Path(__file__).parent.parent
    get_file = init_input(scenario, repo_root)
    total_supply = get_all_supply(results_dir)
    gendata = pd.read_csv(get_file('generation_type_data.csv'))
    gendata["efficiency"] = gendata["efficiency"].fillna(1)

    tplants = gendata.loc[gendata["Is thermal?"], "Type"].to_list()

    # get energy cost
    energy_cost = energy_cost_per_pd_aba(
        config,
        tplants,
        gendata,
        gasprice,
        ctax,
        carbondioxide,
        carbondioxide_obps_2025,
        carbondioxide_obps_2030,
        return_as_dataframe=True,
    )
    total_supply["energy_price"] = energy_cost["fuel_price"]
    total_supply = total_supply.fillna(0)
    # multiply to align index
    total_supply["vom"] = (total_supply["value"] * 0 + 1) * gendata["variable_o_m"]
    total_supply["spec_gen_cost"] = total_supply["energy_price"] + total_supply["vom"]
    total_supply["act_gen_cost"] = total_supply["value"] * total_supply["spec_gen_cost"]

    # set province for aggregation
    total_supply["province"] = (
        total_supply.reset_index()["aba"].str.split(".", expand=True)[0].values
    )

    # hourly marginal price
    is_generating = total_supply["value"] > 0
    hourly_mcp = (
        total_supply.loc[is_generating, :]
        .groupby(["pds", "province", "h"])["spec_gen_cost"]
        .max()
    )

    if hourly:
        return hourly_mcp

    # hourly generation sum
    hourly_gen = total_supply.groupby(["pds", "province", "h"])["value"].sum()

    # annual price as volume weighted average
    hourly_revenue = hourly_mcp * hourly_gen
    annual_prices = (
        (
            hourly_revenue.groupby(["pds", "province"]).sum()
            / hourly_gen.groupby(["pds", "province"]).sum()
        )
        .to_frame(name="price(CAD/MWh)")
        .reset_index()
    )
    return annual_prices


def energy_cost_per_pd_aba(
    config,
    tplants,
    gen_data: pd.DataFrame,
    gasprice,
    ctax,
    carbondioxide,
    carbondioxide_obps_2025,
    carbondioxide_obps_2030,
    GJ_per_MWh=3.6,
    return_as_dataframe=False,
) -> dict:
    if "Type" in gen_data.columns:
        gen_data.set_index("Type", inplace=True)

    energy_cost = {}
    for PD in config["Simulation_Settings"]["pds"]:
        for TP in tplants:
            for ABA in config["Simulation_Settings"]["aba"]:
                # This first part is computed for the actual fuel cost, it happens regardless
                if TP in [
                    "gasCC_pre2025",
                    "gasCC_ccs_pre2025",
                    "gasCC_backup_pre2025",
                    "gasCC_backup_post2025",
                    "gasSC_pre2025",
                    "gasSC_ccs_pre2025",
                    "gasSC_backup_pre2025",
                    "gasSC_backup_post2025",
                ]:
                    energy_cost[PD + "." + TP + "." + ABA] = (
                        gasprice[ABA] / gen_data.loc[TP, "efficiency"]
                    ) * GJ_per_MWh
                else:
                    energy_cost[PD + "." + TP + "." + ABA] = (
                        gen_data.loc[TP, "fuelprice"] / gen_data.loc[TP, "efficiency"]
                    ) * GJ_per_MWh
                # This next part adds in the carbon cost to the fuel cost, different if CER flag is activated
                if PD == "2025":
                    # In 2025 the OBPS is in effect no matter what, charge carbon tax on residual emissions
                    energy_cost[PD + "." + TP + "." + ABA] += (
                        carbondioxide_obps_2025[TP] * ctax[PD]
                    )
                elif PD == "2030":
                    # In 2030 the OBPS is in effect no matter what, charge carbon tax on residual emissions
                    energy_cost[PD + "." + TP + "." + ABA] += (
                        carbondioxide_obps_2030[TP] * ctax[PD]
                    )
                elif config["Regulations"]["CER"]["flag_cer"]:
                    # If CER is on, then full carbon tax is applied 2035-2050
                    energy_cost[PD + "." + TP + "." + ABA] += (
                        carbondioxide[TP] * ctax[PD]
                    )
                else:  # If CER is false, then OBPS carbon tax is applied 2035-2050 with the OBPS 2030 rate
                    energy_cost[PD + "." + TP + "." + ABA] += (
                        carbondioxide_obps_2030[TP] * ctax[PD]
                    )
    if not return_as_dataframe:
        return energy_cost
    else:
        return energy_cost_per_pd_aba_as_df(energy_cost)


def energy_cost_per_pd_aba_as_df(energy_cost: dict) -> pd.DataFrame:

    energy_price_pds = pd.DataFrame(
        energy_cost.values(), index=energy_cost.keys(), columns=["fuel_price"]
    ).reset_index()
    energy_price_pds.loc[:, ["pds", "Type", "aba"]] = (
        energy_price_pds["index"].str.split(".", n=2, expand=True).values
    )
    energy_price_pds["pds"] = energy_price_pds["pds"].astype(int)
    energy_price_pds = energy_price_pds.drop("index", axis=1).set_index(
        ["pds", "aba", "Type"]
    )
    return energy_price_pds

if __name__ == "__main__":
    pass
