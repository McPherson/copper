import pandas as pd

if __name__ == "__main__":
    from postprocessingtools import read_multi_index_csv
else:
    from phases.postprocessingtools import read_multi_index_csv
import os
import toml

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox as tkmb

from tools.COPPER_disaggregation import vre, copper_generators, coders_query, demand, importexport
from tools.COPPER_disaggregation.supply import calculate_necessary_generators, get_copper_supply


class post_process_copper:
    def __init__(self, run_days, scenario):
        # global variables
        self.path = os.getcwd()
        self.folderpath = os.path.join(self.path, 'processed_results')
        if not os.path.exists(self.folderpath):
            os.mkdir(self.folderpath)
        self.disaggregatedpath = os.path.join(self.folderpath, 'disaggregated_results')
        if not os.path.exists(self.disaggregatedpath):
            os.mkdir(self.disaggregatedpath)

        self.scenario = scenario
        self.VRE = ['wind_ons', 'wind_ofs', 'solar', 'wind_ons_recon', 'wind_ofs_recon', 'solar_recon']
        self.hydro_renewal = ['ror_hydro_renewal', 'hydro_daily_renewal', 'hydro_monthly_renewal']
        self.recon_techs = {'wind_ons_recon': 'wind_ons', 'wind_ofs_recon': 'wind_ofs', 'solar_recon': 'solar'}
        self.supply_files = {'windonsout': 'wind_ons', 'windofsout': 'wind_ofs', 'solarout': 'solar',
                             'daystoragehydroout': 'hydro_daily', 'monthstoragehydroout': 'hydro_monthly'}
        self.VRE_cap_files = {'capacity_solar': 'solar', 'capacity_wind_ofs': 'wind_ofs',
                              'capacity_wind_ons': 'wind_ons', 'capacity_solar_recon': 'solar_recon',
                              'capacity_wind_ons_recon': 'wind_ons_recon', 'capacity_wind_ofs_recon': 'wind_ofs_recon'}

        # commonly used config parameters
        self.config = toml.load('config.toml')
        self.pds = self.config['Simulation_Settings']['pds']
        self.aba = self.config['Simulation_Settings']['aba']
        self.ap = self.config['Simulation_Settings']['ap']
        self.seasons = self.config['Simulation_Settings']['season']
        self.gas_prices = self.config['Thermal']['gasprice']
        self.carbon_tax = self.config['Carbon']['ctax']
        self.cer_flag = self.config['Regulations']['CER']['flag_cer']
        self.trans_o_m = self.config['Transmission']['trans_o_m']
        self.transcost = self.config['Transmission']['transcost']
        self.intra_ba_transcost = self.config['Transmission']['intra_ba_transcost']
        self.reference_year = self.config['Simulation_Settings']['reference_year']
        self.hydro_development = self.config['Hydro']['hydro_development']
        self.storage = self.config['Storage']['st']
        self.coders_url = self.config['Disaggregation_Settings']['CODERS_URL']
        self.vre_gen_types = self.config['Disaggregation_Settings']['vre_gen_types']
        self.min_gen_size = self.config['Disaggregation_Settings']['min_gen_size']

        self.year_fraction = 365 / len(run_days)

        # input data that needs to be used
        self.gen_type_data = pd.read_csv('generation_type_data.csv').drop('Unnamed: 0', axis=1)
        self.hydro_data = pd.read_csv('hydro_new.csv').drop('Unnamed: 0', axis=1)
        self.gridcell_data = pd.read_csv('gridcells.csv').drop('Unnamed: 0', axis=1)
        self.trans_data = pd.read_csv('transmission_data.csv').drop('Unnamed: 0', axis=1)
        self.demand = pd.read_csv('demand.csv').drop('Unnamed: 0', axis=1)
        self.annual_growth = pd.read_csv('annual_growth.csv')
        self.hydro_cf = pd.read_csv('hydro_cf.csv')


        # load tech evolution
        self.tech_evolution = pd.read_csv('technology_evolution.csv')
        self.tech_evolution[['tech', 'scenario']] = self.tech_evolution['tech'].str.split('.', expand=True)
        self.tech_evolution = self.tech_evolution.drop(['scenario'], axis=1)
        self.tech_evolution = pd.melt(self.tech_evolution, id_vars=['tech'], var_name='pds', value_name='value')

        ## commonly used dataframes
        # supply
        self.supply, self.hourly_supply = self.calc_supply_tech_BA()
        # carbon
        self.carbon = self.calc_carbon_tech_BA()
        # capacity
        self.new_capacity_VRE_gridcell = self.calc_new_capacity_VRE_tech_gridcell()
        if self.hydro_development:
            self.new_capacity_hydro_binary = self.calc_new_capacity_hydro_binary(costs=None, cumulative=False)
        self.cumulative_new_capacity = self.calc_new_capacity_tech_ba(cumulative=True)
        self.new_capacity = self.calc_new_capacity_tech_ba(cumulative=False)
        self.capacity = self.calc_capacity_tech_ba()
        self.trans_cap, self.trans_cap_new = self.calc_new_transmission()
        self.trans_flow = self.calc_flow()

        # Qualifying capacity
        self.qual_cap = self.calc_qualifying_cap()

        # costs
        self.trans_cap_cost, self.trans_fix_om = self.transmission_costs()
        self.capacity_cost = self.capacity_costs_ba(cumulative=True)
        self.fixed_om_cost = self.fixed_o_m_costs_ba()
        self.var_om_cost = self.var_o_m_costs_ba()
        self.fuel_cost = self.fuel_costs()
        self.emissions_cost = self.emissions_costs()

        # Running the comparison and returning the calculated objective
        self.calc_obj = self.compare_results()

    def calc_carbon_tech_BA(self):
        """
        Calculates the carbon emissions of each technology in each balancing area
        """
        gen_data = self.gen_type_data[['Type', 'fuel_co2']].copy()

        emissions = self.supply.copy().reset_index()
        emissions = pd.merge(emissions, gen_data, how='left', left_on='variable', right_on='Type')
        emissions['value'] *= emissions.fuel_co2 / 1000000
        emissions = emissions.fillna(0)
        emissions['unit'] = 'MTCO2'
        emissions = emissions.drop(['fuel_co2', 'Type'], axis=1)
        emissions = emissions.groupby(['pds', 'aba', 'variable', 'unit']).sum().reset_index()
        emissions.to_csv(os.path.join(self.folderpath, 'emissions.csv'), index=False)
        return emissions

    def calc_supply_tech_BA(self):
        """
        Calculates electricity provided by each technology in each balancing area
        """
        supply = read_multi_index_csv('supply.csv').reset_index().rename(columns={'tplants': 'variable'})
        techs = self.supply_files

        # read each of the supply output sheets
        for file in techs.keys():
            temp = read_multi_index_csv(f'{file}.csv').reset_index()
            temp['variable'] = techs[file]
            supply = pd.concat([supply, temp])

        # reading run of river data
        ror_hydro_out = pd.read_csv('ror_hydroout.csv')
        ror_hydro_out[['pds', 'h', 'aba']] = ror_hydro_out['Unnamed: 0'].str.split('.', n=2, expand=True)
        ror_hydro_out['pds'] = ror_hydro_out.pds.astype(int)
        ror_hydro_out['h'] = ror_hydro_out.h.astype(int)
        ror_hydro_out['variable'] = 'hydro_run'
        ror_hydro_out = ror_hydro_out.rename(columns={'0': 'value'}).drop('Unnamed: 0', axis=1)
        supply = pd.concat([supply, ror_hydro_out])

        # concatenate the hydro renewal supply
        if self.hydro_development:
            supply = pd.concat([supply, self.calc_hydro_renewal_supply(var_om=False)])

        supply['unit'] = 'MWh'
        hourly_supply = supply.copy()
        hourly_supply['pds'] = pd.to_datetime(hourly_supply['pds'].astype(str) + '-01-01') + pd.to_timedelta(
            hourly_supply['h'], unit='h')
        hourly_supply = hourly_supply.drop(['h'], axis=1)

        supply['value'] *= self.year_fraction
        supply = supply.groupby(['pds', 'aba', 'variable', 'unit']).sum()
        supply = supply.drop('h', axis=1)
        supply.to_csv(os.path.join(self.folderpath, 'supply.csv'))
        return supply, hourly_supply

    def calc_hydro_renewal_supply(self, var_om):
        """
        Calculates the supply coming from binary hydro renewal plants, with the option to calculate variable costs
        """
        # Reading hydro renewal data
        hydro_data = self.hydro_data[['Short Name', 'Balancing Area']].copy()

        # day renewal
        day_renewal_out = read_multi_index_csv('dayrenewalout.csv').reset_index()
        day_renewal_out['variable'] = 'hydro_daily_renewal'

        # month renewal
        month_renewal_out = read_multi_index_csv('monthrenewalout.csv').reset_index()
        month_renewal_out['variable'] = 'hydro_monthly_renewal'

        # ror renewal
        ror_renewal_out = pd.read_csv('rorrenewalout.csv')
        ror_renewal_out[['h', 'aba']] = ror_renewal_out['Unnamed: 0'].str.split('.', n=2, expand=True)
        ror_renewal_out['h'] = ror_renewal_out.h.astype(int)
        ror_renewal_out['variable'] = 'ror_hydro_renewal'
        ror_renewal_out = ror_renewal_out.rename(columns={'0': 'value'}).drop('Unnamed: 0', axis=1)
        ror_binary = read_multi_index_csv('ror_renewal_binary.csv').reset_index()
        ror_renewal_out = pd.merge(ror_renewal_out, ror_binary, how='outer', left_on='aba', right_on='hr_ror',
                                   suffixes=['', '_binary'])
        ror_renewal_out['value'] *= ror_renewal_out['value_binary']
        ror_renewal_out = ror_renewal_out.drop(['aba', 'value_binary'], axis=1)

        # var_om flag: keeps short name and multiplies by year fraction
        if var_om:
            day_renewal_out = pd.merge(hydro_data, day_renewal_out, how='right', left_on='Short Name',
                                       right_on='hr_day').drop(['hr_day'], axis=1).rename(
                columns={'Balancing Area': 'aba'})
            month_renewal_out = pd.merge(hydro_data, month_renewal_out, how='right', left_on='Short Name',
                                         right_on='hr_mo').drop(['hr_mo'], axis=1).rename(
                columns={'Balancing Area': 'aba'})
            ror_renewal_out = pd.merge(hydro_data, ror_renewal_out, how='right', left_on='Short Name',
                                       right_on='hr_ror').drop(['hr_ror'], axis=1).rename(
                columns={'Balancing Area': 'aba'})
            supply = pd.concat([day_renewal_out, month_renewal_out, ror_renewal_out])
            supply['value'] *= self.year_fraction
        else:
            day_renewal_out = pd.merge(hydro_data, day_renewal_out, how='right', left_on='Short Name',
                                       right_on='hr_day').drop(['hr_day', 'Short Name'], axis=1).rename(
                columns={'Balancing Area': 'aba'})
            month_renewal_out = pd.merge(hydro_data, month_renewal_out, how='right', left_on='Short Name',
                                         right_on='hr_mo').drop(['hr_mo', 'Short Name'], axis=1).rename(
                columns={'Balancing Area': 'aba'})
            ror_renewal_out = pd.merge(hydro_data, ror_renewal_out, how='right', left_on='Short Name',
                                       right_on='hr_ror').drop(['hr_ror', 'Short Name'], axis=1).rename(
                columns={'Balancing Area': 'aba'})
            supply = pd.concat([day_renewal_out, month_renewal_out, ror_renewal_out])
        return supply

    def calc_new_capacity_VRE_tech_gridcell(self):
        """
        Groups wind and solar installed capacity per grid cell
        """
        files = self.VRE_cap_files
        capacity = pd.DataFrame()
        for file in files.keys():
            temp = read_multi_index_csv(f'{file}.csv')
            temp['variable'] = files[file]
            if capacity.empty:
                capacity = temp
            else:
                capacity = pd.concat([capacity, temp])
        capacity['unit'] = 'MW'
        capacity = capacity.groupby(['pds', 'gl', 'variable', 'unit']).sum()
        capacity.to_csv(os.path.join(self.folderpath, 'VRE_cap_gridcell.csv'))
        return capacity

    def calc_new_capacity_hydro_binary(self, costs, cumulative=False):
        """
        Calculates capacity of binary hydro units, options to calculate costs instead
        """
        ror_renewal_binaries = read_multi_index_csv('ror_renewal_binary.csv').reset_index()
        ror_renewal_binaries['variable'] = 'ror_hydro_renewal'
        ror_renewal_binaries['hr_day'] = ror_renewal_binaries['hr_ror']

        day_renewal_binaries = read_multi_index_csv('day_renewal_binary.csv').reset_index()
        day_renewal_binaries['variable'] = 'hydro_daily_renewal'

        month_renewal_binaries = read_multi_index_csv('month_renewal_binary.csv').reset_index()
        month_renewal_binaries['variable'] = 'hydro_monthly_renewal'
        month_renewal_binaries['hr_day'] = month_renewal_binaries['hr_mo']

        renewal_binaries = pd.concat([ror_renewal_binaries, day_renewal_binaries, month_renewal_binaries]).drop(
            ['hr_mo', 'hr_ror'], axis=1)
        renewal_binaries = renewal_binaries[renewal_binaries['value'] != 0]

        if renewal_binaries.empty:
            print('No binary hydro')
            self.hydro_development = False
            return pd.DataFrame()

        if costs == 'capital_costs':
            renewal_binaries = pd.merge(renewal_binaries, self.hydro_data[
                ['Short Name', 'Balancing Area', 'Additional Capacity (MW)', 'Annualized Capital Cost ($M/year)',
                 'Distance to Grid (km)']].copy(), how='left', left_on='hr_day', right_on='Short Name').drop(
                ['hr_day', 'value'], axis=1)
            renewal_binaries['unit'] = '$/yr'
            renewal_binaries['Annualized Capital Cost ($M/year)'] *= 1000000
            renewal_binaries['Annualized Capital Cost ($M/year)'] += renewal_binaries[
                                                                         'Distance to Grid (km)'] * self.intra_ba_transcost * \
                                                                     renewal_binaries['Additional Capacity (MW)']
            renewal_binaries = renewal_binaries.drop(['Additional Capacity (MW)', 'Distance to Grid (km)'],
                                                     axis=1).rename(
                columns={'Annualized Capital Cost ($M/year)': 'value'})
            if not cumulative:
                renewal_binaries = renewal_binaries.loc[
                    renewal_binaries.groupby(['Balancing Area', 'Short Name', 'unit'])['pds'].idxmin()]

        elif costs == 'fixed_o_m':
            renewal_binaries = pd.merge(renewal_binaries, self.hydro_data[
                ['Short Name', 'Balancing Area', 'Additional Capacity (MW)', 'Fixed O&M ($/MW-year)']].copy(),
                                        how='left', left_on='hr_day', right_on='Short Name').drop(['hr_day', 'value'],
                                                                                                  axis=1)
            renewal_binaries['unit'] = '$/yr'
            renewal_binaries['Fixed O&M ($/MW-year)'] *= renewal_binaries['Additional Capacity (MW)']
            renewal_binaries = renewal_binaries.drop('Additional Capacity (MW)', axis=1).rename(
                columns={'Fixed O&M ($/MW-year)': 'value'})

        elif costs == 'variable_o_m':
            renewal_binaries = pd.merge(renewal_binaries, self.hydro_data[
                ['Short Name', 'Balancing Area', 'Variable O&M ($/MWh)']].copy(), how='left', left_on='hr_day',
                                        right_on='Short Name').drop(['hr_day', 'value'], axis=1)
            supply = self.calc_hydro_renewal_supply(var_om=True).groupby(
                ['Short Name', 'aba', 'pds', 'variable']).sum().reset_index().drop('h', axis=1)
            renewal_binaries = pd.merge(supply, renewal_binaries, how='right', on=['Short Name', 'pds', 'variable'])
            renewal_binaries['value'] *= renewal_binaries['Variable O&M ($/MWh)']
            renewal_binaries['unit'] = '$/yr'
            renewal_binaries = renewal_binaries.drop(['aba', 'Variable O&M ($/MWh)'], axis=1)
        else:
            renewal_binaries = pd.merge(renewal_binaries, self.hydro_data[
                ['Short Name', 'Balancing Area', 'Additional Capacity (MW)']].copy(), how='left', left_on='hr_day',
                                        right_on='Short Name').drop(['hr_day', 'value'], axis=1).rename(
                columns={'Additional Capacity (MW)': 'value'})
            renewal_binaries['unit'] = 'MW'
            renewal_binaries = renewal_binaries.loc[
                renewal_binaries.groupby(['Balancing Area', 'Short Name', 'unit'])['pds'].idxmin()]
            renewal_binaries = renewal_binaries.drop('Short Name', axis=1)

        renewal_binaries = renewal_binaries.groupby(['pds', 'Balancing Area', 'variable', 'unit']).sum().rename_axis(
            index={'Balancing Area': 'aba'})
        return renewal_binaries.reset_index()

    def calc_new_capacity_tech_ba(self, cumulative):
        """
        Calculates newly built capacity in each period, or a cumulative sum (not considering retirement)
        """
        VRE_cap = self.new_capacity_VRE_gridcell.copy().reset_index().drop('unit', axis=1)
        gridcell_data = self.gridcell_data[['grid_cell', 'ba']].copy()
        VRE_cap = pd.merge(VRE_cap, gridcell_data, how='left', left_on='gl', right_on='grid_cell')
        VRE_cap = VRE_cap.groupby(['pds', 'variable', 'ba']).sum().drop(['gl', 'grid_cell'],
                                                                        axis=1).reset_index().rename(
            columns={'ba': 'aba'})
        capacity = VRE_cap

        if self.hydro_development:
            hydro_renewal_cap = self.new_capacity_hydro_binary.copy().reset_index().drop('unit', axis=1)
            capacity = pd.concat([capacity, hydro_renewal_cap])

        storage_cap = read_multi_index_csv('capacity_storage.csv').reset_index().rename(columns={'st': 'variable'})
        thermal_cap = read_multi_index_csv('capacity_therm.csv').rename_axis(
            index={'tplants': 'variable'}).reset_index()
        capacity = pd.concat([capacity, storage_cap, thermal_cap])
        capacity['unit'] = 'MW'
        if cumulative:
            capacity['value'] = capacity.groupby(['aba', 'variable', 'unit'])['value'].cumsum()
            capacity = capacity.groupby(['pds', 'aba', 'variable', 'unit']).sum()
            capacity.reset_index().to_csv(os.path.join(self.folderpath, 'cumulative_new_capacity_ABA.csv'), index=False)
        else:
            capacity = capacity.groupby(['pds', 'aba', 'variable', 'unit']).sum()
            capacity.reset_index().to_csv(os.path.join(self.folderpath, 'new_capacity_ABA.csv'), index=False)
        return capacity

    def calc_capacity_tech_ba(self):
        """
        Calculates total capacity existing in each period
        """
        ### NOTE: COPPER does not consider retirement of any new non-thermal assets or forced retirement due to end of life
        capacity = self.cumulative_new_capacity.copy().reset_index()
        gridcell_data = self.gridcell_data[['grid_cell', 'ba']].copy()

        # reading extant wind/solar
        extant_wind_solar = pd.read_csv('extant_wind_solar.csv').drop('Unnamed: 0', axis=1)
        extant_wind_solar[['gl', 'variable']] = extant_wind_solar['location'].str.split('.', expand=True)
        extant_wind_solar['gl'] = extant_wind_solar.gl.astype(int)
        extant_wind_solar = pd.melt(extant_wind_solar, id_vars=['gl', 'variable', 'location'], var_name='pds',
                                    value_name='value').drop('location', axis=1)
        extant_wind_solar = pd.merge(extant_wind_solar, gridcell_data, how='left', left_on='gl', right_on='grid_cell')
        extant_wind_solar['pds'] = extant_wind_solar['pds'].astype(int)
        extant_wind_solar = extant_wind_solar.groupby(['pds', 'variable', 'ba']).sum().drop(['gl', 'grid_cell'],
                                                                                            axis=1).reset_index().rename(
            columns={'ba': 'aba'})
        # reading thermal retirement
        retire_thermal = read_multi_index_csv('retire_therm.csv').rename_axis(
            index={'tplants': 'variable'}).reset_index()
        retire_thermal['value'] = retire_thermal.groupby(['aba', 'variable'])['value'].cumsum()
        retire_thermal['pds'] = retire_thermal.pds.astype(int)
        thermal_plants = list(retire_thermal.variable.unique())
        retire_thermal = retire_thermal.groupby(['pds', 'aba', 'variable']).sum()

        # reading extant thermal/storage/hydro
        extant_capacity = pd.read_csv('extant_capacity.csv').drop(['Unnamed: 0'], axis=1)
        extant_capacity = pd.melt(extant_capacity, id_vars=['ABA'], var_name='pds', value_name='value')
        extant_capacity['pds'] = extant_capacity.pds.astype(int)
        extant_capacity[['aba', 'variable']] = extant_capacity['ABA'].str.rsplit('.', n=1, expand=True)
        extant_capacity = extant_capacity[~extant_capacity['variable'].isin(self.VRE)]
        # fixing SMR naming
        extant_capacity['variable'] = extant_capacity['variable'].apply(lambda x: 'nuclear_' + x if 'SMR' in x else x)
        thermal_capacity = extant_capacity[extant_capacity.variable.isin(thermal_plants)].copy()
        extant_capacity = extant_capacity[~extant_capacity.variable.isin(thermal_plants)]
        extant_capacity = extant_capacity.groupby(['pds', 'aba', 'variable']).sum().reset_index()

        # thermal capacity is treated differently, the first year's values are used and the model decides when things are retired
        # this copies the first year's values to all the other years then subtracts the cumulative retirement
        years = list(thermal_capacity.pds.unique())
        thermal_capacity = thermal_capacity[thermal_capacity.pds == min(years)]
        thermal_capacity = thermal_capacity.rename(columns={'value': str(min(years))}).drop(['pds', 'ABA'], axis=1)
        for year in years:
            thermal_capacity[f'{year}'] = thermal_capacity[str(min(years))].copy()
        thermal_capacity = pd.melt(thermal_capacity, id_vars=['aba', 'variable'], var_name='pds', value_name='value')
        thermal_capacity['pds'] = thermal_capacity.pds.astype(int)
        thermal_capacity = thermal_capacity.groupby(['pds', 'aba', 'variable']).sum()
        retire_thermal = retire_thermal.reindex(thermal_capacity.index, fill_value=0)
        thermal_capacity = thermal_capacity.sub(retire_thermal).reset_index()

        capacity = pd.concat([capacity, extant_capacity, extant_wind_solar, thermal_capacity])
        capacity['unit'] = 'MW'
        capacity = capacity.groupby(['pds', 'aba', 'variable', 'unit']).sum()
        capacity.reset_index().to_csv(os.path.join(self.folderpath, 'total_capacity_ABA.csv'), index=False)
        return capacity

    def calc_flow(self):
        flow = pd.read_csv('transmission.csv')
        flow['unit'] = 'MWh'
        flow['pds'] = pd.to_datetime(flow['pds'].astype(str) + '-01-01') + pd.to_timedelta(
            flow['h'], unit='h')
        flow = flow.drop(['h'], axis=1)
        flow = flow.rename(columns={'abba': 'variable'})
        return flow

    def calc_new_transmission(self):
        """
        Calculates newly installed transmission capacity for each line, in each period
        """
        trans_data = self.trans_data.drop(['Distance', 'Expansion_limits'], axis=1).copy().rename(
            columns={'Transmission Line': 'variable'})
        trans_data = pd.melt(trans_data, id_vars=['variable'], var_name='pds', value_name='value_existing')
        trans_data['pds'] = trans_data.pds.astype(int)

        # get region and variable out of variable by splitting at . and taking :2 as aba and 2: as variable
        trans_data['variable'] = trans_data['variable'].apply(lambda x: x.split('.'))
        trans_data['aba'] = trans_data['variable'].apply(lambda x: '.'.join(x[:2]))
        trans_data['variable'] = trans_data['variable'].apply(lambda x: '.'.join(x[2:]))

        trans_cap = read_multi_index_csv('capacity_transmission.csv').reset_index()
        trans_cap = trans_cap.rename(columns={'abba': 'variable'})
        trans_cap['unit'] = 'MW'
        trans_cap = trans_cap.groupby(['pds', 'aba', 'variable', 'unit']).sum().reset_index()

        trans_cap_w_extant = trans_cap.copy()
        trans_cap_w_extant.to_csv(os.path.join(self.folderpath, 'installed_transmission_capacity.csv'), index=False)
        trans_cap_w_extant['value'] = trans_cap_w_extant.groupby(['aba', 'variable', 'unit'])['value'].cumsum()

        trans_cap_w_extant = pd.merge(trans_data, trans_cap_w_extant, how='left', on=['aba', 'variable', 'pds'])
        trans_cap_w_extant['value'] += trans_cap_w_extant['value_existing']
        trans_cap_w_extant = trans_cap_w_extant.drop(['value_existing'], axis=1)
        trans_cap_w_extant['unit'] = 'MW'
        trans_cap_w_extant.to_csv(os.path.join(self.folderpath, 'Transmission_Capacity.csv'), index=False)
        trans_cap_w_extant = trans_cap_w_extant.groupby(['pds', 'aba', 'variable', 'unit']).sum().reset_index()
        return trans_cap_w_extant, trans_cap

    def calc_qualifying_cap(self):
        capacity = self.capacity.copy().reset_index()
        capacity['province'] = capacity.aba.str.split('.').str[0]
        capacity_value = pd.read_csv('capacity_value.csv', index_col=0)

        result = pd.DataFrame()
        for season in self.seasons:
            season_cap = capacity.copy()
            season_cap_val = capacity_value.loc[:, capacity_value.columns.str.contains(season)]
            season_cap_val.columns = season_cap_val.iloc[0]
            season_cap_val = season_cap_val.iloc[1:]
            for tech in season_cap_val.columns:
                temp = season_cap[season_cap.variable.str.contains(tech)]
                temp = pd.merge(temp, season_cap_val[tech], how='left', left_on='province', right_index=True)
                temp[tech] = temp[tech].astype(float)
                temp['value'] *= temp[tech].values
                temp = temp.drop([tech], axis=1)

                season_cap = season_cap[~season_cap.variable.str.contains(tech)]
                season_cap = pd.concat([season_cap, temp])
            season_cap = season_cap.drop('province', axis=1)
            season_cap['variable'] = f'Qualifying_capacity_{season}|' + season_cap['variable']
            result = pd.concat([result, season_cap])
        result = result.drop('ABA', axis=1)
        result = result.groupby(['pds', 'aba', 'variable', 'unit']).sum().reset_index()
        result.to_csv(os.path.join(self.folderpath, 'qualifying_capacity.csv'), index=False)
        return result

    def save_results_summary_excel(self):
        """
        Saves the results summary sheets
        """
        # Carbon emissions
        emissions = self.carbon.copy()
        emissions = emissions[emissions.value != 0]

        emissions_aba = emissions.groupby(['pds', 'aba', 'unit']).sum().reset_index()
        emissions_aba = emissions_aba[emissions_aba.value != 0]

        emissions_national_tech = emissions.groupby(['pds', 'variable', 'unit']).sum().reset_index()
        emissions_national_tech['aba'] = 'Canada'
        emissions_national_tech = emissions_national_tech[emissions_national_tech.value != 0]

        emissions_national = emissions.groupby(['pds', 'unit']).sum().reset_index()
        emissions_national['aba'] = 'Canada'
        emissions_national = emissions_national[emissions_national.value != 0]

        emissions_provincial = emissions.copy()
        emissions_provincial['prov'] = emissions_provincial['aba'].apply(lambda x: x.split('.')[0])
        # drop aba
        emissions_provincial = emissions_provincial.drop('aba', axis=1)
        emissions_provincial = emissions_provincial.groupby(['pds', 'prov', 'variable', 'unit']).sum().reset_index()
        emissions_provincial = emissions_provincial[emissions_provincial.value != 0]

        # Generation
        supply = self.supply.copy().reset_index()
        supply = supply[supply.value != 0]

        supply_national = supply.groupby(['pds', 'variable', 'unit']).sum().reset_index()
        supply_national['aba'] = 'Canada'
        supply_national = supply_national[supply_national.value != 0]

        # Capacity
        new_cap_aba = self.new_capacity.copy().reset_index()
        new_cap_aba = new_cap_aba[new_cap_aba.value != 0]

        new_cap_national = new_cap_aba.groupby(['pds', 'variable', 'unit']).sum().reset_index()
        new_cap_national['aba'] = 'Canada'
        new_cap_national = new_cap_national[new_cap_national.value != 0]

        cap_national = self.capacity.copy().reset_index().groupby(['pds', 'variable', 'unit']).sum().reset_index()
        cap_national['aba'] = 'Canada'
        cap_national = cap_national[cap_national.value != 0]

        cap = self.capacity.copy().reset_index()
        cap = cap[cap.value != 0]
        cap = cap.drop(['ABA'], axis=1)

        # Transmission
        new_transmission = self.trans_cap_new.copy()
        new_transmission = new_transmission[new_transmission.value != 0]

        trans_cap_cost = self.trans_cap_cost.copy().reset_index()
        trans_cap_cost = trans_cap_cost[trans_cap_cost.value != 0]

        ##Costs
        costs = pd.concat(
            [self.capacity_cost, self.fixed_om_cost, self.var_om_cost, self.fuel_cost, self.emissions_cost])
        costs = costs.reset_index()
        costs = costs[costs.value != 0]
        costs['value'] = costs.apply(self.discount_costs, axis=1)
        costs['value'] = costs.apply(self.apply_inflation, axis=1)

        capital_costs = self.capacity_cost.copy().reset_index()
        capital_costs = capital_costs[capital_costs.value != 0]
        canada_investment_costs = capital_costs.groupby(['pds', 'variable', 'unit']).sum().reset_index()

        # Qualifying capacity
        qual_cap = self.qual_cap.copy()
        qual_cap = qual_cap[qual_cap.value != 0]

        # objective value
        obj = pd.read_csv('obj_value.csv').drop('Unnamed: 0', axis=1).rename(
            columns={'Objective_function_value': 'Optimization Obj. Value'})
        obj['Calculated Obj. Value'] = self.calc_obj

        with pd.ExcelWriter(os.path.join(self.folderpath, 'Results_summary.xlsx')) as writer:
            # Carbon emissions
            emissions.to_excel(writer, sheet_name='Carbon_ABA_Tech', index=False)
            emissions_aba.to_excel(writer, sheet_name='Carbon_ABA', index=False)
            emissions_national_tech.to_excel(writer, sheet_name='Carbon_National_TP', index=False)
            emissions_national.to_excel(writer, sheet_name='Carbon_National', index=False)
            emissions_provincial.to_excel(writer, sheet_name='Carbon AP', index=False)

            # Generation
            supply.to_excel(writer, sheet_name='ABA Supply by Source', index=False)
            supply_national.to_excel(writer, sheet_name='Supply by Source', index=False)

            # Capacity
            new_cap_aba.to_excel(writer, sheet_name='New Installed ABA', index=False)
            new_cap_national.to_excel(writer, sheet_name='New Installed Capacity', index=False)
            cap_national = cap_national.drop('ABA', axis=1)
            cap_national.to_excel(writer, sheet_name='Canada Generation Mix', index=False)
            cap.to_excel(writer, sheet_name='ABA Generation Mix', index=False)

            # Transmission
            new_transmission.to_excel(writer, sheet_name='New Installed Transmission', index=False)

            # Costs
            capital_costs = capital_costs.drop(['tech', 'index'], axis=1)
            capital_costs.to_excel(writer, sheet_name='ABA Investment Costs', index=False)
            canada_investment_costs = canada_investment_costs.drop(['tech', 'index'], axis=1)
            canada_investment_costs.to_excel(writer, sheet_name='Canada Investment Costs', index=False)
            trans_cap_cost.to_excel(writer, sheet_name='Transmission Investment', index=False)

            # Qualifying Capacity
            qual_cap.to_excel(writer, sheet_name='Qualifying Capacity', index=False)

            # Objective
            obj.to_excel(writer, sheet_name='Obj', index=False)

    def capacity_costs_VRE(self):
        """
        Calculates the capital costs incurred by wind and solar installation
        """
        recon_costs = {'wind_ons_recon': self.config['Economics']['windcost_recon_ons'],
                       'wind_ofs_recon': self.config['Economics']['windcost_recon_ofs'],
                       'solar_recon': self.config['Economics']['solarcost_recon']}
        gridcell_data = self.gridcell_data[['grid_cell', 'distance_to_grid', 'ba']].copy()
        VRE_cap_gridcell = self.new_capacity_VRE_gridcell.copy().reset_index()
        VRE_cap_gridcell = pd.merge(VRE_cap_gridcell, gridcell_data, how='left', left_on='gl', right_on='grid_cell')
        vre_types = VRE_cap_gridcell['variable'].unique()

        cap_costs = self.gen_type_data[['Type', 'capitalcost']].copy()
        cap_costs = pd.merge(self.tech_evolution.copy(), cap_costs, how='left', left_on='tech', right_on='Type')
        cap_costs['pds'] = cap_costs['pds'].astype(int)

        # # drop non vre techs now for clarity as we'll do it later anyway
        cap_costs = cap_costs[cap_costs['tech'].isin(vre_types)]

        # separate recon techs to new dataframe
        recon_tech_df = cap_costs[cap_costs['tech'].isin(self.recon_techs.values())].copy()

        # invert dictionary to map tech to recon tech
        tech_to_recon = {v: k for k, v in self.recon_techs.items()}
        recon_tech_df['tech'] = recon_tech_df['tech'].map(tech_to_recon)

        # vre recon costs need to be calculated separately as they have a different capital cost
        recon_tech_df['capitalcost'] = recon_tech_df['value'] * recon_tech_df['tech'].map(recon_costs)
        cap_costs['capitalcost'] = cap_costs['capitalcost'] * cap_costs['value']

        cap_costs = pd.concat([cap_costs, recon_tech_df])

        cap_costs = cap_costs.drop(['value', 'Type'], axis=1)

        VRE_cap_gridcell = pd.merge(VRE_cap_gridcell, cap_costs,
                                    how='left', left_on=['variable', 'pds'], right_on=['tech', 'pds'])

        VRE_cap_gridcell['unit'] = '$/yr'

        # the recon costs do not include intra_ba_transcost, set the distance to 0
        VRE_cap_gridcell.loc[VRE_cap_gridcell['variable'].isin(list(self.recon_techs.keys())), 'distance_to_grid'] = 0
        VRE_cap_gridcell['value'] *= VRE_cap_gridcell.capitalcost + (
                    VRE_cap_gridcell['distance_to_grid'] * self.intra_ba_transcost)

        VRE_cap_ba = (VRE_cap_gridcell.groupby(['pds', 'ba', 'variable', 'unit'])
                      .sum()
                      .drop(['gl', 'grid_cell', 'distance_to_grid', 'capitalcost'], axis=1)
                      .rename_axis(index={'ba': 'aba'})
                      .reset_index())

        return VRE_cap_ba

    def capacity_costs_ba(self, cumulative=False):
        """
        Calculates the capital costs for each technology in each balancing area
        """

        # TODO This function is returning a dataframe with an abba with values for rows with aba == Canada, the values in the row seem to be all aba string values concatenated

        # Removes hydro renewal and VRE from the capacity data
        if cumulative:
            new_cap = self.cumulative_new_capacity.copy().reset_index()
        else:
            new_cap = self.new_capacity.reset_index().copy().reset_index()
        new_cap = new_cap[~new_cap['variable'].isin(self.VRE)]
        new_cap = new_cap[~new_cap['variable'].isin(self.hydro_renewal)]

        VRE_cap_ba = self.capacity_costs_VRE().reset_index()

        tech_evolution = self.tech_evolution.copy()

        cap_costs = self.gen_type_data[['Type', 'capitalcost']].copy()
        cap_costs = pd.merge(tech_evolution, cap_costs, how='left', left_on='tech', right_on='Type')
        cap_costs['capitalcost'] *= cap_costs['value']
        cap_costs['pds'] = cap_costs['pds'].astype(int)
        cap_costs = cap_costs.drop(['value', 'tech'], axis=1)

        new_cap = pd.merge(new_cap, cap_costs, how='left', left_on=['variable', 'pds'], right_on=['Type', 'pds'])
        new_cap['value'] *= new_cap['capitalcost']
        new_cap = new_cap.drop(['Type', 'capitalcost'], axis=1)

        trans_capcost = self.trans_cap_cost.copy().reset_index()
        trans_capcost = trans_capcost.groupby(['pds', 'aba', 'unit']).sum().reset_index()
        trans_capcost['variable'] = 'transmission'

        if self.hydro_development:
            hydro_renewal_cost = self.calc_new_capacity_hydro_binary(costs='capital_costs', cumulative=cumulative)
            new_cap = pd.concat([new_cap, hydro_renewal_cost])

        if cumulative:
            VRE_cap_ba['value'] = VRE_cap_ba.groupby(['aba', 'variable', 'unit'])['value'].cumsum()
            trans_capcost['value'] = trans_capcost.groupby(['aba', 'variable', 'unit'])['value'].cumsum()

        new_cap = pd.concat([new_cap, VRE_cap_ba, trans_capcost])
        new_cap['unit'] = '$'

        # apply ITC if enabled
        if self.config['Economics']['flag_itc']:
            self.itc_factor = self.config['Economics']['factor_itc']
            gen_data = self.gen_type_data[['Type', 'ITC support?', 'Is thermal?']]
            self.thermal_itc_gens = list(gen_data[gen_data['ITC support?'] & gen_data['Is thermal?']]['Type'])
            self.itc_gens = list(gen_data[gen_data['ITC support?']]['Type']) + self.VRE + self.hydro_renewal + [
                'transmission']
            new_cap['value'] = new_cap.apply(self.apply_itc, axis=1)

        new_cap = new_cap.groupby(['pds', 'aba', 'variable', 'unit']).sum()
        new_cap.reset_index().to_csv(os.path.join(self.folderpath, 'capital_costs.csv'), index=False)
        return new_cap

    def fixed_o_m_costs_ba(self):
        """
        Calculates the fixed annual operations and maintenance costs for each planning year
        """

        # TODO BUG the dataframe that this function returns has two aba columns one "aba" with correct aba values
        ##        and one "ABA" column for all hydro generation rows with concatenated aba values and hydro type e.g. :Saskatchewan.a.hydro_run
        capacity = self.capacity.copy().reset_index()
        capacity = capacity[~capacity['variable'].isin(self.hydro_renewal)]

        fixed_om_costs = self.gen_type_data[['Type', 'fixed_o_m']].copy()
        fixed_om_costs = fixed_om_costs.set_index(fixed_om_costs['Type'])

        for recon_tech in self.recon_techs.keys():
            tech = self.recon_techs[recon_tech]
            fixed_om_costs.loc[recon_tech, 'fixed_o_m'] = fixed_om_costs.at[tech, 'fixed_o_m'].copy()

        capacity = pd.merge(capacity, fixed_om_costs, how='left', left_on='variable', right_index=True)
        capacity['value'] *= capacity['fixed_o_m']
        capacity = capacity.drop(['Type', 'fixed_o_m'], axis=1)

        if self.hydro_development:
            hydro_renewal_fix_o_m = self.calc_new_capacity_hydro_binary(costs='fixed_o_m')
            ### TEMPORARY: THIS IS TO MATCH A BUG IN THE COPPER.PY CODE, REMOVE LINE BELOW WHEN FIXED
            hydro_renewal_fix_o_m['value'] = hydro_renewal_fix_o_m.groupby(['aba', 'variable', 'unit'])[
                'value'].cumsum()
            hydro_renewal_fix_o_m.to_csv(os.path.join(self.folderpath, 'hydro_fixed_o_m_costs.csv'), index=False)
            capacity = pd.concat([capacity, hydro_renewal_fix_o_m])
        capacity['unit'] = '$'
        capacity = capacity.groupby(['pds', 'aba', 'variable', 'unit']).sum().reset_index()

        # TODO BUG This function is returning a dataframe with column name "abba" with values for rows with aba == Canada, the values in the row seem to be all aba string values concatenated
        trans_om = self.trans_fix_om.reset_index()
        # trans_om['aba'] = 'Canada'
        trans_om['variable'] = 'transmission'
        capacity = pd.concat([capacity, trans_om])
        capacity = capacity.groupby(['pds', 'aba', 'variable', 'unit']).sum()
        capacity.reset_index().to_csv(os.path.join(self.folderpath, 'fixed_o_m_costs.csv'), index=False)
        return capacity

    def var_o_m_costs_ba(self):
        """
        Calculates the variable operations and maintenance costs for each year
        """
        supply = self.supply.copy().reset_index()
        supply = supply[~supply['variable'].isin(self.hydro_renewal)]

        # Variable O&M for storage currently not included in the COPPER formulation
        # storage_activity = (read_multi_index_csv('storageout.csv') + read_multi_index_csv('storagein.csv')).reset_index().rename(columns={'st':'variable'})
        # storage_activity = storage_activity.groupby(['pds', 'aba', 'variable']).sum().reset_index().drop('h',axis=1)
        # supply = pd.concat([supply, storage_activity])

        var_om_costs = self.gen_type_data[['Type', 'variable_o_m']].copy()
        supply = pd.merge(supply, var_om_costs, how='left', right_on='Type', left_on='variable')
        supply['value'] *= supply['variable_o_m']
        supply = supply.drop(['Type', 'variable_o_m'], axis=1)

        if self.hydro_development:
            hydro_costs = self.calc_new_capacity_hydro_binary(costs='variable_o_m')
            supply = pd.concat([supply, hydro_costs])

        supply['unit'] = '$'
        supply = supply.groupby(['pds', 'aba', 'variable', 'unit']).sum()
        supply.reset_index().to_csv(os.path.join(self.folderpath, 'variable_o_m_costs.csv'), index=False)
        return supply

    def fuel_costs(self):
        """
        Calculates the fuel costs for thermal plants
        """
        # NOTE: COPPER includes emissions in the fuel costs, here they are separated
        gen_data = self.gen_type_data[['Type', 'efficiency', 'fuelprice']].copy()

        # gas_gens = gen_data[gen_data['Type'].str.contains('gas') & ~gen_data['Type'].str.contains('rng') & gen_data['fuelprice'] != 0 & ~gen_data['Type'].str.contains('restricted')]
        gas_list = ['gasCC_pre2025', 'gasCC_ccs_pre2025', 'gasCC_backup_pre2025', 'gasCC_backup_post2025',
                    'gasSC_pre2025', 'gasSC_ccs_pre2025', 'gasSC_backup_pre2025', 'gasSC_backup_post2025']
        gas_gens = gen_data[gen_data['Type'].isin(gas_list)]
        gen_data = gen_data[~gen_data['Type'].isin(list(gas_gens['Type']))]
        gas_price = pd.DataFrame.from_dict(self.gas_prices, orient='index')
        gas_price.columns = ['fuel_price']
        gas_price = gas_price.reset_index()
        gas_price = pd.merge(gas_gens, gas_price, how='cross').rename(
            columns={'Type': 'variable', 'index': 'aba'}).drop('fuelprice', axis=1).rename(
            columns={'fuel_price': 'fuelprice'})

        supply = self.supply.copy().reset_index()
        gas_supply = supply[supply['variable'].isin(list(gas_gens['Type']))]
        supply = pd.merge(supply, gen_data, how='left', right_on='Type', left_on='variable')
        supply = supply[~supply['Type'].isna() & supply['fuelprice'] != 0]
        supply = supply.drop('Type', axis=1)

        gas_supply = pd.merge(gas_supply, gas_price, how='left', on=['aba', 'variable'])
        supply = pd.concat([supply, gas_supply])
        supply['value'] *= (supply['fuelprice'] / supply['efficiency']) * 3.6  # 3.6 MWh/GJ
        supply['unit'] = '$'
        supply = supply.drop(['efficiency', 'fuelprice'], axis=1).groupby(['pds', 'aba', 'variable', 'unit']).sum()
        supply.reset_index().to_csv(os.path.join(self.folderpath, 'fuelcosts.csv'), index=False)
        return supply

    def emissions_costs(self):
        """
        Calculates the cost of carbon emissions for thermal generators
        """
        gen_data = self.gen_type_data[['Type', 'fuel_co2', 'fuel_co2_obps_2025', 'fuel_co2_obps_2030']].copy()

        carbon_tax = pd.DataFrame.from_dict(self.carbon_tax, orient='index').reset_index()
        carbon_tax.columns = ['pds', 'carbon_price']
        carbon_tax['pds'] = carbon_tax['pds'].astype(int)
        carbon_tax['carbon_price'] = carbon_tax['carbon_price'].astype(float)

        supply = self.supply.copy().reset_index()
        supply = supply[~supply['variable'].isin(self.hydro_renewal)]

        for period in self.pds:
            period = int(period)
            if period == 2025:
                value = gen_data['fuel_co2_obps_2025'].copy()
            elif period == 2030:
                value = gen_data['fuel_co2_obps_2030'].copy()
            elif period > 2030:
                if self.cer_flag:
                    value = gen_data['fuel_co2'].copy()
                else:
                    value = gen_data['fuel_co2_obps_2030'].copy()
            gen_data[period] = value

        gen_data = gen_data.drop(['fuel_co2', 'fuel_co2_obps_2025', 'fuel_co2_obps_2030'], axis=1)
        gen_data = pd.melt(gen_data, id_vars=['Type'], var_name='pds', value_name='co2_intensity').rename(
            columns={'Type': 'variable'})
        supply = pd.merge(supply, gen_data, how='left', on=['pds', 'variable'])
        supply['pds'] = supply['pds'].astype(int)

        supply = pd.merge(supply, carbon_tax, how='left', on='pds')
        supply['value'] *= supply['carbon_price'] * supply['co2_intensity']
        supply['unit'] = '$'
        supply = supply.drop(['co2_intensity', 'carbon_price'], axis=1).groupby(
            ['pds', 'aba', 'variable', 'unit']).sum()
        supply.reset_index().to_csv(os.path.join(self.folderpath, 'emissions_cost.csv'), index=False)
        return supply

    def transmission_costs(self):
        """
        Calculates the transmission capital/fixed O&M costs
        """
        # capital costs
        transcap_new = self.trans_cap_new.copy()
        trans_data = self.trans_data[['Transmission Line', 'Distance']].copy().rename(
            columns={'Transmission Line': 'variable'})
        trans_data['variable'] = trans_data['variable'].apply(lambda x: x.split('.'))
        trans_data['aba'] = trans_data['variable'].apply(lambda x: '.'.join(x[:2]))
        trans_data['variable'] = trans_data['variable'].apply(lambda x: '.'.join(x[2:]))
        transcap_new = pd.merge(transcap_new, trans_data, how='left', on=['aba', 'variable'])
        # drop na row
        transcap_new = transcap_new.dropna()
        transcap_new['value'] *= transcap_new['Distance'] * self.transcost
        # transcap_new['aba'] = 'Canada'
        transcap_new['unit'] = '$'
        cap_cost = transcap_new.fillna(0).drop('Distance', axis=1)
        cap_cost = cap_cost.groupby(['pds', 'aba', 'variable', 'unit']).sum()

        trans_om = self.trans_cap.copy()
        trans_om['value'] *= self.trans_o_m
        trans_om['unit'] = '$'
        trans_om = trans_om.groupby(['pds', 'aba', 'variable', 'unit']).sum()
        return cap_cost, trans_om

    def cost_summary(self):
        """
        Summary of costs
        """
        capital_costs = self.capacity_cost.copy().reset_index()
        capital_costs['variable'] = 'Capital Costs|' + capital_costs['variable']

        fixed_om = self.fixed_om_cost.copy().reset_index()
        fixed_om['variable'] = 'Fixed O&M Costs|' + fixed_om['variable']

        var_om = self.var_om_cost.copy().reset_index()
        var_om['variable'] = 'Variable O&M Costs|' + var_om['variable']

        fuel_costs = self.fuel_cost.copy().reset_index()
        fuel_costs['variable'] = 'Fuel Costs|' + fuel_costs['variable']

        carbon_costs = self.emissions_cost.copy().reset_index()
        carbon_costs['variable'] = 'Carbon Costs|' + carbon_costs['variable']

        trans_cap_cost = self.trans_cap_cost.copy().reset_index()
        # trans_o_m = self.trans_fix_om.copy().reset_index()
        trans_cap_cost['variable'] = 'Capital Costs|' + 'transmission'
        # trans_o_m['variable'] = 'Transmission Fixed O&M|' + trans_o_m['variable']

        costs = pd.concat([capital_costs, fixed_om, var_om, fuel_costs, carbon_costs, trans_cap_cost])
        costs = costs[['pds', 'aba', 'variable', 'unit', 'value']]
        # costs['value'] = costs.apply(self.discount_costs, axis=1)
        costs.to_csv(os.path.join(self.folderpath, 'Cost_summary.csv'), index=False)
        return costs

    def output_summary(self):
        import json
        """
        WIP: outputs all relevant model results to a pyam formatted csv
        """
        cost_summary = self.cost_summary()
        # Carbon emissions
        emissions = self.carbon.copy()

        emissions['variable'] = 'Emissions|' + emissions['variable']

        # Generation
        supply = self.hourly_supply.copy().reset_index()
        supply['variable'] = 'Generation|' + supply['variable']

        # Storage
        storage_out = read_multi_index_csv('storageout.csv').reset_index().rename(columns={'st': 'variable'})
        storage_out['pds'] = pd.to_datetime(storage_out['pds'].astype(str) + '-01-01') + pd.to_timedelta(
            storage_out['h'], unit='h')
        storage_out = storage_out.drop(['h'], axis=1)
        storage_out['variable'] = 'Storage Out|' + storage_out['variable']
        storage_out['unit'] = 'MWh'

        storage_in = read_multi_index_csv('storagein.csv').reset_index().rename(columns={'st': 'variable'})
        storage_in['pds'] = pd.to_datetime(storage_in['pds'].astype(str) + '-01-01') + pd.to_timedelta(
            storage_in['h'], unit='h')
        storage_in = storage_in.drop(['h'], axis=1)
        storage_in['variable'] = 'Storage In|' + storage_in['variable']
        storage_in['unit'] = 'MWh'

        # Capacity
        new_cap = self.new_capacity.copy().reset_index()
        new_cap['variable'] = 'New Capacity|' + new_cap['variable']
        total_cap = self.capacity.copy().reset_index()
        total_cap['variable'] = 'Total Capacity|' + total_cap['variable']

        # Transmission
        new_transmission = self.trans_cap_new.copy()
        new_transmission['variable'] = 'New Transmission|' + new_transmission['variable']
        total_transmission = self.trans_cap.copy()
        total_transmission['variable'] = 'Total Transmission|' + total_transmission['variable']
        flow = self.trans_flow.copy()
        flow['variable'] = 'Flow|' + flow['variable']

        # Qualifying capacity
        qual_cap = self.qual_cap.copy()
        qual_cap = qual_cap[qual_cap.value != 0]

        # VRE gridcells
        vre_cells = self.new_capacity_VRE_gridcell.copy().reset_index()
        vre_cells['variable'] = 'Merra Capacity|' + vre_cells['variable']
        vre_cells = vre_cells.rename(columns={'gl': 'aba'})

        data = pd.concat(
            [cost_summary, emissions, supply, new_cap, total_cap, new_transmission, total_transmission, flow, qual_cap,
             storage_in, storage_out, vre_cells])
        data['model'] = 'copper'
        data['scenario'] = self.scenario
        data = data.rename(columns={'pds': 'time', 'aba': 'region'})
        data = data[['model', 'scenario', 'region', 'variable', 'unit', 'time', 'value']]
        data = data[data['value'] != 0]
        data.to_csv(os.path.join(self.folderpath, f'{self.scenario}_output_summary_copper_names.csv'), index=False)

        data['type'] = data['variable'].apply(lambda x: x.split('|')[0])
        data['variable'] = data['variable'].apply(lambda x: x.split('|')[1])

        phases_path = os.path.join(os.path.dirname(__file__), 'iamc_mapping.json')
        mapping = json.load(open(phases_path))
        data['variable'] = data['variable'].map(mapping).fillna(data['variable'])
        data['variable'] = data['type'] + '|' + data['variable']
        data = data.drop('type', axis=1)
        data.to_csv(os.path.join(self.folderpath, f'{self.scenario}_output_summary_IAMC_names.csv'), index=False)

    def compare_results(self):
        """
        Function for debugging, used to evaluate how close the results calculated in these scripts are to the results
        output by the model. All differences should be 0
        """
        model_cap_cost = pd.read_csv('capcost.csv').rename(columns={'value': 'model_value'})
        model_fix_om = pd.read_csv('fixOMcost.csv').rename(columns={'value': 'model_value'})
        model_var_om = pd.read_csv('varOMcost.csv').rename(columns={'value': 'model_value'})
        model_fuel_costs = pd.read_csv('fuel_cost.csv').rename(columns={'value': 'model_value'})
        model_new_storage_cap_cost = pd.read_csv('newstorage_ccost.csv').rename(columns={'value': 'model_value'})
        mode_new_storage_om = pd.read_csv('newstorage_omcost.csv').rename(columns={'value': 'model_value'})
        if self.hydro_development:
            model_hydro_renewal_cap_cost = pd.read_csv('hydrorenewalccost.csv').rename(columns={'value': 'model_value'})
            model_hydro_renewal_om = pd.read_csv('hydrorenewalomcost.csv').rename(columns={'value': 'model_value'})
        model_obj = pd.read_csv('obj_value.csv').rename(columns={'Objective_function_value': 'model_value'})

        capital_costs = self.capacity_cost.copy().reset_index()
        cap_costs = capital_costs.copy()
        capital_costs = capital_costs[
            ~(capital_costs.variable.isin(self.hydro_renewal)) & ~(capital_costs.variable.isin(self.storage))]
        capital_costs = capital_costs.groupby(['pds']).value.sum().reset_index()
        model_cap_cost = pd.merge(model_cap_cost, capital_costs, on='pds')

        fixed_om = self.fixed_om_cost.copy().reset_index()
        fixom = fixed_om.copy()
        fixed_om = fixed_om[~(fixed_om.variable.isin(self.hydro_renewal)) & ~(fixed_om.variable.isin(self.storage))]
        fixed_om = fixed_om.groupby('pds').value.sum().reset_index()
        model_fix_om = pd.merge(model_fix_om, fixed_om, on='pds')

        var_om = self.var_om_cost.copy().reset_index()
        varom = var_om.copy()
        var_om = var_om[~(var_om.variable.isin(self.hydro_renewal)) & ~(var_om.variable.isin(self.storage))]
        var_om = var_om.groupby('pds').value.sum().reset_index()
        model_var_om = pd.merge(model_var_om, var_om, on='pds')

        fuel_costs = self.fuel_cost.copy().reset_index()
        fuel_costs = pd.concat([fuel_costs, self.emissions_cost.copy().reset_index()])
        fuel_costs = fuel_costs.groupby('pds').value.sum().reset_index()
        model_fuel_costs = pd.merge(model_fuel_costs, fuel_costs, on='pds')

        storage_cap_cost = cap_costs.copy()
        storage_cap_cost = storage_cap_cost[storage_cap_cost.variable.isin(self.storage)]
        storage_cap_cost = storage_cap_cost.groupby(['pds']).value.sum().reset_index()
        model_new_storage_cap_cost = pd.merge(model_new_storage_cap_cost, storage_cap_cost, on='pds')

        storage_om = fixom.copy()
        storage_om = storage_om[storage_om.variable.isin(self.storage)]
        storage_om = storage_om.groupby(['pds']).sum().reset_index()
        mode_new_storage_om = pd.merge(mode_new_storage_om, storage_om, on='pds')

        costs = pd.concat([model_cap_cost, model_fix_om, model_var_om, model_fuel_costs, model_new_storage_cap_cost,
                           mode_new_storage_om])

        if self.hydro_development:
            renewal_cap_cost = cap_costs.copy()
            renewal_cap_cost = renewal_cap_cost[renewal_cap_cost.variable.isin(self.hydro_renewal)]
            renewal_cap_cost['value'] = renewal_cap_cost.groupby(['aba', 'variable', 'unit'])['value'].cumsum()
            renewal_cap_cost = renewal_cap_cost.groupby(['pds']).value.sum().reset_index()
            model_hydro_renewal_cap_cost = pd.merge(model_hydro_renewal_cap_cost, renewal_cap_cost, on='pds')

            renewal_om = fixom.copy()
            renewal_om = pd.concat([renewal_om, varom.copy()])
            renewal_om = renewal_om[renewal_om.variable.isin(self.hydro_renewal)]
            renewal_om = renewal_om.groupby(['pds']).value.sum().reset_index()
            model_hydro_renewal_om = pd.merge(model_hydro_renewal_om, renewal_om, on='pds')

            costs = pd.concat([costs, model_hydro_renewal_cap_cost, model_hydro_renewal_om])

        costs = costs.groupby('pds').value.sum().reset_index()
        costs['value'] = costs.apply(self.discount_costs, axis=1)
        costs['value'] = costs.apply(self.apply_inflation, axis=1)
        model_obj['value'] = costs['value'].sum()

        print('-----COST COMPARISON-----')
        print(
            f'Capital costs difference: {round(abs((model_cap_cost.model_value - model_cap_cost.value).sum()) / ((model_cap_cost.value.sum() + model_cap_cost.model_value.sum()) / 2) * 100, 3)}%')
        print(
            f'Fixed OM costs difference: {round(abs((model_fix_om.model_value - model_fix_om.value).sum()) / ((model_fix_om.value.sum() + model_fix_om.model_value.sum()) / 2) * 100, 3)}%')
        print(
            f'Variable OM costs difference: {round(abs((model_var_om.model_value - model_var_om.value).sum()) / ((model_var_om.value.sum() + model_var_om.model_value.sum()) / 2) * 100, 3)}%')
        print(
            f'Fuel costs difference: {round(abs((model_fuel_costs.model_value - model_fuel_costs.value).sum()) / ((model_fuel_costs.value.sum() + model_fuel_costs.model_value.sum()) / 2) * 100, 3)}%')
        print(
            f'Storage Capital costs difference: {round(abs((model_new_storage_cap_cost.model_value - model_new_storage_cap_cost.value).sum()) / ((model_new_storage_cap_cost.value.sum() + model_new_storage_cap_cost.model_value.sum()) / 2) * 100, 3)}%')
        print(
            f'Storage OM costs difference: {round(abs((mode_new_storage_om.model_value - mode_new_storage_om.value).sum()) / ((mode_new_storage_om.value.sum() + mode_new_storage_om.model_value.sum()) / 2) * 100, 3)}%')

        if self.hydro_development:
            print(
                f'Hydro Renewal Captial costs difference: {round(abs((model_hydro_renewal_cap_cost.model_value - model_hydro_renewal_cap_cost.value).sum()) / ((model_hydro_renewal_cap_cost.value.sum() + model_hydro_renewal_cap_cost.model_value.sum()) / 2) * 100, 3)}%')
            print(
                f'Hydro Renewal OM costs difference: {round(abs((model_hydro_renewal_om.model_value - model_hydro_renewal_om.value).sum()) / ((model_hydro_renewal_om.value.sum() + model_hydro_renewal_om.model_value.sum()) / 2) * 100, 3)}%')

        print(
            f'Objective cost difference: {round(abs((model_obj.model_value - model_obj.value).sum()) / ((model_obj.value.sum() + model_obj.model_value.sum()) / 2) * 100, 3)}%')

        with pd.ExcelWriter(os.path.join(self.folderpath, 'value_comparison.xlsx')) as writer:
            model_cap_cost.to_excel(writer, sheet_name='Capital Costs', index=False)
            model_fix_om.to_excel(writer, sheet_name='Fixed OM Costs', index=False)
            model_var_om.to_excel(writer, sheet_name='Variable OM Costs', index=False)
            model_fuel_costs.to_excel(writer, sheet_name='Fuel Costs', index=False)
            model_new_storage_cap_cost.to_excel(writer, sheet_name='Storage Capital Costs', index=False)
            mode_new_storage_om.to_excel(writer, sheet_name='Storage OM Costs', index=False)

            if self.hydro_development:
                model_hydro_renewal_cap_cost.to_excel(writer, sheet_name='Hydro Renewal Capital Costs', index=False)
                model_hydro_renewal_om.to_excel(writer, sheet_name='Hydro Renewal OM Costs', index=False)

            model_obj.to_excel(writer, sheet_name='Objective Costs', index=False)

        return model_obj['value']

    def apply_itc(self, row):
        """
        Applies itc factor to costs
        """
        year = int(row['pds'])

        if row['variable'] in self.itc_gens:
            if year <= 2030:
                return row['value'] * self.itc_factor
            else:
                if row['variable'] in self.VRE:
                    return row['value'] * self.itc_factor
                elif row['variable'] == 'transmission':
                    return row['value'] * self.itc_factor
                else:
                    return row['value']
        else:
            return row['value']

    def apply_inflation(self, row):
        """
        Applies inflation
        """
        inflation = self.config['Economics']['inflation']
        year = int(row['pds'])

        inf_coef = (1 + inflation) ** (year - self.reference_year)
        return row['value'] * inf_coef

    def discount_costs(self, row):
        """
        Applies discount and inflation rate to annualized costs, as in COPPER.py
        """
        discount = self.config['Economics']['discount']
        discount_2050 = self.config['Economics']['discount_2050']
        end_year = int(self.config['Simulation_Settings']['forecast_year'])
        year = int(row['pds'])

        # calculating discount coefficient, this assumes 5 year investment periods
        if year == end_year:
            disc_coef = (1 / (1 + discount_2050) ** (year - self.reference_year))
        else:
            first_term = 1 / ((1 + discount) ** ((year + 5) - year))
            disc_ann_coef = ((1 - first_term) / discount)
            disc_coef = disc_ann_coef * (1 / (1 + discount) ** ((year - 1) - self.reference_year))
        return row['value'] * disc_coef
    
####################### Generator disaggregation #######################

    def show_tkinter_error_box(self, message):
        root = tk.Tk()
        root.withdraw()
        root.attributes('-topmost', True)
        tkmb.showerror("Error", message)
        root.destroy()


    def ask_user_for_folder(self, message="select folder"):  # Create a window
        root = tk.Tk()
        root.withdraw()
        root.attributes('-topmost', True)
        path = filedialog.askdirectory(
            title=message
        )
        root.destroy()
        return path


    def select_options(self, options, message):
        selected_options = []

        def close_window():
            nonlocal selected_options
            selected_options = [listbox.get(idx) for idx in listbox.curselection()]
            root.destroy()

        root = tk.Tk()
        root.geometry("400x250")
        root.attributes('-topmost', True)
        root.title(message)
        listbox = tk.Listbox(root, selectmode=tk.MULTIPLE, bd=5)
        for option in options:
            listbox.insert(tk.END, option)
            # listbox.selection_set(tk.END)  # Select all options by default

        listbox.pack(padx=10, pady=10)

        button = tk.Button(root, text="OK", command=close_window)
        button.pack(pady=5)

        root.mainloop()

        return selected_options


    def disaggregate_generators(self):
        
        df_supply = pd.read_csv(os.path.join(self.folderpath, 'new_capacity_ABA.csv'))
        # rename for ease of use
        df_supply = df_supply.rename(columns={'pds': 'year',
                                            'aba': 'balancing_area',
                                            'variable': 'generation_type',
                                            'value': 'capacity'})
        copper_supply = get_copper_supply(df_supply).copy()
        copper_supply.replace({'hydro_run': 'hydro_hourly'}, inplace=True)

        # Uses wind/solar capacity files instead of aba generation mix because we need latitude and longitude
        merra_data = self.gridcell_data[['lon', 'lat', 'ba', 'pr', 'grid_cell']].copy()
        merra_data.rename(columns={'pr': 'Province'}, inplace=True)
        copper_vre_generators = vre.get_wind_and_solar_capacity(merra_data)

        generators = coders_query.get_CODERS_generators(self.coders_url)
        storage = coders_query.get_CODERS_storage(self.coders_url)
        storage.rename(columns = {
            'storage_facility_name': 'generation_unit_name',
            'storage_code': 'generation_unit_code',
            'connecting_voltage_in_kv': 'connecting_voltage',
            'storage_type': 'gen_type',
            'storage_type_copper': 'gen_type_copper',
            'storage_capacity': 'unit_installed_capacity'
            }, inplace=True)
        generators = pd.concat([generators, storage], ignore_index=True)

        generators['copper_balancing_area'] = generators['copper_balancing_area'].str.lower()
        # unit_installed_capacity is read in as object by default
        generators['unit_installed_capacity'] = generators['unit_installed_capacity'].astype('float64')
        generators['latitude'] = generators['latitude'].astype('float64')
        generators['longitude'] = generators['longitude'].astype('float64')


        # province in coders is a 2 letter abbreviation and we need the full name
        generators['province'] = generators['operating_region'].str.lower()
        generators.replace({'hydro_run': 'hydro_hourly'}, inplace=True)

        # allow users to select which provinces to convert
        possible_provinces = copper_supply.province.unique()
        possible_provinces.sort()
        selected_provinces = self.select_options(possible_provinces, "Select provinces to convert")

        # allow users to select which years to convert
        possible_years = []
        for province in selected_provinces:
            possible_years.extend(copper_supply[copper_supply.province == province].year.unique())
        possible_years = sorted(set(possible_years))
        selected_years = self.select_options(possible_years, "Select years to convert")
        selected_years = [int(year) for year in selected_years]


        #############################################################

        avg_gen_size_canada = generators.groupby('gen_type_copper')['unit_installed_capacity'].mean()

        generators_necessary = calculate_necessary_generators(generators, copper_supply)
        for year in selected_years:
            for province in selected_provinces:
                existing_generators = generators[(generators['province'].str.lower() == province.lower())].reset_index(drop=True)

                if not copper_vre_generators.empty:
                    new_vre_generators = copper_vre_generators[(copper_vre_generators['province'] == province.lower())
                                                            & (copper_vre_generators['year'] == year)].reset_index(drop=True)
                else:
                    new_vre_generators = copper_vre_generators

                new_generators = generators_necessary[(generators_necessary['year'] == year)
                                                    & (generators_necessary['province'] == province)]

                if not new_vre_generators.empty:
                    existing_generators = vre.add_vre_generators(existing_generators, new_vre_generators, avg_gen_size_canada)

                existing_generators = copper_generators.add_new_copper_generators(existing_generators, new_generators, avg_gen_size_canada, 
                                                                                  self.vre_gen_types, self.min_gen_size)

                disaggregated_scenario_name = f'{self.scenario}_{province}_{year}'
                with open(f"{self.disaggregatedpath}/disaggregated_scenario_names.txt", 'a+') as f:
                    f.write(f'{disaggregated_scenario_name}\n')
                
                existing_generators.to_csv(f'{self.disaggregatedpath}/raw_generators_data_to_meet_supply_for_{disaggregated_scenario_name}.csv', index=False)

                if existing_generators['generation_unit_code'].str.contains('need_to_build').any():
                    print(f"WARNING: {disaggregated_scenario_name} has generators that may need to be manually assigned")
                
                extracted_demand = demand.extract_provincial_demand(province, year, self.demand, self.annual_growth, self.reference_year)
                extracted_hydro_cf = demand.get_copper_demand(self.hydro_cf, province, year)
                extracted_importexport = importexport.get_copper_transmission(pd.read_csv('transmission.csv'), province, year)

                extracted_demand.to_csv(f'{self.disaggregatedpath}/raw_demand_for_{disaggregated_scenario_name}.csv', index=False)
                extracted_hydro_cf.to_csv(f'{self.disaggregatedpath}/raw_hydro_cf_for_{disaggregated_scenario_name}.csv', index=False)
                extracted_importexport.to_csv(f'{self.disaggregatedpath}/raw_importexport_for_{disaggregated_scenario_name}.csv')

        print("Generator disaggregation complete for the following provinces and years:")
        print("YEARS SELECTED: ", selected_years)
        print("PROVINCES SELECTED: ", selected_provinces)
        print(f"Generator and demand data has been saved to: {self.disaggregatedpath}")




if __name__ == "__main__":
    path = 'results\\LastRun'
    os.chdir(path)

    post_process = post_process_copper([1], 'BAU_scenario')
    # post_process.calc_carbon_tech_BA()
    # post_process.calc_supply_tech_BA()
    # post_process.calc_capacity_VRE_tech_gridcell()
    # post_process.calc_new_capacity_tech_ba()
    # post_process.calc_capacity_tech_ba()
    # post_process.calc_new_transmission()
    post_process.save_results_summary_excel()

    # post_process.capacity_costs_VRE()
    # post_process.capacity_costs_ba()
    # post_process.fixed_o_m_costs_ba()
    # post_process.var_o_m_costs_ba()
    # post_process.fuel_costs()
    # post_process.emissions_costs()
    # post_process.transmission_costs()
    # post_process.cost_summary()

    post_process.output_summary()

    post_process.disaggregate_generators()

    # post_process.compare_results()