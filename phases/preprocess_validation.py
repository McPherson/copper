import pandas as pd
import os
import sys
import toml
from datetime import datetime, timedelta

class pre_process_copper:

	def __init__(self, scenario:str):
		'''
		COPPER model pre-process

		Methods:
		* config_validation: verifies the config file
		* input_validation: verifies the scenario folder and input files
		'''
		self.scenario = scenario
		self.scenario_path = os.path.join('scenarios', scenario)
		#self.common_inputs = os.path.join('scenarios','common')
		self.sample_config = toml.load(os.path.join(os.getcwd(), 'phases', "sample_config.toml"))


	# Input processing functions

	def config_validation(self):
		'''
		Function to verify the COPPER config file


		Input: path to config.toml file
		Output: Error, or success messages

		Logic: only checking for missing fields, since additional settings won't hinder
		model execution, only missing settings will cause the model to fail
		'''
		file_path=os.path.join(self.scenario_path, 'config.toml')
		try:
			with open(file_path, 'r') as config_file:
				check_config = toml.load(config_file)
				print("Config file read successfully!")
		except FileNotFoundError:
			print(f"TOML config file not found in path {file_path}")
			sys.exit()

		sample_file = self.sample_config

		# Format verification
		for section, fields in sample_file.items():
			for field, sub_field in fields.items():
				# First check that all fields and sections are present in the config file
				if section not in check_config:
					raise Exception(f"Missing section '{section}' in config file '{file_path}'")
				elif field not in check_config[section]:
					raise Exception(f"Missing field '{field}' in section '{section}'")

		##############################################################################

		# Verifying specific sections of the config file

		# Verifying provinces and balancing areas
		balancing_areas = [tuple(i.split(".")) for i in sample_file["Simulation_Settings"]["aba"]]
		balancing_areas_dict = {}
		for i in balancing_areas:
			if i[0] in balancing_areas_dict.keys():
				balancing_areas_dict[i[0]] = [balancing_areas_dict[i[0]],i[1]]
			else:
				balancing_areas_dict[i[0]] = i[1]

		for section, fields in check_config.items():
			for field, sub_field in fields.items():
				if field == 'aba' and self.scenario != 'Regional_scenario':
					check_aba = [j for j in sample_file["Simulation_Settings"]["aba"] if j not in sub_field]
				elif field == 'aba' and self.scenario == 'Regional_scenario':
					check_aba = [j for j in check_config["Simulation_Settings"]["aba"] if j not in sub_field]

		if check_aba != []:
			aba_dict = dict([tuple(i.split(".")) for i in check_aba])
			raise Exception(f"Missing balancing areas from: {aba_dict.keys()} in field {section}.{field}\nThese provinces have the following balancing areas: {[balancing_areas_dict[i] for i in aba_dict.keys()]}")


		print(f"{'#' * 100}\nConfig file '{file_path}' passed format verification!\n{'#' * 100}")


	def input_validation(self, mode="common"):
		'''
		Function to validate COPPER input files

		Input: path to input / scenario folder
		Output: error, or success messages

		Logic: checking that all required files are present in the
		provided scenario folder. Additionaly, check if the individual file is
		structured to corresponding standard
		'''
		input_path = self.scenario_path
		if not os.path.isdir(input_path):
			print(f"Couldn't find path {input_path}\nCheck your scenario again")
			sys.exit()

		########################################################
		match mode:
			case "common":
				base_path = os.path.join(os.getcwd() + "\\scenarios\\common")
			case "demand":
				base_path = os.path.join(os.getcwd() + "\\scenarios\\BAU_scenario")

		base_dict = {}
		for root, _,input_files in os.walk(base_path):
			relative_path = os.path.relpath(root, base_path)
			input_files = [i for i in input_files if i.endswith('.csv')]
			base_dict[relative_path] = input_files
		#########################################################

		scenario_dict = {}
		for root, _,input_files in os.walk(input_path):
			relative_path = os.path.relpath(root, input_path)
			input_files = [i for i in input_files if i.endswith('.csv')]
			scenario_dict[relative_path] = input_files

		# Check if all input files exist in the scenario folder
		missing_files = []
		for k,v in base_dict.items():
			missing_files.append([f for f in v if f in scenario_dict[k]])
		match mode:
			case "common":
				print(f"Using the following files from scenario folder:\n{missing_files}")
			case "demand":
				for i in missing_files:
					if i != []:
						raise Exception(f"Files {i} not found! in {input_path}")

		# Check file formatting - if additional files exist, their format is not checked
		for k,v in scenario_dict.items():
			try:
				if k == 'demand':
					for f in v:
						df_base = pd.read_csv(base_path + "\\" + k + "\\" + f, sep=',', header=0)
						df_scenario = pd.read_csv(input_path + "\\" + k + "\\" + f, sep=',', header=0)
						for col in df_base.columns:
							if col not in df_scenario.columns:
								raise Exception(f"The file {f} is missing required columns at the given input")
				else:
					for f in v:
						df_base = pd.read_csv(base_path + "\\" + f, sep=',', header=0)
						df_scenario = pd.read_csv(input_path + "\\" + f, sep=',', header=0)
						for col in df_base.columns:
							if col not in df_scenario.columns:
								raise Exception(f"The file {f} is missing required columns at the given input scenario")
			except FileNotFoundError:
				continue

		print(f"{'#' * 100}\nInputs for scenario {self.scenario_path} passed verification!\n{'#' * 100}")

	def runday_generator(self, demand_file="scenarios\\common\\demand.csv", config_file="BAU_scenario\\config.toml"):
		run_days = [] # Create array for run days
		try:
			demand_df = pd.read_csv(demand_file, sep=',', header=0, index_col=0)
		except FileNotFoundError:
			print(f"No demand.csv file found in {self.scenario}. Using common demand file to generate run days")
			demand_df = pd.read_csv("scenarios\\common\\demand.csv", sep=',', header=0, index_col=0)
		demand_df.index.names = ['hour'] # Rename index column

		# Create total demand column
		demand_df["Total"] = demand_df.sum(axis=1)

		# Create date column with months to which each index belongs
		start_date = datetime(datetime.now().year,1,1)
		date_range = [start_date + timedelta(hours=i) for i in demand_df.index]
		demand_df["Date"] = [date.strftime('%b') for date in date_range]


		# Loop through total demand column and grab representative days
		all_months = demand_df["Date"].unique()
		for month in all_months:
			monthly_demands = demand_df[demand_df["Date"] == month]
			max_demand_hour = monthly_demands["Total"].idxmax()
			# Pick the representative days = round(hour of the year / 24)
			run_days.append(round((int(max_demand_hour) + 1)/24)) # +1 because indices start at 0

		run_days_test = [run_days[0]]

		# Change to scenario folder to write run days to the config file
		config_file = os.path.join('scenarios', config_file) 

		with open(config_file, 'r') as read_config:
			lines = read_config.readlines()
		with open(config_file, 'w') as update_config:
			for line in lines:
				if 'run_days=' in line:
					parts_of_line = line.split('run_days=',1)
					line = parts_of_line[0] + 'run_days=' + f"{run_days}\n"
				elif 'run_days_test=' in line:
					parts_of_line = line.split('run_days_test=',1)
					line = parts_of_line[0] + 'run_days_test=' + f"{run_days_test}\n"
				update_config.write(line)

		print("Updated run days for full and test runs respectively to:\n", run_days, "\n", run_days_test)




