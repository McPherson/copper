# COPPER 10.0
## Description
The **C**anadian **O**pportunities for **P**lanning and **P**roduction of **E**lectricity **R**esources (COPPER) framework is an electricity system planning model. It minimizes total system costs (including investment, operation and maintenance costs) over an extended planning period. The COPPER framework builds upon the CREST framework developed by Dolter and Rivers (Dolter and Rivers, 2018) with several important modifications. 

Updated mathematical notation for COPPER 9.0 is availaible in the documentation folder

### Authors:

Founding authors of the project are:

- Dr. Reza Arjmand
- Dr. Madeleine McPherson
- Dr. Jacob G. Monroe
- Keegan Griffiths
- Omar Attia
- Nathan de Matos
- Mackenzie Judson
- Cristiano Fernandes
- David Huckebrink
- Ahnaf Ahmed
- Dominic Rivest

Other important contributors to the project are:

- Government of Canada (NRCan)

## Full documenation is now being hosted on read the docs [here](https://sesit-copper.readthedocs.io/en/latest/)

## Quickstart

1) Clone the repository:
    * `git clone https://gitlab.com/sesit/copper.git`

2) Install either [Anaconda](https://www.anaconda.com/) or [Miniconda](https://docs.conda.io/projects/miniconda/en/latest/). During installation make sure you add `conda` to your path variable.

3) Open your local version of copper (the directory, cloned in step 1) and enter the following commands to create and activate the conda environment:
    * `conda create -n copper python=3.10`
    * `conda activate copper` (You have to activate the environment each time the promt is started.)
    * Install packages (relevant for the first time you activate the environment): `pip install -r requirements.txt`
 
4) Run COPPER
    * `python COPPER.py`

## Release Notes

Below is a structured change log for the updates made. Each entry briefly describes the change and its impact or purpose.

### Version 10.0 May 2024
#### Added
- **Pyomo Sets for Indexing:** Implemented Pyomo sets to index `pyomovar`, replacing manual changes and hardcoded indexing. This enhances model scalability and maintainability.
- **Index Sets for Column Names:** Utilized the newly introduced index sets for dynamic column name determination, improving code readability and flexibility.
- **Loop for Output Writing:** Replaced lengthy output writing statements with a loop iterating over all model variables, streamlining the output process.
- **Multiindexed Dataframes:** Transitioned from using `.iloc` to multiindexed dataframes for detailed data handling, enabling province-level copper runs.
- **Common Folder for Shared Files:** Introduced a common folder to manage shared files across different scenarios, facilitating better file organization and access.
- **Command-Line Arguments:** Added command-line arguments to select solver options, scenarios, and test configurations, increasing user control and flexibility in model execution.
- **Detailed Storage Data in Postprocessing:** Enhanced postprocessing to output more detailed storage data, providing deeper insights into storage dynamics.
- **IAMC Mappings:** Incorporated IAMC mappings for postprocessed data, aiming for improved compatibility and cross-referencing with other modeling teams.
- **CI Tests:** Implemented continuous integration tests to ensure code stability and reliability through automated testing routines.
- **New IAMC Nomenclature:** Adopted new IAMC nomenclature, promoting standardization and easier integration with external models and teams.

#### Improved
- **Postprocessing and Preprocessing Logic:** Made substantial improvements to the logic in both preprocessing and postprocessing, optimizing data handling and analysis.
- **Datetime Usage in Postprocessing:** Refined datetime usage in postprocessing to enhance performance and accuracy of time-based calculations.
- **Requirements file:** Included requirements.txt file in favor of env.yaml for python environments. This change alligns the Python packages used by COPPER in both local machines and the Digital Alliance of Canada's cloud computers

#### Fixed
- **Pandas Deprecation Warnings:** Addressed and resolved warnings related to deprecated Pandas operations, ensuring compatibility with newer library versions.

#### Removed
- **Redundant `.iloc[:]` Usage:** Eliminated unnecessary `.iloc[:]` usage, which did not alter the dataframe but cluttered the code.
- **Unused GLs Removal:** Added functionality to remove generator lines (GLs) that are not used when the model includes only a subsection of the full dataset, optimizing resource usage and processing time.
- **Deprecated Operations and Data Manipulation:** Removed outdated operations and data manipulation methods, cleaning up the codebase and improving execution efficiency.

This change log outlines the major changes to the capacity expansion model, highlighting efforts to modernize the code, enhance functionality, and maintain alignment with best practices in data handling and modeling.

### Version 9.0 October 2023
New:
- CER constraints (w/ new generation data)
- Better OBPS implementation
- Load Shedding constraint
- Better handling of inputs
- Faster computing time for inputs
- Better handling of hydro renewal binaries
- demand.csv and us_demand.csv synchronized for UTC
- fix of transcap constraint 

### Version 8.0 Jun 2023
New:
- wind offshore
- CODERS 2021 data

### Version 7.3
New: added cost breakdown

### Version 7.2 Nov 2021

- Converted windg solarg from equality to less than equal constraints
- Done: technology evolution, limit new thermal generation, thermal phase out, just small hydro, national carbon limit
- Done: new storage technology, fix pumped continues to include new build
- Done: limited new capacity of a specific generation type
- Done: Solved tranmission  cap problem
- Done: Transmission expansion limit
- New: supply by source
- New: added a provincial breakdown of new installed capacity
- New: added supply by source
- discount
- renewable min max limit per AP

## References
Dolter, B., Rivers, N., 2018. The cost of decarbonizing the Canadian electricity system. Energy Policy 113, 135–148. https://doi.org/10.1016/j.enpol.2017.10.040

COPPER has been written by Reza Arjmand, Ph.D candidate at UVic (rezaarjmand@uvic.ca; rezaarjmand@yahoo.com). Linkedin: www.linkedin.com/in/rezaarjmand.

Please cite the paper below:

Arjmand, Reza, and Madeleine McPherson. "Canada's electricity system transition under alternative policy scenarios." Energy Policy 163 (2022): 112844. https://doi.org/10.1016/j.enpol.2022.112844
