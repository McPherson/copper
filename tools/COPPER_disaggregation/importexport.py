import pandas as pd
from datetime import datetime as dt
from datetime import timedelta as td
from calendar import monthrange


start = dt(1900, 1, 1, 0, 0, 0)
result = [start]
for i in range(1, 8760):
    start += td(seconds=3600)
    result.append(start)

year_hours = pd.DataFrame({'dates': result})
year_hours['8760'] = year_hours.index+1
year_hours['hour'] = year_hours['dates'].dt.hour
year_hours['month'] = year_hours['dates'].dt.month

map_hh = dict(year_hours[['8760', 'hour']].values)
map_hm = dict(year_hours[['8760', 'month']].values)


def get_copper_transmission(rep_transmission, province, year):
    """
    Get copper interprovincial transmission hourly results, filter it by province and year, and create annual timeseries.
    Assumed that importexport schdeule will follow representative day schedule.

    Parameters:
        transmission (str): Path to the output hourly transmission file.
        province (str): Province to filter results by.
        year (str): Year to filter results by.

    Returns:
        DataFrame: Formatted DataFrame with hourly transmission import/exports from province.
    """

    # filter by relevant year
    rep_transmission = rep_transmission[rep_transmission['pds']==int(year)]
    # filter out hours without transmission
    rep_transmission = rep_transmission.loc[~(rep_transmission['value']==0)]

    rep_transmission['from_prov'] = rep_transmission['aba'].str.split('.', expand=True)[0].str.lower()
    rep_transmission['to_prov'] = rep_transmission['abba'].str.split('.', expand=True)[0].str.lower()
    rep_transmission['importexport'] = rep_transmission['from_prov']+'_'+rep_transmission['to_prov']
    # filter by relevant province
    rep_transmission = rep_transmission[rep_transmission['importexport'].str.contains(province)]
    # exclude intraprovincial transmission for provinces with nultiple BAs
    rep_transmission = rep_transmission.loc[~(rep_transmission['importexport']==f'{province}_{province}')]

    # create seperate importexport streams for each province
    ie_streams = []
    for index, row in rep_transmission.iterrows():
        if row.from_prov == province:
            stream = f'importexport_{row.to_prov}'
            rep_transmission.loc[index, stream] = -row.value
            ie_streams.append(stream)
        if row.to_prov == province:
            stream = f'importexport_{row.from_prov}'
            rep_transmission.loc[index, stream] = row.value
            ie_streams.append(stream)
            
    ie_streams = list(set(ie_streams))
    rep_transmission.fillna(0, inplace=True)

    # group by hour
    gb_filter = ie_streams.copy()
    gb_filter.append('h')
    rep_transmission = rep_transmission[gb_filter].groupby(by=['h']).sum()

    # associate representative day hour with month
    rep_transmission['month'] = rep_transmission.index.map(map_hm)
    rep_transmission['hour'] = rep_transmission.index.map(map_hh)
    rep_transmission.reset_index(drop=True, inplace=True)

    # use represenative day hourly transmission as daily importexport timeseries for whole month
    hourly_transmission = pd.DataFrame()
    for month in rep_transmission['month'].unique():
        in_month = rep_transmission['month'] == month
        rep_day = rep_transmission[in_month]
        month_days = monthrange(int(year), month)[1]
        for day in range(1,month_days+1):
            rep_day['day'] = day
            hourly_transmission = pd.concat([hourly_transmission, rep_day], ignore_index=True)

    for index,row in hourly_transmission.iterrows():
        hourly_transmission.loc[index, 'date'] = dt(int(year), int(row.month), int(row.day), int(row.hour))

    # create propoerly formatted datetime for entire year
    start_date = f'{int(year)}-01-01'
    end_date = f'{int(year)+1}-01-01'
    date_range = pd.date_range(start=start_date, end=end_date, freq='h', inclusive='left')
    hour_template = pd.DataFrame(date_range, columns=['date'])
    importexport_df = pd.merge(hourly_transmission, hour_template, how="right", on='date').fillna(0)

    final_filter = ie_streams.copy()
    final_filter.append('date')
    importexport_df = importexport_df[final_filter].set_index('date')
    importexport_df.columns = importexport_df.columns.str.replace(' ', '')

    return importexport_df
        