import pandas as pd


def get_copper_supply(new_capacity: pd.DataFrame):
    """
    Get copper supply data from the generation mix DataFrame.

    Parameters:
        generation_mix_df (DataFrame): DataFrame containing copper supply data with columns:
                                       'year', 'balancing_area', 'generation_type', 'capacity'

    Returns:
        DataFrame: DataFrame containing copper supply data with columns:
                    'year', 'var', 'balancing_area', 'generation_type', 'supply'.
                    Rows with 0 or near 0 supply values are removed.
    """
    new_capacity.rename(columns={'generation_type': 'gen_type_copper'}, inplace=True)

    # Create a new column for the supply and demand DataFrames
    new_capacity['province'] = new_capacity['balancing_area'].str.split('.').str[0].str.lower()

    # Remove 0 and near 0 values from the 'capacity' column
    new_capacity = new_capacity[new_capacity['capacity'] >= 0.01]

    # Convert 'year' column from float values to year datetime values
    # new_capacity['year'] = pd.to_datetime(new_capacity['year'].copy(), format='%Y').dt.year


    return new_capacity


def calculate_necessary_generators(generators_df, supply_df):
    """
    Calculate necessary generators to meet copper supply.

    Parameters:
        generators_df (DataFrame): DataFrame containing generator data.
        supply_df (DataFrame): DataFrame containing copper supply data.

    Returns:
        DataFrame: DataFrame containing necessary generator data to meet copper supply.
    """
    generators_df['province'] = generators_df['copper_balancing_area'].str.split('.').str[0]

    generators_aggregated = generators_df.groupby(['copper_balancing_area', 'gen_type_copper']).agg(
        {'unit_installed_capacity': 'sum'}).reset_index()

    generators_aggregated.rename(columns={'copper_balancing_area': 'balancing_area'}, inplace=True)
    generators_aggregated['province'] = generators_aggregated['balancing_area'].str.split('.').str[0]
    generators_aggregated.drop(columns=['province'], inplace=True)

    generators_to_meet_supply = pd.merge(generators_aggregated, supply_df, how="right",
                                         on=['balancing_area', 'gen_type_copper'])

    
    # generators_to_meet_supply['unit_installed_capacity'].fillna(0, inplace=True)
    # generators_to_meet_supply['capacity'].fillna(0, inplace=True)
    generators_to_meet_supply.fillna({'unit_installed_capacity': 0}, inplace=True)
    generators_to_meet_supply.fillna({'capacity': 0}, inplace=True)
    

    generators_to_meet_supply['dif'] = generators_to_meet_supply['capacity'] - generators_to_meet_supply[
        'unit_installed_capacity']

    return generators_to_meet_supply
