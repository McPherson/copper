import pandas as pd
import random
import math

def remove_excess_generation_capacity(generators_df, excess_capacity, generation_type):
    """
    Remove excess generation capacity from the specified generator type in the DataFrame.

    Parameters:
        generators_df (DataFrame): DataFrame containing the generator data.
        excess_capacity (float): The amount of capacity to be removed.
        generation_type (str): The type of generation to be modified.

    Returns:
        DataFrame: Updated DataFrame with the removed excess capacity.
    """
    while excess_capacity > 0:
        # Find the oldest generator of the specified generation type
        old_gen = generators_df[(generators_df['gen_type_copper'] == generation_type)
                                & (generators_df['unit_installed_capacity'] > 0)].sort_values(
            by=['closure_year', 'possible_renewal_year'], ascending=True).iloc[0]

        if old_gen['unit_installed_capacity'] >= excess_capacity:
            # Reduce the capacity of the selected generator
            generators_df.at[old_gen.name, 'unit_installed_capacity'] -= excess_capacity

            # if old_gen['gen_type_copper'].lower() not in storage_types:
            #     new_facility_cap = generators_df.loc[generators_df['generation_facility_code'] == old_gen[
            #         'generation_facility_code']]['unit_installed_capacity'].sum()
            #     generators_df.loc[generators_df['generation_facility_code'] == old_gen[
            #         'generation_facility_code'], 'facility_installed_capacity'] = new_facility_cap

            new_facility_cap = generators_df.loc[generators_df['generation_facility_code'] == old_gen[
                'generation_facility_code']]['unit_installed_capacity'].sum()
            generators_df.loc[generators_df['generation_facility_code'] == old_gen[
                'generation_facility_code'], 'facility_installed_capacity'] = new_facility_cap

            return generators_df

        else:
            # Remove the entire capacity of the selected generator
            generators_df.at[old_gen.name, 'unit_installed_capacity'] = 0
            excess_capacity -= old_gen['unit_installed_capacity']

            # if old_gen['gen_type_copper'].lower() not in storage_types:
            #     new_facility_cap = generators_df.loc[generators_df['generation_facility_code'] == old_gen[
            #         'generation_facility_code']]['unit_installed_capacity'].sum()
            #     generators_df.loc[generators_df['generation_facility_code'] == old_gen[
            #         'generation_facility_code'], 'facility_installed_capacity'] = new_facility_cap

            new_facility_cap = generators_df.loc[generators_df['generation_facility_code'] == old_gen[
                'generation_facility_code']]['unit_installed_capacity'].sum()
            generators_df.loc[generators_df['generation_facility_code'] == old_gen[
                'generation_facility_code'], 'facility_installed_capacity'] = new_facility_cap

        # need to be able to break function in case of supply/copper output inconsistencies
        if generators_df[generators_df['gen_type_copper'] == generation_type]['unit_installed_capacity'].sum() == 0:
            # if old_gen['gen_type_copper'].lower() not in storage_types:
            #     generators_df.loc[generators_df['generation_facility_code'] == old_gen[
            #         'generation_facility_code'], 'facility_installed_capacity'] = 0

            generators_df.loc[generators_df['generation_facility_code'] == old_gen[
                'generation_facility_code'], 'facility_installed_capacity'] = 0

            return generators_df

    return generators_df

def add_new_copper_generators(existing_generators_df, new_generators_df, avg_gen_size_canada, vre_gen_types, min_gen_size):
    """
    Add new copper generators to the existing generators DataFrame.

    Parameters:
        existing_generators_df (DataFrame): DataFrame containing existing generators data.
        new_generators_df (DataFrame): DataFrame containing new generators data.

    Returns:
        DataFrame: Updated DataFrame with added new generators.
    """
    # append new generators to existing generators
    for index, row in new_generators_df.iterrows():
        generation_type = row['gen_type_copper'].lower()
        if 'retire_pre2025' in generation_type:
            continue
        if row['gen_type_copper'].lower() not in vre_gen_types:

            gen_type_mapping = {
                'gascc': 'NG_CC',
                'gasccs': 'NG_CC',
                'gassc': 'NG_SC',
                'coal': 'coal'
            }

            bad_types = ["pre", "post"]

            gen_val = row['gen_type_copper'].lower()
            if any(x in gen_val for x in bad_types):
                gen_val_split = gen_val.split("_")
                gen_val = gen_type_mapping[gen_val_split[0]]

            if row['dif'] > 0:  # need new generators

                avg_size = existing_generators_df[existing_generators_df['gen_type_copper'] ==
                                                      gen_val]['unit_installed_capacity'].mean()

                # if no (or little) existing generation type in province, use canadian average or minimum gen size
                # (defined in disaggregate_generators function)
                if gen_val in avg_gen_size_canada:
                    avg_size = max(avg_gen_size_canada[gen_val], min_gen_size)
                else:
                    avg_size = min_gen_size
                num_needed = math.ceil(row['dif']/avg_size)

                facility_name = f'need_to_build_{row.province}_{row.gen_type_copper}_generator{random.randint(1,99999999999999)}'
                facility_node_code = f"please_assign_bus_in_{row.province}_{random.randint(1,99999999999999)}"
                facility_cap = row['dif']
                remaining_cap = row['dif']
                for new in range(num_needed):

                    new_size = min(avg_size, remaining_cap)

                    new_gen = {'generation_unit_code': f'need_to_build_{row.gen_type_copper}_generator{random.randint(1,99999999999999)}',
                                'generation_facility_code': facility_name, 'unit_installed_capacity': new_size,
                                'facility_installed_capacity': facility_cap,
                                'copper_balancing_area': row.balancing_area, 'gen_type': gen_val,
                                'connecting_node_code': facility_node_code,
                                'province':row.province, 'operating_region': row.province,
                                # 'gen_type_copper': generation_type,
                                'operating_region': row['province']
                                }

                    remaining_cap -= new_size
                    existing_generators_df.loc[len(existing_generators_df.index)] = new_gen

            if row['dif'] < 0:  # need to retire generators

                existing_generators_df = remove_excess_generation_capacity(existing_generators_df, -(row['dif']),
                                                                        row.gen_type_copper)

    return existing_generators_df
