import pandas as pd
import random
import math

def get_wind_and_solar_capacity(grid_cell_data):
    """
    Get wind and solar capacity data and merge it with grid cell data.

    Parameters:
        copper_folder (str): Path to the folder containing capacity data for solar, wind_ons, and wind_ofs.
        grid_cell_data (DataFrame): Grid cell data to be merged.

    Returns:
        DataFrame: Merged DataFrame containing wind and solar capacity data.
    """

    # overrides column names for ease of use
    col_names = ['year', 'grid_cell', 'capacity']
    solar_capacity = pd.read_csv('capacity_solar.csv', header=0, names=col_names)
    wind_ons_capacity = pd.read_csv('capacity_wind_ons.csv', header=0, names=col_names)
    wind_offs_capacity = pd.read_csv('capacity_wind_ofs.csv', header=0, names=col_names)

    # Filter capacity < 0.01
    solar_capacity = solar_capacity[solar_capacity['capacity'] >= 0.01]
    wind_ons_capacity = wind_ons_capacity[wind_ons_capacity['capacity'] >= 0.01]
    wind_offs_capacity = wind_offs_capacity[wind_offs_capacity['capacity'] >= 0.01]

    # Merge DataFrames
    wind_and_solar_plants = pd.merge(solar_capacity, wind_ons_capacity,
                                     on=['year', 'grid_cell'],
                                     how='outer',
                                     suffixes=['_solar', '_wind_ons'])

    wind_offs_capacity["capacity_wind_ofs"] = wind_offs_capacity["capacity"]
    wind_offs_capacity.drop(columns=["capacity"], inplace=True)

    wind_and_solar_plants = pd.merge(wind_and_solar_plants, wind_offs_capacity,
                                     on=['year', 'grid_cell'],
                                     how='outer',
                                     suffixes=['', '_wind_ofs'])

    wind_and_solar_plants = pd.merge(wind_and_solar_plants, grid_cell_data,
                                     how='left',
                                     on='grid_cell')

    wind_and_solar_plants['year'] = pd.to_datetime(wind_and_solar_plants['year'], format='%Y').dt.year
    wind_and_solar_plants.rename(columns={'ba': 'balancing_area'}, inplace=True)
    wind_and_solar_plants['copper_balancing_area'] = wind_and_solar_plants['balancing_area'].copy()
    wind_and_solar_plants['province'] = wind_and_solar_plants['Province'].str.lower()
    wind_and_solar_plants.drop(columns=['Province'], inplace=True)

    return wind_and_solar_plants

def template_new_gen(row, type, new_gen_size):
    # silver doesnt currently support wind_ons or wind_ofs
    return {
        'generation_unit_code': f'need_to_build_{type}_generator{random.randint(1,99999999999999)}',
        'unit_installed_capacity': new_gen_size,
        'copper_balancing_area': row.province,
        'gen_type': 'wind' if type in ['wind_ons', 'wind_ofs'] else 'solar',
        'operating_region': row.province,
        'latitude': row.lat,
        'longitude': row.lon,
        'connecting_node_code': f"please_assign_bus_{row.province}_{row.lat}_{row.lon}",
    }

def add_new_wind_ons(row, existing_generators, num_gen_needed, avg_gen_size):
    remaining_cap = row.capacity_wind_ons
    for _ in range(num_gen_needed):
        new_gen_size = min(avg_gen_size, remaining_cap)
        new_gen = template_new_gen(row, 'wind_ons', new_gen_size)
        remaining_cap -= new_gen_size
        existing_generators.loc[len(existing_generators.index)] = new_gen
    return existing_generators

def add_new_wind_ofs(row, existing_generators, num_gen_needed, avg_gen_size):
    remaining_cap = row.capacity_wind_ofs
    for _ in range(num_gen_needed):
        new_gen_size = min(avg_gen_size, remaining_cap)
        new_gen = template_new_gen(row, 'wind_ofs', new_gen_size)

        remaining_cap -= new_gen_size
        existing_generators.loc[len(existing_generators.index)] = new_gen
    return existing_generators

def add_new_solar(row, existing_generators, num_gen_needed, avg_gen_size):
    remaining_cap = row.capacity_wind_ofs
    for _ in range(num_gen_needed):
        new_gen_size = min(avg_gen_size, remaining_cap)
        new_gen = template_new_gen(row, 'solar', new_gen_size)

        remaining_cap -= new_gen_size
        existing_generators.loc[len(existing_generators.index)] = new_gen
    return existing_generators

def add_vre_generators(existing_generators, new_generators, avg_gen_size_canada):
    existing_generators.reset_index(drop=True, inplace=True)

    # to get rid of outliers, minimum generator size is set
    # TO DO: make this so user can set this themselves?
    min_vre_gen_size = 50

    # TODO: This function can likely be shortened a great deal by utilizing sub-functions
    for index, row in new_generators.iterrows():
        if row['capacity_wind_ons'] > 0:
            avg_gen_size = existing_generators[existing_generators['gen_type_copper'] ==
                                                      'wind_ons']['unit_installed_capacity'].mean()

            if avg_gen_size >= min_vre_gen_size:
                num_gen_needed = math.ceil(row['capacity_wind_ons']/avg_gen_size)
            else:
                # if no existing generation type in province, use canadian average or min VRE size
                # (defined in disaggregate_generators function)
                try:
                    size_used = max(avg_gen_size_canada['wind_ons'], min_vre_gen_size)
                except:
                    size_used = min_vre_gen_size
                num_gen_needed = math.ceil(row['capacity_wind_ons']/size_used)
                avg_gen_size = size_used

            existing_generators = add_new_wind_ons(row, existing_generators, num_gen_needed, avg_gen_size)

        if row['capacity_wind_ofs'] > 0:
            avg_gen_size = existing_generators[existing_generators['gen_type_copper'] ==
                                                      'wind_ofs']['unit_installed_capacity'].mean()

            if avg_gen_size >= min_vre_gen_size:
                num_gen_needed = math.ceil(row['capacity_wind_ofs']/avg_gen_size)
            else:
                # if no existing generation type in province, use canadian average or min VRE size
                # (defined in disaggregate_generators function)
                try:
                    size_used = max(avg_gen_size_canada['wind_ofs'], min_vre_gen_size)
                except:
                    size_used = min_vre_gen_size
                num_gen_needed = math.ceil(row['capacity_wind_ofs']/size_used)
                avg_gen_size = size_used

            existing_generators = add_new_wind_ofs(row, existing_generators, num_gen_needed, avg_gen_size)

        if row['capacity_solar'] > 0:  # TODO need to retire generators
            avg_gen_size = existing_generators[existing_generators['gen_type_copper'] ==
                                                      'solar']['unit_installed_capacity'].mean()

            if avg_gen_size >= min_vre_gen_size:
                num_gen_needed = math.ceil(row['capacity_solar']/avg_gen_size)

            else:
                # if no existing generation type in province, use canadian average or min VRE size
                # (defined in disaggregate_generators function)
                try:
                    size_used = max(avg_gen_size_canada['solar'], min_vre_gen_size)
                except:
                    size_used = min_vre_gen_size
                num_gen_needed = math.ceil(row['capacity_solar']/size_used)
                avg_gen_size = size_used

            existing_generators = add_new_solar(row, existing_generators, num_gen_needed, avg_gen_size)
    return existing_generators
