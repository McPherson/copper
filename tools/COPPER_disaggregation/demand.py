import random
import shutil

import pandas as pd
from openpyxl.reader.excel import load_workbook



def merge_hourly_demand(demand_profiles, year):
    """
    Merge demand profiles with an hourly datetime index.

    Parameters:
        demand_profiles (DataFrame): DataFrame containing demand profiles.

    Returns:
        DataFrame: Merged DataFrame with hourly datetime index and hour index of the year.
    """
    if year==None:
        year='2021'

    start_date = f'{int(year)}-01-01'
    end_date = f'{int(year)+1}-01-01'
    date_range = pd.date_range(start=start_date, end=end_date, freq='h', inclusive='left')
    hour_template = pd.DataFrame(date_range, columns=['date'])
    hour_template['hour'] = hour_template.index + 1
    demand_profiles = pd.merge(demand_profiles, hour_template, how="left", on='hour')
    return demand_profiles


def get_copper_demand(demand_profiles, province, year=None):
    """
    Get copper input demand profile and format it for conversion and scaling into silver scenario inputs.

    Parameters:
        demand_file (str): Path to the demand profile file.

    Returns:
        DataFrame: Formatted DataFrame with hourly demand profile and hour index of the year.
    """

    if 'capacity_factor' in demand_profiles.columns:
        demand_profiles['province'] = demand_profiles['region_hour'].apply(lambda x: x.split('.')[0]).str.lower()
        demand_profiles['hour'] = demand_profiles['region_hour'].apply(lambda x: int(x.split('.')[1]))
        demand_profiles = demand_profiles.loc[demand_profiles['province'].str.lower() == province]
        demand_profiles = demand_profiles.rename(columns={'capacity_factor': 'demand'}).drop(columns={'region_hour'})
        demand_profiles = merge_hourly_demand(demand_profiles, year)

    else:
        demand_profiles_new = pd.DataFrame(columns = ['demand', 'province', 'hour'])
        for i in demand_profiles.columns:
            temp_df = pd.DataFrame()
            temp_df['demand'] = demand_profiles[i]
            temp_df['province'] = i.lower()
            temp_df['hour'] = demand_profiles.index + 1
            demand_profiles_new = pd.concat([demand_profiles_new, temp_df], ignore_index=True)

        demand_profiles = merge_hourly_demand(demand_profiles_new, year)


    return demand_profiles


def extract_provincial_demand(province, year, national_demand, growth_rates, reference_year):
    """
        This function reads in hourly demand schedules from COPPER's inputs, and reformats it with each province as a column

            input: list of provinces to read demand for
            output: demand dataframe
        """


    formatted_demand = get_copper_demand(national_demand, province, year)

    growth_rate = growth_rates.loc[growth_rates['AP'].str.lower() == province, str(year)].values[0]
    scale_factor = (1 + growth_rate) ** (year - reference_year)

    # Create dataframe with hourly datetime index
    provincial_demand = formatted_demand.loc[formatted_demand['province'].str.lower() == province]
    total_real = pd.DataFrame()
    total_real['date'] = provincial_demand['date']
    
    # change the year in the datestring in the date column to the year in the year variable
    total_real['date'] = total_real['date'].apply(lambda dt: dt.replace(year=year))
    total_real['demand'] = provincial_demand['demand'].apply(lambda x: x * scale_factor)

    return total_real