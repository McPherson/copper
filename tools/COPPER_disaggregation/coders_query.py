from urllib.request import urlopen
import json
import sys
import os
import pandas as pd

try:
    API_KEY = str(open('key.txt', 'r').read())
    if API_KEY == "":
        print(f'No CODERS api key found, add in your CODERS api key to a key.txt file in {os.getcwd()} directory, if the file doesnt exist create a file called key.txt and add your API key there.')
        sys.exit()
except FileNotFoundError as api_error:
    api_error.strerror = f'No CODERS api key found, add in your CODERS api key to a key.txt file in {os.getcwd()} directory'
    #raise api_error

def construct_query(CODERS_URL, endpoint:str, params:dict[str,str]) -> str:
    '''
    Constructs a query string to endpoint by adding dictionary of parameters to URL along with an API key
    '''
    query = f'{CODERS_URL}/{endpoint}?key={API_KEY}&'
    for key, value in params.items():
        query += f'{key}={value}&'
    query = query[:-1]
    return query

def get_CODERS_generators(CODERS_URL):
    """
    Get generators data from CODERS API.

    Returns:
        DataFrame: DataFrame containing generators data.
    """
    query = construct_query(CODERS_URL, "generators",{})
    with urlopen(query) as response:
        response_content = response.read()
        json_response = json.loads(response_content)
        generators = pd.json_normalize(json_response)
    return generators


def get_CODERS_storage(CODERS_URL):
    """
    Get storage data from CODERS API.

    Returns:
        DataFrame: DataFrame containing storage data.
    """
    query = construct_query(CODERS_URL, "storage",{})
    with urlopen(query) as response:
        response_content = response.read()
        json_response = json.loads(response_content)
        storage = pd.json_normalize(json_response)
    return storage