from pathlib import Path
import os
import pandas as pd
from datetime import datetime, timedelta
import sys
import subprocess

def extra_package_installer(package):
    """
    Function to pip install any package passed to it
    Use: if a package is not found from base requirements.txt, but is needed for whatever reason, call this function
    """
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

def init_input(scenario_folder: str, root: str|Path|None = None):
    """
    Creates a function that prioritizes pulling files from scenario folder and uses common folder as fallback
    
    Args:
        scenario_folder: path of scenario folder
        root: (Optional) where scenario folder is contained, takes current folder by default

    Returns: 
        file retrieval function

    """
    
    main_folder = Path(root) if root else Path(os.getcwd())

    scenario_path = main_folder / 'scenarios' / scenario_folder
    common_path = main_folder / 'scenarios' / 'common'

    def get_scenario_file(file: str) -> Path:
        """
        Get path of file prioritizing scenario folder over common folder
        
        Args:
            file: file to search for

        Returns: 
            Path: full file path

        """
        if os.path.exists(file_loc := scenario_path / file):
            return file_loc

        if os.path.exists(file_loc := common_path / file):
            return file_loc
        
        raise FileNotFoundError(f"File {file} not found in {scenario_folder} or common folder")
    return get_scenario_file

def get_VRE_cf(h, file_name, getter):
    """
    Reading hourly wind and solar CF data in each grid cell only for hours in rundays
    """
    #get_file = init_input(scenario)
    cf_df = pd.read_csv(getter(file_name), index_col="h", engine="pyarrow")
    mean_cf = cf_df.mean().mean()
    # Filtering solarcf data to only include the hours in rundays
    cf_df = cf_df.query("h == @h")
    mean_cf_reduced = cf_df.mean().mean()
    cf_df.columns = cf_df.columns.astype(int)
    return cf_df, mean_cf, mean_cf_reduced

def dataframe_processor(df, ap, prov_idx=-1):
    """
    Removes unwanted provinces from inputs
    """
    df = df[df.iloc[:, prov_idx].str.contains("|".join(ap))]  # drop unwanted provinces
    df = df.reset_index(drop=True)

    return df


def VRE_study(solar_file : str, wind_file : str):
    """Function to plot and provide insights on the VRE data files"""
    
    try:
        import matplotlib.pyplot as plt
    except ModuleNotFoundError:
        extra_package_installer("matplotlib")
        import matplotlib.pyplot as plt
    
    os.chdir('scenarios\\common')
    
    # Dataframes
    
    solar = pd.read_csv(solar_file, header=0, index_col=0, sep=",")
    wind = pd.read_csv(wind_file, header=0, index_col=0, sep=',')

    vre_index = pd.MultiIndex.from_product([solar.index, ['solar', 'wind']], names=['hour', 'gridcell'])
    vre_df = pd.concat([solar, wind], axis=1, keys=['solar', 'wind'], ignore_index=False)
    
    vre_df["date"] = pd.to_datetime("2021-01-01") + pd.to_timedelta(vre_df.index - 1, unit='h')
    vre_df.set_index("date", inplace=True)
    print(vre_df.head())
    
    # Plots

    ####### By hour #######

    hourly_solar = vre_df['solar'].resample('H').sum().median(axis=1)
    fig, axs = plt.subplots(3, 1, figsize=(10, 15), constrained_layout=True)
    axs[0].plot(hourly_solar.index, hourly_solar.values, label='Sum of Solar Capacity Factors')
    axs[0].set_xlabel('Month of the Year')
    axs[0].set_ylabel('Capacity Factor')
    axs[0].set_title('Hourly Solar Capacity Factors')
    axs[0].set_xticks(vre_df.resample('MS').first().index)

    monthly_solar = vre_df['solar'].resample('M').mean()
    monthly_solar['mean_all'] = vre_df['solar'].resample('M').mean().mean(axis=1)
    
    axs[1].bar(monthly_solar.index.month, monthly_solar['mean_all'], label='Mean of All Gridcells')
    axs[1].set_xlabel('Month of the Year')
    axs[1].set_ylabel('Average Solar Capacity Factor')
    axs[1].set_title('Monthly Average Solar Capacity Factor')
    axs[1].set_xticks(range(1,13)) # 12 months
    # Format x-tick labels to show the month name
    axs[1].set_xticklabels([pd.to_datetime('2021-{:02d}-01'.format(month)).strftime('%b') for month in range(1, 13)])
    axs[1].legend()

    #### By gridcell #####

    gridcell_solar = vre_df['solar'].resample('H').sum().mean(axis=0)
    #fig2, axs2 = plt.plot(figsize=(15, 10))
    axs[2].plot(gridcell_solar.index, gridcell_solar.values, label='Average of Year-round Sum of Solar Capacity Factors by Gridcell')
    axs[2].set_xlabel('Gridcell')
    axs[2].set_ylabel('Capacity Factor')
    axs[2].set_title('Average of Year-round Sum of Solar Capacity Factors by Gridcell')
    axs[2].set_xticks(range(0, max(vre_df['solar'].columns.astype(int)) + 1, 100))

    axis_grid = plt.gca()
    axis_grid.xaxis.grid(True)
    axis_grid.grid(which='major', linestyle='-', linewidth='0.5', color='gray')

    plt.show()