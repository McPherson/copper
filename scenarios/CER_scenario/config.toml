[Simulation_Settings]
    user_specified_demand= false
    forecast_year= 2050
    reference_year= 2021
    season= ["winter", "summer"]
    capacity_val_inds= ['winter.wind','winter.solar','summer.wind','summer.solar']
    pds= ["2025","2030","2035","2040","2045","2050"]   

    # Regions 
    ap= [
        "British Columbia",
        "Alberta",
        "Saskatchewan",
        "Manitoba",
        "Ontario",
        "Quebec",
        "New Brunswick",
        "Newfoundland and Labrador",
        "Nova Scotia",
        "Prince Edward Island",
    ]  # all provinces
    
    # If regions are changed above, change the "aba" vector accordingly
    aba= [
        "British Columbia.a",
        "Alberta.a",
        "Saskatchewan.a",
        "Manitoba.a",
        "Ontario.a",
        "Ontario.b",
        "Quebec.a",
        "Quebec.b",
        "New Brunswick.a",
        "Newfoundland and Labrador.a",
        "Newfoundland and Labrador.b",
        "Nova Scotia.a",
        "Prince Edward Island.a",
    ]  # all possible balancing areas
    aba1= ["a", "b"] # Leave this vector untouched
    run_days=[29, 42, 61, 91, 145, 179, 195, 225, 250, 298, 333, 357]
    run_days_test=[29]
    save_lp= true

[Solver]
    [Solver.cplex]
        randomseed= 0
        absmipgap= 0.05
    [Solver.cbc]
        randomS= 0
        ratio= 0.05
        maxSo= 1
    [Solver.glpk]
        seed= 0
        mipgap= 0.05 

[Transmission]
    trans_o_m= 25000
    transcost= 140
    intra_ba_transcost= 940
    translossfixed= 0.02
    translossperkm= 0.000525
    CTE_extant= false ##constrained transmission expansion, constrains the tranmission expansion capacity to a coefficient of the current capacity
    CTE_coef= 1 ## percent allowable increase in transmission beyond the original capacities (0 indicates a grid fixed to its input transmission capacities; 1 implies an allowable increase of 100% of the input transmission capacities)
    CTE_custom = false ## flag for a custom transmission capacity scenario - user must include the trans_expansion_limits.csv file to submit input for transmission capacity constraints
        

[Hydro]
    pump_ret_limit= 0.2 # percent of available hydro that can be upgraded to  store energy (retrofit to add pumped hydro storage)
    hydro_development= true # flag for hydro development
    just_small_hydro= false ## just consider small hydro projects, under 100 MW
    hydro_minflow= 0.05 # Minimal flow of hydro installations
    

[Carbon]
    carbon_reduction= 0
    provincial_emission_limit= false
    emission_limit_ref_year = 2017
    national_emission_limit = false
    local_gas_price = true # local or national natural gas price
    
    ###No CCS will be built in the balancing areas specified in no_CCS_aba 
    no_CCS_aba = ["British Columbia.a", "Manitoba.a", "Ontario.a","Ontario.b","Quebec.a","Quebec.b", "New Brunswick.a", "Newfoundland and Labrador.a","Newfoundland and Labrador.b","Nova Scotia.a","Prince Edward Island.a"]
    flag_noNewCCS_2035 = true

    ###CCS technologies
    CCS_tech = ['coal_ccs_pre2025', 'gasSC_ccs_pre2025', 'gasCCS_post2025', 'gasCC_ccs_pre2025', 'gasCG_restricted_ccs_pre2025']
    ##Cogeneration technologies
    cg_tech = ['gasCG_free_pre2025', 'gasCG_restricted_pre2025', 'gasCG_restricted_ccs_pre2025', 'gasCG_restricted_rng_pre2025', 'gasCG_restricted_backup_pre2025']
    ##Cogeneration Rate used to set constraints on cogeneration technologies supply<=(capacity-retired_capacity)+(existing_capacity*cogeneration_rate)
    cogeneration_rate = 0.37
    
    ## dictionary of periods (keys) and their associated emission limits (values)
    [Carbon.nat_em_limit]
        "2025" = 100
        "2030" = 11
        "2035" = 0
        "2040" = 0
        "2045" = 0
        "2050" = 0
    
    ## Carbon Tax
    [Carbon.ctax]
        '2025' = 143 # Carbon tax using average tax between 2025-2029 = 143
        '2030' = 170
        '2035' = 170
        '2040' = 170
        '2045' = 170
        '2050'= 170
    

[Phase_out]        
    thermal_phase_out= true ## Thermal unit phase out, type:year
    ## Phase out years
    [Phase_out.phase_out_type_year]
        coal_pre2025= '2030'
        coal_ccs_pre2025= '2030'


[Economics]
    inflation = 0 # inflation rate expressed as a decimal
    discount = 0.0374 # discount rate expressed as a decimal
    discount_2050 = 0.374 # discount rate for year 2050 - 10x regular discount
    windcost_recon_ons = 99230
    windcost_recon_ofs = 99230
    solarcost_recon = 75325
    load_shedding_cost= 10000 # load_shedding_cost [$/MWh]
    flag_itc = true # flag for Investment Tax Credit (ITC)
    factor_itc = 0.85 # ITC factor for 15% discount on cap cost
    duals_of_interest= ["demsup"] # duals are exported for these constraints, if possible
    [Economics.technology_evolution_on]
        base= true
        evolving= false   



[Storage]
    storage_continuous= true # flag for continuous storage
    st= ['storage_PH', 'storage_LI']
    ## for linkage feedback
    min_installed_LB_PHP= false
    [Storage.storage_hours]
        storage_PH= 8
        storage_LI= 4


[Regulations]
    GPS= true
    CPO= true
    OBPS_on= true ## activate OBPS
    autarky= false # flag for autarky - a scenario of limited trade between balancing areas
    [Regulations.autonomy_pct]
        "British Columbia"= 0.85
        "Alberta"= 0.60
        "Saskatchewan"= 0.60
        "Manitoba"= 0.80
        "Ontario"= 0.80
        "Quebec"= 0
        "New Brunswick"= 0.50
        "Newfoundland and Labrador"= 0
        "Nova Scotia"= 0.50
        "Prince Edward Island"= 0
    
    [Regulations.CER]
        flag_cer = true # flag for Clean Electricity Regulations (CER)
        ### backup generation technologies
        backup_generators = ['diesel_backup_pre2025', 'gasSC_backup_pre2025', 'gasCC_backup_pre2025', 'diesel_backup_post2025', 'gasSC_backup_post2025', 'gasCC_backup_post2025'] 
        ### retired generation technologies
        retired_generators = ['coal_retire_pre2025', 'diesel_retire_pre2025', 'gasSC_retire_pre2025', 'gasCC_retire_pre2025'] 
        ### pre2025 fossil emitting thermal technologies
        pre2025_generators = ['diesel_pre2025', 'gasSC_pre2025', 'gasCC_pre2025', 'coal_pre2025'] 
        max_em_limit = 385.44


[Electrical_grid]
    flag_reserve_aggregated= true
    reserve_margin_aggregate = 0.2
    non_emitting_limit= false
    [Electrical_grid.nonemitting_limit]
        "2025"= 0
        "2030"= 0.9
        "2035"= 0
        "2040"= 0
        "2045"= 0
        "2050"= 0


[Thermal]
    new_thermal_limit= false ## new thermal unit restriction, types
    
    ## list of thermal generation technologies that are restricted for new development
    old_limited_tplants= ['coal','gasCC','gasSC','nuclear','diesel','biomass']
    
    limited_new_thermal_gen= []
    #percentage of parasitic load, .2 represents 20% parasitic load this is applied to CCS themral plants in parloadccs() constraint
    parasitic_load = 0.2
    [Thermal.gasprice]
        "British Columbia.a"= 2.69
        "Alberta.a"= 2.60
        "Saskatchewan.a"= 2.55
        "Manitoba.a"= 2.73
        "Ontario.a"= 6.77
        "Ontario.b"= 6.77
        "Quebec.a"= 6.73
        "Quebec.b"= 6.73
        "New Brunswick.a"= 6.21
        "Newfoundland and Labrador.a"= 7.39
        "Newfoundland and Labrador.b"= 7.39
        "Nova Scotia.a"= 7.39
        "Prince Edward Island.a"= 7.39


[Constraints]
    max_nuclear_aba= true ### flag for nuclear capacity constraints by balancing area
    max_smr_aba= true ### flag for SMR capacity constraints by balancing area
    max_bio_aba= true ### flag for biomass capacity constraints by balancing area
    maxwind_ons_perkmsq= 2.5
    maxwind_ofs_perkmsq= 2.5
    maxsolarperkmsq= 29.8
    [Constraints.renewable_portfolio_standards]
        "wind_ons.max"= false
        "wind_ons.min"= false
        "wind_ofs.max"= false
        "wind_ofs.min"= false
        "solar.max"= false
        "solar.min"= false

[Disaggregation_Constants]
    flag_disaggregate_generators = false
    CODERS_URL = 'http://206.12.95.102'
    vre_gen_types = ["wind_ons", "wind_ons_recon", "wind_onshore", "wind_ofs", "wind_ofs_recon", "wind_offshore", "wind", "solar", "solar_recon"]
    min_gen_size = 50  #to get rid of outliers, minimum generator size is set for disaggregation